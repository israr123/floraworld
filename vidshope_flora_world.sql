-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 21, 2018 at 01:00 PM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidshope_flora_world`
--

-- --------------------------------------------------------

--
-- Table structure for table `applied_jobs`
--

CREATE TABLE `applied_jobs` (
  `id` int(11) NOT NULL,
  `gardner_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `cost` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `ignored` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applied_jobs`
--

INSERT INTO `applied_jobs` (`id`, `gardner_id`, `job_id`, `cost`, `description`, `ignored`) VALUES
(5, 1, 1, '222', 'hell ho', 0),
(9, 5, 2, '800', 'tyrtytryrtyr', 0),
(10, 5, 4, '100', 'sfasfasfsa', 0),
(11, 1, 4, '40', 'dsfsdfdsfsd  ddddd', 0),
(12, 1, 5, '40', 'fdfsdfsd sd fd', 0),
(13, 1, 2, '133', 'its a bog job', 0);

-- --------------------------------------------------------

--
-- Table structure for table `follow`
--

CREATE TABLE `follow` (
  `id` int(11) NOT NULL,
  `follower_id` int(11) NOT NULL,
  `following_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gardners`
--

CREATE TABLE `gardners` (
  `id` int(11) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `lat` varchar(255) NOT NULL,
  `log` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `email_verification_code` varchar(255) NOT NULL,
  `verified` int(11) NOT NULL,
  `account_status` int(11) NOT NULL,
  `online_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gardners`
--

INSERT INTO `gardners` (`id`, `f_name`, `l_name`, `username`, `email`, `password`, `phone`, `picture`, `lat`, `log`, `location`, `description`, `email_verification_code`, `verified`, `account_status`, `online_status`) VALUES
(1, 'garner1', 'lname', 'gardner1', 'garner1@yahoo.com', '$2y$10$QsSe.IxoFXgg6YoAbCdHH.40BlUIpsIVCEXdm.XAcg5Sa2.C2EqXG', '3333', '', '29.3892978', '71.71034600000007', 'Bahawalpur', 'ddddddddddddddddddddddddddsaaaaaaaaaaaaaaaaaaaavxvcvcxvcxvcxc', '', 0, 0, 0),
(2, 'garner2', 'laname', 'gardner2', 'garner2@yahoo.com', '$2y$10$QsSe.IxoFXgg6YoAbCdHH.40BlUIpsIVCEXdm.XAcg5Sa2.C2EqXG', '1234567', 'smallman.png', '31.520370', '74.358747', 'Lahore', '', '', 0, 0, 0),
(5, 'gardner3', 'gardner', 'gardner3', 'gardener3@gmail.com', '$2y$10$awlXx42gbytFYGa0gcv/c.CLdjfdvJnyelI1FsOlZ35xsU.nXNG5K', '1234234234', '161_4373.jpg', '29.3892978', '71.71034600000007', 'Satellite Town, Bahawalpur, Pakistan', 'i am experienced gardener , i have much experience in gardening .', '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `gardner_reviews`
--

CREATE TABLE `gardner_reviews` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `gardner_id` int(11) NOT NULL,
  `review` text NOT NULL,
  `rates` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gardner_reviews`
--

INSERT INTO `gardner_reviews` (`id`, `job_id`, `gardner_id`, `review`, `rates`) VALUES
(1, 4, 0, 'fsdf', 3),
(2, 4, 0, 'its done\r\n', 5),
(3, 5, 0, 'thanks', 5),
(4, 5, 1, 'gggggg', 5);

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `job_description` text NOT NULL,
  `cost` varchar(255) NOT NULL,
  `post_date` date DEFAULT NULL,
  `expire_date` date NOT NULL,
  `lat` varchar(255) NOT NULL,
  `log` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `job_status` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `assigned_to` int(11) NOT NULL DEFAULT '0',
  `job_picture1` varchar(255) NOT NULL,
  `job_picture2` varchar(255) NOT NULL,
  `job_picture3` varchar(255) NOT NULL,
  `job_picture4` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `job_title`, `job_description`, `cost`, `post_date`, `expire_date`, `lat`, `log`, `location`, `job_status`, `user_id`, `assigned_to`, `job_picture1`, `job_picture2`, `job_picture3`, `job_picture4`) VALUES
(1, '', 'this is sample job description for job 1', '800', '2018-04-15', '2018-04-16', '31.520318', '74.360432', 'job location', 3, 1, 2, '', '', '', ''),
(2, 'Job Titile 2', 'sample description for job 2', '400', '2018-05-08', '2018-05-13', '29.354350', '71.691066', 'Bahawalpur', 2, 1, 5, '', '', '', ''),
(3, '', 'sample description for job 2', '350', '2018-05-08', '2018-05-15', '31.520370', '74.358747', 'Lahore', 3, 1, 2, '', '', '', ''),
(4, '', 'test description 1', '20', '0000-00-00', '2018-05-30', '29.3892978', '71.71034600000007', 'Satellite Town, Bahawalpur, Pakistan', 2, 2, 1, '6-pack-abs-100x100.jpg', '', '', ''),
(5, 'new job zz', 'zzzzzzzzzzzzzzz', '33', '2018-05-20', '2018-05-23', '29.3892978', '71.71034600000007', 'Satellite Town, Bahawalpur, Pakistan', 2, 2, 1, '22482578-bacon-jpg.jpg', '', '', ''),
(6, 'test job zzz', 'zzzzzzzzzzzzzzzzzz', '44', '2018-05-20', '0000-00-00', '29.35435', '71.69106599999998', 'Bahawalpur, Pakistan', 0, 4, 0, 'authentic-925-silver-beads-silver-beloved1.jpg', 'bank1.jpg', 'Adorable_(Custom)2.jpg', '22-Inch-Upright-Multi-60-Game-Arcade-Machine-Cocktail-Mame-Coin-Op-Arcade1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `job_history`
--

CREATE TABLE `job_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `gardner_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `job_start_date` date NOT NULL,
  `comp_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_history`
--

INSERT INTO `job_history` (`id`, `user_id`, `gardner_id`, `job_id`, `job_start_date`, `comp_date`) VALUES
(1, 1, 2, 1, '2018-04-15', '2018-04-16'),
(2, 1, 5, 2, '2018-05-15', '2018-05-16'),
(5, 2, 1, 4, '0000-00-00', '0000-00-00'),
(6, 2, 1, 5, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `post_date` date NOT NULL,
  `picture1` varchar(255) NOT NULL,
  `picture2` varchar(255) NOT NULL,
  `picture3` varchar(255) NOT NULL,
  `picture4` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `f_name` varchar(255) NOT NULL,
  `l_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL DEFAULT 'smallman.png',
  `lat` varchar(255) NOT NULL,
  `log` varchar(255) NOT NULL,
  `email_verification_code` varchar(255) NOT NULL,
  `verified` int(11) NOT NULL,
  `account_status` int(11) NOT NULL,
  `online_status` int(11) NOT NULL,
  `location` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `f_name`, `l_name`, `username`, `email`, `password`, `phone`, `picture`, `lat`, `log`, `email_verification_code`, `verified`, `account_status`, `online_status`, `location`, `description`) VALUES
(1, 'user1', 'lname', 'user1', 'user1@yahoo.com', '$2y$10$zmnu3stDaX0hRJzxBHU5JuTgLVH2I03O7pp2VaWUhOZ6CwNbfaTpu', '023457852', 'smallman.png', '', '', '', 0, 0, 0, '', ''),
(2, 'user2', 'lname', 'user2', 'user2@yahoo.com', '$2y$10$rxbdzvwb3YRV47QG.aRmQeE6wI4/axogVBRxYKkNVqjg1h9GwSk1m', '023454875', 'Adorable21.jpg', '', '', '', 0, 0, 0, 'Satellite Town, Bahawalpur, Pakistan', 'i am garden ownerxzcxcz'),
(3, 'fname', 'lname', 'user33', 'user33@email.com', '$2y$10$2tVFYOmq864ooT9ZYZxhDOGFwAuOmkt2jf.CoSensMbZlVec1vH.u', '03000000000000', '161_4374.jpg', '29.35435', '71.69106599999998', '', 0, 0, 0, 'Bahawalpur, Pakistan', ''),
(4, 'user222', 'lname', 'user22', 'user22@email.com', '$2y$10$pzXQk1K2LMG3GGDZerkLmesILSqVsMUjNrFmRfTg78ThK3VnNz5xi', '03000000000', '100px-1907_double_eagle_obv_(searched_for_american_history0.jpg', '', '', '', 0, 0, 0, 'Bahawalpur, Pakistan', ''),
(5, 'wesdf', 'sdf', 'sdfsf', 'ds@sda.d', '$2y$10$nlQjRmWXgPsqoaJZ9iAgpetRZuWxyzYKjfISquaeNETUVfPWjidFC', '784545', '', '12.8320148', '77.68557129999999', '', 0, 0, 0, 'DSFS Convent, Kammasandra, Electronic City, Bengaluru, Karnataka, India', 'hello this section is ne'),
(6, 'firstname', 'lastname', 'firstlast', 'testing@gmail.com', '$2y$10$45TIjcxqOhWAkN7FkTC2LOGOk6ir1NubR0kvFPT7qOFVuOifKVhx2', '031234567895', '', '33.6844202', '73.04788480000002', '', 0, 0, 0, 'Islamabad, Pakistan', 'hello this is something about me'),
(7, 'firstname1', 'lastname1', 'firstlast1', 'testing1@gmail.com', '$2y$10$qvg1bOq40UG5dUA7egDn.uSunC9QUg7eh7HulM9aKzyFdGz1/gUyC', '031234567895', 'smallman.png', '', '', '', 0, 0, 0, 'Islamabad, Pakistan', 'this is second one ');

-- --------------------------------------------------------

--
-- Table structure for table `user_reviews`
--

CREATE TABLE `user_reviews` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `review` text NOT NULL,
  `rates` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_reviews`
--

INSERT INTO `user_reviews` (`id`, `job_id`, `user_id`, `review`, `rates`) VALUES
(6, 4, 0, 'good', 3),
(12, 4, 0, 'sadasdasdas', 4),
(13, 5, 0, 'goood', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applied_jobs`
--
ALTER TABLE `applied_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `follow`
--
ALTER TABLE `follow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gardners`
--
ALTER TABLE `gardners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gardner_reviews`
--
ALTER TABLE `gardner_reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_history`
--
ALTER TABLE `job_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_reviews`
--
ALTER TABLE `user_reviews`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applied_jobs`
--
ALTER TABLE `applied_jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `follow`
--
ALTER TABLE `follow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gardners`
--
ALTER TABLE `gardners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `gardner_reviews`
--
ALTER TABLE `gardner_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `job_history`
--
ALTER TABLE `job_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user_reviews`
--
ALTER TABLE `user_reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
