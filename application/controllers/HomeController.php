<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends MY_Controller {
     
	function __construct()
	{
		parent::__construct();
		$this->load->Model('HomeModel');
		$this->load->model('GardnerModel');
		$this->load->model('UserModel');
		$this->load->helper('url');
	}
	
	public function index()
	{
		$data['main_content']="index";
		$this->load->view('layout/template',$data);

	}
	public function listing(){
        
		$data['gardners']=$this->HomeModel->listing();
		$data['topgardners']=$this->HomeModel->top_gardeners();   
		$data['main_content']='listing';
		$this->load->view('layout/template',$data);

	}
	
	public function about($cont=""){
		
		$data['main_content']='about';
		$this->load->view('layout/template',$data);

	}
	public function contactus(){
		$data['main_content']='contact';
		$this->load->view('layout/template',$data);

	}

	public function savecontactus(){
      
		$this->form_validation->set_rules('email','Email','required');
		$this->form_validation->set_rules('message','Comment','required');

		if($this->form_validation->run()==false){
			$this->session->set_flashdata('error', validation_errors());
			redirect('contactus');
		}

		else{

			$data= array(
			
			'email' => $this->input->post('email'),
			'message' => $this->input->post('message')

			 );

			$this->HomeModel->savecontactus($data);
			$this->session->set_flashdata('success','Your Query sent Successfully!');
			redirect('contactus');
		}




	}
	
	public function get_grd_total_notifications()
    {
		$userid=$this->session->userdata('gardner_id');
		$total_notifications=$this->GardnerModel->count_notifications($userid);
		 
		  echo   json_encode( $total_notifications);
		 
	}
	
	public function get_user_notifications()
    {
		$userid=$this->session->userdata('user_id');
		$notifications=$this->UserModel->get_new_notifications($userid);
		//echo "<pre>"; print_r($notification);die;
		$data=array();
		foreach($notifications as $notification)
		{
			$data[]='<a class="dropdown-item" href="'.base_url().'buying">
			<strong>'.$notification->description .'</strong>
			</a>';
		}
		
		  echo json_encode( $data );
	}	
	
	public function get_posts()
    {
		$userid=$this->session->userdata('user_id');
		$post_ids=$this->session->userdata("post_idies");
		$posts=$this->UserModel->get_user_posts_by_ajax($userid,$post_ids);
		//echo "<pre>"; print_r($posts);die;
		if(!empty($posts))
		{
		
		$data=array();
		foreach($posts as $feed)
		{
			$col=6;
			if($feed->picture2 =='' && $feed->picture3=='' && $feed->picture4=='' ) $col=12; else $col=6;
			
			$total_likes=$this->UserModel->count_likes($feed->id);
			array_push($post_ids,$feed->id);
			$this->session->set_userdata("post_idies",$post_ids);
			$data[]='<div class="body-content">
        	<div class="widget">
            	<div class="title text-green">'. $feed->f_name." ".$feed->l_name.'</div>
                <div class="time text-muted">'. $feed->post_date.'</div>
                <div class="discription">'.substr($feed->description,0,200).'. . .</div>
                <div class="gallery">
                	<div class="row">
					
                    	<div class="col-md-'.$col.'">
                        	<img alt="" src="'.base_url("assets/img/").'/'.$feed->picture1.'" class="img-fluid" >
                        </div>
                        <div class="col-md-6">
                        	<img alt="" src="'.base_url("assets/img/").'/'.$feed->picture2.'" class="img-fluid" >
                        </div>
                        <div class="col-md-6">
                        	<img alt="" src="'.base_url("assets/img/").'/'.$feed->picture3.'" class="img-fluid" >
                        </div>
                        <div class="col-md-6">
                        	<img alt="" src="'.base_url("assets/img/").'/'.$feed->picture4.'" class="img-fluid" >
                        </div>
                    
                    </div>
                
                </div>
                
            <div class="actions">
                	<a herf="#" class="text-muted"> <i id="like'.$feed->id.'" class="fa fa-thumbs-up like"></i> <span id="total_likeslike'.$feed->id.'">'.$total_likes.'</span> Likes</a>
                    <a herf="#" onClick="sharepost(post'.$feed->id.');" class="text-muted"> <i class="fa fa-share"></i> Share</a>
                <div class="share" id="post'.$feed->id.'" style="display:none">        
         <div data-network="twitter" class="st-custom-button">Twitter</div>
<div data-network="facebook" class="st-custom-button">Facebook</div> 
<div data-network="linkedin" class="st-custom-button">LinkedIn</div> 
<div data-network="email" class="st-custom-button">Email</div> 
            </div>
                </div>
            </div>
        
        </div>';
			
			
			
		}
		
		  echo json_encode( $data );
		}
	}	
	
	public function get_grd_notifications()
    {
		$userid=$this->session->userdata('gardner_id');
		$notifications=$this->GardnerModel->get_new_notifications($userid);
		//echo "<pre>"; print_r($notification);die;
		$data=array();
		foreach($notifications as $notification)
		{
			$data[]='<a class="dropdown-item" href="'.base_url().'gindex">
			<strong>'.$notification->description .'</strong>
			</a>';
		}
		
		  echo json_encode( $data );
	}
public function get_user_total_notifications()
    {
		$userid=$this->session->userdata('user_id');
		$total_notifications=$this->UserModel->count_notifications($userid);
		 
		  echo   json_encode( $total_notifications);
		 
	}		
	public function check_ajax_user_exist($val="")
    {
		
		$res=$total_notifications=$this->UserModel->check_user_exist($val);
		 
		  echo   json_encode( $res);
		  die;
		 
	}		
	





	
	public function viewgardnerprofile($username){
        
		$data['gardner']=$this->HomeModel->viewgardnerprofile($username);
		if($data['gardner']==''){
			$this->session->set_flashdata('error',"Sorry! This Gardener Does't Exist");
			    redirect('listing');
			
		}
		$id=$data['gardner']->id;
		$data['reviews']=$this->HomeModel->get_gardner_review($id);
		$total_reviews=sizeof($data['reviews']);
		//print_r($total_reviews);
		//die();
		if($total_reviews){
			
			$count=0;
			foreach ($data['reviews'] as $review){
				$count+=$review->rates;
			  }
			$total_rates=$count;
			$data['avgreview']=$total_rates/$total_reviews;
			
		}
		else{
			$data['avgreview']="";

		}
		
		//echo "<pre>"; print_r($data['reviews']);die();
		
		$data['main_content']='gardners/viewgardnerprofile';
		$this->load->view('layout/template',$data);

	}
	public function blankerror(){
		$this->session->set_flashdata('error',"Sorry! This User Does't Exist");
			$data['main_content']='blankerror';
			$this->load->view('layout/template',$data);
	}

	public function viewuserprofile($username){
        
		$data['user']=$this->HomeModel->viewuserprofile($username);
		if($data['user']==''){
			$this->session->set_flashdata('error',"Sorry! This User Does't Exist");
			redirect('listing');
			
		}
		
		
		$id=$data['user']->id;
		$data['reviews']=$this->HomeModel->get_user_review($id);
		$total_reviews=sizeof($data['reviews']);
     
     if($total_reviews){

		$count=0;
		foreach ($data['reviews'] as $review){
			$count+=$review->rates;
		  }
		$total_rates=$count;
		$data['avgreview']=$total_rates/$total_reviews;
		
		}
		else{
			$data['avgreview']="";

		}
		//echo "<pre>"; print_r($data['reviews']);
		//die();
		$data['main_content']='users/viewuserprofile';
		$this->load->view('layout/template',$data);


	}
   
   public function search_gardeners_ajax()
   {
	$keyword=$this->input->get('term'); 
	  
        $result=$this->HomeModel->search_gardeners_ajax($keyword);        
        
		 if (count($result) > 0) {
            foreach ($result as $row)
			{
				 $arr_result[] =  $row->f_name." ".$row->l_name;
				
			}
               
                
                echo json_encode($arr_result);
            }

   }

 public function search_gardeners()
   {
	   $keyword=$this->input->post('keyword'); 
	  
        $data['gardners']=$this->HomeModel->search_gardeners($keyword);     
        $data['topgardners']=$this->HomeModel->top_gardeners();     
		//echo $this->db->last_query(); die;		
        //echo "<pre>"; print_r($data['gardners']); die;
		$data['main_content']='search_results';
		$this->load->view('layout/template',$data);
		 
   }

public function search($key=""){
        
		$data['gardners']=$this->HomeModel->search_gardeners($key);
		 echo "<pre>"; print_r($data['gardners']); die;
		

	}

public function hide_grd_notifications()
    {
		$userid=$this->session->userdata('gardner_id');
		$this->GardnerModel->hide_notifications($userid);
		
	}
public function hide_user_notifications()
    {
		$userid=$this->session->userdata('user_id');
		$this->UserModel->hide_notifications($userid);
		
	}
	










































}
