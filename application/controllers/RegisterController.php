<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegisterController extends MY_Controller {

    function __construct()
	{
		parent::__construct();
		//$this->CheckLogin();
		$this->load->model('RegisterModel');
		$this->load->helper('string');
		$this->load->model('Email_model');
	}

	public function index()
	{
		//$this->load->view('welcome_message');

	}
























































































































































































	public function userregister(){
    	
    $data['main_content']='users/signup';
    $this->load->view('layout/template',$data);
     


    }

    public function userregistered(){
       // echo "<pre>"; print_r($this->input->post()); die;
        $this->form_validation->set_rules('first_name', 'First_name', 'required');
        $this->form_validation->set_rules('last_name', 'Last_name', 'required');
        $this->form_validation->set_rules('location', 'Lcation', 'required|min_length[4]');
        $this->form_validation->set_rules('username', 'Username', 'required|min_length[4]|is_unique[users.username]');
        $this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('lat', 'Location you have selected is not valid this, ', 'required');
		 //$this->form_validation->set_rules('password', 'Password', 'required|matches[confirm_password]');
		if($this->RegisterModel->check_user_exist($this->input->post('username')) || $this->RegisterModel->check_user_exist($this->input->post('email'))){

			$this->form_validation->set_rules('username1', 'The %s is already Registered in our database!');

		} 
        
        //$this->form_validation->set_rules('phone', 'phone', 'required|regex_match[/^[0-9]{11}$/]');
		
	$this->form_validation->set_message('is_unique', 'The %s is already Registered in our database!');
	$this->form_validation->set_message('min_length', '%s must be 4 character long!');
	$this->form_validation->set_message('valid_email', 'This %s is not valid!');

// Check if username has changed

		if($this->form_validation->run()==false){
			$this->session->set_flashdata('error', validation_errors());
         $file_name ='';
        $data['info'] = array('fname' =>$this->input->post('first_name'),           
                      'lname'=>$this->input->post('last_name'),
                      'username'=>$this->input->post('username'),
                      'email'=>$this->input->post('email'),
                      'location' => $this->input->post('location'),
                      'log' => $this->input->post('lng'),
                      'lat' => $this->input->post('lat'),
                      'phone'=>$this->input->post('phone')
                   );



            //redirect('user/register',$data);
             $data['main_content']='users/signup';
             $this->load->view('layout/template',$data);
		}
		else{

             $file_name ='smallman.png';


			if(isset($_FILES['picture']['name'])){
					
					$config['upload_path']='./assets/img';
					$config['allowed_types']='jpg|jpeg|png';
					
					$this->load->library('upload',$config);
					
					if($this->upload->do_upload('picture')){
						$file_name = $this->upload->data('file_name');

					}
				}	
					



		   $email_verification_code=random_string('alnum', 16);
		   $app_verification_code=random_int(1111,9999);
			$data = array(
				'f_name' => $this->input->post('first_name'),
				'l_name' => $this->input->post('last_name'),
				'username' => $this->input->post('username'),
				'email' => $this->input->post('email'),
				'password' => password_hash($this->input->post('password'),PASSWORD_BCRYPT),
				'phone' => $this->input->post('phone'),
				'location' => $this->input->post('location'),
				'picture' => $file_name,
				'description' => $this->input->post('description'),
                 'log' => $this->input->post('lng'),
                 'lat' => $this->input->post('lat'),
                 'email_verification_code' => $email_verification_code,
                 'app_verification_code' => $app_verification_code

			);

		if ($user = $this->RegisterModel->add_user($data)) {

			$email = $this->input->post('email');
			$this->send_verification_email($email,$email_verification_code,$app_verification_code);
		//die("hello");	
			//$this->session->set_userdata('email_verify',$email);
			//redirect('user/email_verify');

			$this->session->set_userdata('user_id', $user);
			$this->session->set_flashdata('success', "User has been Registered Successfully!");
			//die("selected");
			
			redirect('uindex');
		}
		}



    }

   public function send_verification_email($email,$email_verification_code,$app_verification_code)
    {
    	$email;
    	$email_verification_code;
    	//die("controller last");
    	$this->load->model('Email_model');
	    $this->Email_model->sendverificationemail($email,$email_verification_code,$app_verification_code);

    }
   public function verify_email($verification_code)
	{
		if($verification_code)
		{
			$response=$this->RegisterModel->verify_user_email($verification_code);
			if($response){
			
				$id=$response->id;
				$this->RegisterModel->update_verify_user_email($id);
				$this->session->unset_userdata('verify_email');
				$this->session->set_userdata('user_id',$id);
				//$this->session->userdata('user_id');
				//die("hello");
				$this->session->set_flashdata('success',"Thank You! for your email verification");
				 redirect('uindex');

			}
			else{
				$response1=$this->RegisterModel->verify_gardner_email($verification_code);
				if($response1){

			  	$id=$response1->id;
				$this->RegisterModel->update_verify_gardner_email($id);
				$this->session->unset_userdata('verify_email');
				$this->session->set_userdata('gardner_id',$id);
				$this->session->set_flashdata('success',"Thank You! for your email verification");
				redirect('gindex');

				}
				else{
					//die('hello');
					redirect();
				}
			}

		} else{
			  redirect();
		     }
		
		
	}
   public function sendEmailfornewpassword($email,$email_verification_code,$app_verification_code){

   	$this->Email_model->sendEmailfornewpassword($email,$email_verification_code,$app_verification_code);

   }

   public function verify_forget_password($code){

	   	if($code)
	   	{

	   $verify=$this->RegisterModel->verify_user_forget_password($code);
	   		if($verify){
	   				$id=$verify->id;
	   				$this->session->set_userdata('uid',$id);
	   				$data['main_content']='forget_password';
	   				$this->load->view('layout/template',$data);

	   			}
	   			else{
	  $verify=$this->RegisterModel->verify_gardner_forget_password($code);     
	   		if($verify){
	   				$id=$verify->id;
	   				$this->session->set_userdata('gid',$id);
	   				$data['main_content']='forget_password';
	   				$this->load->view('layout/template',$data);

	   			    }else{ redirect(); }	 
	   			     
	   			}
	   			
	   	} else{  redirect(); }
	
	   	 


   }

 public function save_forget_password(){

 	$this->form_validation->set_rules('password','Password','required');

 	if($this->form_validation->run()==false){
 		$this->session->set_flashdata('error',validation_errors());
 		$data['main_content']='forget_password';
 		$this->load->view('layout/template',$data);
 	}
 	else{
 		  // $uid=$this->session->userdata('uid');
         //die("hi");
	 	   if($this->session->userdata('uid')){
	 	   	$uid=$this->session->userdata('uid');
	 	   	$data = array(
	 	   		'password' =>password_hash($this->input->post('password'),PASSWORD_BCRYPT) 
	 	   	);
	 	   	$this->RegisterModel->save_user_forget_password($uid,$data);
	 	   	$this->session->unset_userdata('uid');
	 	   	$this->session->set_flashdata('success',"Your password is Updated Successfully");
	 	   	 redirect('login');
	 	   }

	 	   if($this->session->userdata('gid')){
	        $gid=$this->session->userdata('gid');
	        	$data = array(
	 	   		'password' =>password_hash($this->input->post('password'),PASSWORD_BCRYPT)
	 	   	);
	 	   $this->RegisterModel->save_gardner_forget_password($gid,$data);
	 	   $this->session->unset_userdata('gid');
	 	   $this->session->set_flashdata('success',"Your password is Updated Successfully");
	 	    redirect('login');
	 	   }

 	}

 } 

 public function forget_password(){

 	$this->form_validation->set_rules('email','Email','required');

 	if($this->form_validation->run()==false){
 		$this->session->set_flashdata('error',validation_errors());
 		redirect('login');
 	}
 	else{

    $email=$this->input->post('email');
 	$result=$this->RegisterModel->get_user_forget_password_email($email);
 		if($result){

		//die('hi'); 		
		 		$email_verification_code=random_string('alnum', 16);
		 		$app_verification_code=random_int(1111,9999);

		 	
					$data = array(
						'email_verification_code' =>$email_verification_code,
						'app_verification_code' =>$app_verification_code
					);

		 		$this->RegisterModel->add_user_password_string($email,$data);
		 		$this->sendEmailfornewpassword($email,$email_verification_code,$app_verification_code);
		 		$this->session->set_flashdata('success','Please check your inbox to reset your password!');
		 		redirect('login');
		 	    

		    } 
		else{

  					$result=$this->RegisterModel->get_gardner_forget_password_email($email);
           if ($result) {
           	
          
				 		
		 		$email_verification_code=random_string('alnum', 16);
		 		$app_verification_code=random_int(1111,9999);

		 	
					$data = array(
						'email_verification_code' =>$email_verification_code,
						'app_verification_code' =>$app_verification_code
					);

		 		$this->RegisterModel->add_user_password_string($email,$data);
		 		$this->sendEmailfornewpassword($email,$email_verification_code,$app_verification_code);
		 		$this->session->set_flashdata('success','Please check your inbox to reset your password!');
		 		redirect('login');				
			}
			else{

				$this->session->set_flashdata('error',"This Email Does't Exits!");
				redirect('login');
			}
		
		

 	}
    
  }
}

    public function gardnerregister(){
    	//die();
    $data['main_content']='gardners/becomegardner';
    $this->load->view('layout/template',$data);



    }





    public function gardnerregistered(){
    
        $this->form_validation->set_rules('first_name', 'First_name', 'required');
        $this->form_validation->set_rules('last_name', 'Last_name', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required|min_length[4]|is_unique[gardners.username]');
        $this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[gardners.email]');
		$this->form_validation->set_rules('lat', 'Location you have selected is not valid this, ', 'required');
		// $this->form_validation->set_rules('password', 'Password', 'required|matches[confirm_password]');
		 
        if($this->RegisterModel->check_user_exist($this->input->post('username')) || $this->RegisterModel->check_user_exist($this->input->post('email'))){

			$this->form_validation->set_rules('username1', 'The %s is already Registered in our database!');

		} 
        
        //$this->form_validation->set_rules('phone', 'phone', 'required|regex_match[/^[0-9]{11}$/]');
			$this->form_validation->set_message('min_length', '%s 			must be 4 character long. ');
			$this->form_validation->set_message('is_unique', 'The %s is already Registered in our database');
			$this->form_validation->set_message('valid_email', 'This %s is not a valid email!');
		if($this->form_validation->run()==false){
			$this->session->set_flashdata('error', validation_errors());

        $data['info'] = array('fname' =>$this->input->post('first_name'),           
                      'lname'=>$this->input->post('last_name'),
                      'username'=>$this->input->post('username'),
                      'email'=>$this->input->post('email'),
                      'location' => $this->input->post('location'),
                      'lat' => $this->input->post('lat'),
                      'log' => $this->input->post('lng'),
                      'description' => $this->input->post('description'),
                      'phone'=>$this->input->post('phone')
                   );



            //redirect('user/register',$data);
             $data['main_content']='gardners/becomegardner';
             $this->load->view('layout/template',$data);
		}
		else{

			$file_name ='smallman.png';

			if(isset($_FILES['picture']['name'])){
					
					$config['upload_path']='./assets/img';
					$config['allowed_types']='jpg|jpeg|png';
					
					$this->load->library('upload',$config);
					
					if($this->upload->do_upload('picture')){
						$file_name = $this->upload->data('file_name');

					}
				}	
					
		   $email_verification_code=random_string('alnum', 16);
		   $app_verification_code=random_int(1111,9999);
			$data = array(
				'f_name' => $this->input->post('first_name'),
				'l_name' => $this->input->post('last_name'),
				'username' => $this->input->post('username'),
				'email' => $this->input->post('email'),
				'password' => password_hash($this->input->post('password'),PASSWORD_BCRYPT),
				'phone' => $this->input->post('phone'),
				'location' => $this->input->post('location'),
				 'lat' => $this->input->post('lat'),
                 'log' => $this->input->post('lng'),
                 'description' => $this->input->post('description'),
				'picture' => $file_name,
				'email_verification_code'=>$email_verification_code,
				'app_verification_code'=>$app_verification_code
				);
		// echo "<pre>"; print_r($data); die;
		if ($user = $this->RegisterModel->add_gardner($data)) {

			$email = $this->input->post('email');
			$this->send_verification_email($email,$email_verification_code,$app_verification_code);
			//$this->session->set_userdata('email_verify',$email);
			//redirect('user/email_verify');

			$this->session->set_userdata('gardner_id', $user);
			$this->session->set_flashdata('success', "You have been Registered Successfully");
			//die("selected");
			redirect('gindex');
		}
		}



    }














}
