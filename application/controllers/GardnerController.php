<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GardnerController extends MY_Controller {

function __construct()
	{
		parent::__construct();
		$this->checkgardnerlogin();
		//$this->load->model('admin/ArticleModel');
		$this->load->model('GardnerModel');
		//die("hi");
		
	}
	public function index()
	{  $offset1=0;$offset2=100;
		//echo "welcome garner!";
	    $gid=$this->session->userdata('gardner_id');
		//$data['profile']=$this->GardnerModel->profile_get($gid);
		$data['job_history']=$this->GardnerModel->get_previous_jobs($gid,$offset1,$offset2);
		$data['new_jobs']=$this->GardnerModel->get_new_jobs($gid,$offset1,$offset2);
		$data['my_jobs']=$this->GardnerModel->get_my_jobs($gid,$offset1,$offset2);
		$data['applied_jobs']=$this->GardnerModel->applied_jobs($gid,$offset1,$offset2);
		//echo "<pre>"; print_r($data['new_jobs']); die;
		$data['main_content']="gardners/index";
		$this->load->view('layout/template',$data);

	}

   public function profile()
   {
	  //echo "Gardner profile";
	   	$id=$this->session->userdata('gardner_id');
		$data['profile']=$this->GardnerModel->profile_get($id);
		$this->load->Model('HomeModel');
		$data['reviews']=$this->HomeModel->get_gardner_review($id);
		$total_reviews=sizeof($data['reviews']);
		//print_r($total_reviews);
		//die();
		if($total_reviews){
			
			$count=0;
			foreach ($data['reviews'] as $review){
				$count+=$review->rates;
			  }
			$total_rates=$count;
			$data['avgreview']=$total_rates/$total_reviews;
			
		}
		else{
			$data['avgreview']="";

		}
		//print_r($data);die();
		$data['main_content']="gardners/profile";
		$this->load->view('layout/template',$data);

	   
   }
   public function get_user_notifications()
    {
		$userid=$this->session->userdata('gardner_id');
		$notifications=$this->GardnerModel->get_new_notifications($userid);
		//echo "<pre>"; print_r($notification);die;
		$data=array();
		foreach($notifications as $notification)
		{
			$data[]='<a class="dropdown-item" href="gindex">
			<strong>'.$notification->description .'</strong>
			</a>';
		}
		
		  echo json_encode( $data );
	}		
	
			
	
   public function get_ajax_job_history(){
   	$offset1=0;$offset2=100;
	   	$gid=$this->session->userdata('gardner_id');
	   $jobs=$this->GardnerModel->get_previous_jobs($gid,$offset1,$offset2);
	 //  echo "<pre>"; print_r($jobs); die;
    	echo '<div class="job-listing mt-5 table-responsive">';
                
			 if(!empty($jobs)):
				echo '<table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Date</th>
                      <th scope="col">Buyer</th>
                      <th scope="col">Discription</th>
                      <th scope="col">Budget</th>
                    </tr>
                  </thead>
                  <tbody>';
				  foreach($jobs as $job): 
                    echo '<tr>
					
                      <td style="width: 12%;">'.date('d-m-Y',strtotime($job->complition_date)).'</td>
                      <td><a href="user/'.$job->username.'"><img src="'.base_url('assets/img/').$job->picture.'" class="rounded" height="50px"></a></td>
                      <td><a href="'.base_url('history_detail_page/'.base64_encode($job->id)).'">'.$job->job_description.'</a></td>
                      <td>$'.$job->job_cost.'</td>
                    </tr>';
                    endforeach; 
                 echo '</tbody>
                </table>';
				else: echo "There is no Job Available right now !"; endif; 
				
              echo '</div>';
    }  
	
	public function get_ajax_new_jobs(){
		$offset1=0;$offset2=100;
	   	$gid=$this->session->userdata('gardner_id');
	   $jobs=$this->GardnerModel->get_new_jobs($gid,$offset1,$offset2);
	 //  echo "<pre>"; print_r($jobs); die;
    	echo '<div class="job-listing mt-5 table-responsive">';
                
			 if(!empty($jobs)):
				echo '<table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Date</th>
                      <th scope="col">Buyer</th>
                      <th scope="col">Discription</th>
                      <th scope="col">Budget</th>
					   <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>';
				  foreach($jobs as $job): 
                    echo '<tr id="'.$job->id.'">
					
                      <td style="width: 12%;"><span class="time" title="'.$job->post_date.'">'.$job->post_date.'</span></td>
                      <td><a href="user/'.$job->username.'"><img src="'.base_url('assets/img/').$job->picture.'" class="rounded" height="50px"></a></td>
                      <td><a href="'.base_url('job_detail_page/'.base64_encode($job->id)).'">'.$job->job_description.'</a></td>
                      <td>$'.$job->job_cost.'</td>
					  <td><div class="buttonsets">
                          <a onClick="ignorejob('.$job->id.')"><button  type="button" class="btn btn-grey">Ignore</button></a> 
                    
                          <a class="btn themebutton" href="'.base_url().'job_detail_page/'.base64_encode($job->id).'"> Apply</a> 

                        </div></td>
					  
                    </tr>';
                    endforeach; 
                 echo '</tbody>
                </table>';
				else: echo "There is no Job Available right now !"; endif; 
				
              echo '</div>';
    }  
	
	public function get_ajax_my_jobs(){
		$offset1=0;$offset2=100;
	   	$gid=$this->session->userdata('gardner_id');
	   $jobs=$this->GardnerModel->get_my_jobs($gid,$offset1,$offset2);
	 //  echo "<pre>"; print_r($jobs); die;
    	echo '<div class="job-listing mt-5 table-responsive">';
                
			 if(!empty($jobs)):
				echo '<table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Date</th>
                      <th scope="col">Buyer</th>
                      <th scope="col">Discription</th>
                      <th scope="col">Budget</th>
                    </tr>
                  </thead>
                  <tbody>';
				  foreach($jobs as $job): 
                    echo '<tr>
					
                      <td style="width: 12%;"><span class="time" title="'.$job->post_date.'">'.$job->post_date.'</span></td>
                      <td><a href="user/'.$job->username.'"><img src="'.base_url('assets/img/').$job->picture.'" class="rounded" height="50px"></a></td>
                      <td><a href="'.base_url('deliver_work/'.base64_encode($job->id)).'">'.$job->job_description.'</a></td>
                      <td>$'.$job->job_cost.'</td>
                    </tr>';
                    endforeach; 
                 echo '</tbody>
                </table>';
				else: echo "There is no Job Available right now !"; endif; 
				
              echo '</div>';
    }  
	
	public function get_ajax_applied_jobs(){
		$offset1=0;$offset2=100;
	   	$gid=$this->session->userdata('gardner_id');
	   $jobs=$this->GardnerModel->applied_jobs($gid,$offset1,$offset2);
	 // echo "<pre>"; print_r($jobs); die;
    	echo '<div class="job-listing mt-5 table-responsive">';
                
			 if(!empty($jobs)):
				echo '<table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Date</th>
                      <th scope="col">Buyer</th>
                      <th scope="col">Discription</th>
                      <th scope="col">Budget</th>
                    </tr>
                  </thead>
                  <tbody>';
				  foreach($jobs as $job): 
                    echo '<tr>
					
                      <td style="width: 12%;"><span class="time" title="'.$job->post_date.'">'.$job->post_date.'</span></td>
                      <td><a href="user/'.$job->username.'"><img src="'.base_url('assets/img/').$job->picture.'" class="rounded" height="50px"></a></td>
                      <td><a href="'.base_url('job_detail_page/'.base64_encode($job->id)).'">'.$job->job_description.'</a></td>
                      <td>$'.$job->cost.'</td>
                    </tr>';
                    endforeach; 
                 echo '</tbody>
                </table>';
				else: echo "There is no Job Available right now !"; endif; 
				
              echo '</div>';
    } 
	
   public function do_ajax_ignore_job($job_id)
   {
	  $gid=$this->session->userdata('gardner_id');
	  $this->GardnerModel->ignore_job($job_id,$gid);
   }
   
   public function ignore_job($job_id)
   {
	  $gid=$this->session->userdata('gardner_id');
	  $this->GardnerModel->ignore_job($job_id,$gid);
	  redirect('gindex');
   }
	   public function deliverwork($job_id)
   {
	     $job_id=base64_decode($job_id);
	   
	  
	   if(!is_numeric($job_id))
	  {
		  echo '<script>
		 window.history.go(-1);	
		 </script>';
		  
	  }
	   //echo "deliver work ctrl"; die;
	   $gid=$this->session->userdata('gardner_id');
		$data['job_details']=$this->GardnerModel->get_job_details($job_id);
		$data['reviewed']=$this->GardnerModel->chek_grd_review($job_id,$gid);
		
		$this->load->model('UserModel');
		$data['rev_details']=$this->UserModel->get_user_review($job_id);
		
	    //echo "<pre>"; print_r($data['job_details']); die;
	     $data['main_content']="gardners/review_page";
		$this->load->view('layout/template',$data);
	   
   }
   
   public function deliveredwork($job_id)
   {
	   
	   //echo "deliver work ctrl"; die;
	   $id=$this->session->userdata('gardner_id');
		$this->GardnerModel->deliveredwork($job_id,$id);
	    redirect('gindex');
	   
   }
  
  


























   
































































































































   public function settings(){
		$id=$this->session->userdata('gardner_id');
	   	$data['info']=$this->GardnerModel->get_gardner_setting($id);
	   	//print_r($data);
	   	//die();
	   	$data['main_content']='gardners/settings';
	   	$this->load->view('layout/template',$data);
   }
  
    public function updatesettings(){


		$file_name ='';
    	$this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');    
        $this->form_validation->set_rules('location', 'Address', 'required');	
        $this->form_validation->set_rules('phone', 'Phone', 'required');
		$this->form_validation->set_rules('description', 'Something About You', 'required');
		$this->form_validation->set_rules('lat', 'Location you have selected is not valid  , Please select location from dropdown list this, ', 'required');
		 //$this->form_validation->set_rules('password', 'Password', 'required|matches[confirm_password]');
		 
        
        //$this->form_validation->set_rules('phone', 'phone', 'required|regex_match[/^[0-9]{11}$/]');
		
		if($this->form_validation->run() == false){
			$this->session->set_flashdata('error', validation_errors());

          //redirect('user/register',$data);
           redirect('settings');
		}
		else{


			$file_name ='';

			if(isset($_FILES['picture']['name'])){
					
					$config['upload_path']='./assets/img';
					$config['allowed_types']='jpg|jpeg|png';
					
					$this->load->library('upload',$config);
					
					if($this->upload->do_upload('picture')){
						$file_name = $this->upload->data('file_name');

					}
				}	
					



			if($file_name==''){

			$data = array(
				'f_name' => $this->input->post('first_name'),
				'l_name' => $this->input->post('last_name'),
				'location' => $this->input->post('location'),
				'phone' => $this->input->post('phone'),
				'description' => $this->input->post('description'),
				'lat' => $this->input->post('lat'),
				'log' => $this->input->post('lng')
				//'picture' => $file_name
				
			    );
		   }
		   else{

		   		$data = array(
				'f_name' => $this->input->post('first_name'),
				'l_name' => $this->input->post('last_name'),
				'location' => $this->input->post('location'),
				'phone' => $this->input->post('phone'),
				'description' => $this->input->post('description'),
				'picture' => $file_name,
				'lat' => $this->input->post('lat'),
				'log' => $this->input->post('lng')
				
			    );

		   }


			$id=$this->session->userdata('gardner_id');
		if ($user = $this->GardnerModel->update_gardner($id,$data)) {
			//$this->session->set_userdata('user_id', $user);
			$this->session->set_flashdata('success', "Your Settings Updated Successfully");
			//die("selected");
		redirect('settings');
		}
		}

    }



	public function jobdetailpage($id){
     $id=base64_decode($id);
	  if(!is_numeric($id))
	  {
		  echo '<script>
		 window.history.go(-1);	
		 </script>';
		  die;
	  }
	  
	  
		$gid=$this->session->userdata('gardner_id');
	  $data['details']=$this->GardnerModel->jobdetailpage($id);
	  $data['status']=$this->GardnerModel->appliedornot($id,$gid);
	  //print_r($data);
	 // die();
      $data['main_content']="gardners/job_detail_page";
      $this->load->view('layout/template',$data);
	}

	public function applyforjob($jobid){
		$cost=$this->input->post('cost');
		$gid=$this->session->userdata('gardner_id');
		$description=$this->input->post('description');
		$data = array(
			'job_id' => $jobid,
			'gardner_id' => $gid,
			'cost' => $cost,
			'description' => $description
			 );
		$this->GardnerModel->applyforjob($data,$gid);
		
		$this->session->set_flashdata('success',"You have applied for this job Successfully!");
		
		//sending notification on web 
			$notify_data=array(
			"description"=>"Someone have applied on your job",
			"job_id" => $jobid
			);
			
			$this->GardnerModel->notify('jobapplied',0,$gid,$notify_data);
		
		redirect('gindex');

	}

	public function historydetailpage($id)
    {
		 $id=base64_decode($id);
		 if(!is_numeric($id))
	  {
		  echo '<script>
		 window.history.go(-1);	
		 </script>';
		  die;
	  }
	  $gid=$this->session->userdata('gardner_id');
	  $data['reviewed']=$this->GardnerModel->chek_grd_review($id,$gid);
    	$data['job_details']=$this->GardnerModel->historydetailpage($id);
    	$data['job_review']=$this->GardnerModel->get_job_user_review($id);
    	//echo'<pre>';print_r($data);die;
    	$data['main_content']='gardners/history_detail_page';
    	$this->load->view('layout/template',$data);
    }


	public function jobreview($id)
   {
	 $id=base64_decode($id);
	 if(!is_numeric($id))
	  {
		  echo '<script>
		 window.history.go(-1);	
		 </script>';
		  die;
	  }
   	$data['details']=$this->GardnerModel->jobreview($id);
    $data['main_content']='gardners/review_page';
    $this->load->view('layout/template',$data);

    }

	 public function savereview(){

    $this->form_validation->set_rules('rating', 'Rating', 'required');
   	$this->form_validation->set_rules('review', 'Review', 'required');
   


   	if($this->form_validation->run()==false){
			$this->session->set_flashdata('error', validation_errors());
           
           redirect('gindex');
		}
		else{

    	$rating=$this->input->post('rating');
    	$review=$this->input->post('review');
    	$job_id=$this->input->post('job_id');
    	$gid=$this->session->userdata('gardner_id');
    	
    	$data = array(
    		'job_id' =>$job_id , 
    		'gardner_id' =>$gid , 
    		'review' =>$review , 
    		'rates' =>$rating 

    	);
    	$this->GardnerModel->savereview($data);
    	$this->session->set_flashdata('success',"You have reviewed Successfully");
		 redirect('gindex');
        }
	}








}	