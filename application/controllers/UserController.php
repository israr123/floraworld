<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserController extends MY_Controller {

function __construct()
	{
		parent::__construct();
		$this->checkuserlogin();
		//$this->load->model('admin/ArticleModel');
		$this->load->model('UserModel');
		$this->load->model('GardnerModel');
		
		//die("hi");
	}
	
	

	public function index()
	{
		//echo "welcome User!";
		$lat=$lng=0;
		$userid=$this->session->userdata('user_id');
		$myinfo=$this->UserModel->get_user_setting($userid);
		$lat=$myinfo[0]->lat;
		$lng=$myinfo[0]->log;
		$data['feeds']=$this->UserModel->get_wall_feeds($userid);
		$post_idies=array();
		foreach($data['feeds'] as $feed)
		{
			$post_idies[]=$feed->id;
		}
		$this->session->set_userdata("post_idies",$post_idies);
		
		$data['users']=$this->UserModel->get_users_not_followed_yet();
		$this->load->model('GardnerModel'); //loading model 
		$data['gardners']=$this->GardnerModel->get_near_by_gardners($lat,$lng,100);
		//echo "<pre>"; print_r($data['gardners']); die;
		$data['main_content']="users/index";
		$this->load->view('layout/template',$data);


	}
	function add_likes()
	{
		$post_id=str_replace("like","",$this->input->post('post_id'));
		$user_id= $this->session->userdata('user_id');;
		echo $this->UserModel->add_likes($post_id,$user_id);	
		
	}

	public function profile(){
    
	$id=$this->session->userdata('user_id');
	$data['user']=$this->UserModel->viewuserprofileprivate($id);
	$data['posts']=$this->UserModel->getuserposts($id);

	$data['info']=$this->UserModel->get_user_setting($id);
	//echo "<pre>"; print_r($data['info']); die;
		$id=$this->session->userdata('user_id');
		$this->load->model('HomeModel');
		$data['reviews']=$this->HomeModel->get_user_review($id);
		$total_reviews=sizeof($data['reviews']);
     
     if($total_reviews){

		$count=0;
		foreach ($data['reviews'] as $review){
			$count+=$review->rates;
		  }
		$total_rates=$count;
		$data['avgreview']=$total_rates/$total_reviews;
		
		}
		else{
			$data['avgreview']="";

		}


		$data['main_content']="users/profile";
		$this->load->view('layout/template',$data);



	
    }
/* 
	  $data['job_history']= $this->UserModel->user_job_history($userid);
		$data['new_jobs']=$this->UserModel->get_new_jobs($userid);
		$data['running_jobs']=$this->UserModel->get_running_jobs($userid);
		$data['applied_jobs']=$this->UserModel->applied_jobs($userid); */
		
	 public function get_ajax_job_history(){
	   	$userid=$this->session->userdata('user_id');
	   $jobs=$this->UserModel->user_job_history($userid);
	  //echo "<pre>"; print_r($jobs); die;
    	echo '<div class="job-listing mt-5 table-responsive">';
                
			 if(!empty($jobs)):
				echo '<table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Date</th>
                      <th scope="col">Gardener</th>
                      <th scope="col">Description</th>
                      <th scope="col">Budget</th>
                    </tr>
                  </thead>
                  <tbody>';
				  foreach($jobs as $job): 
                    echo '<tr>
		
                      <td style="width: 12%;">'.date('d-m-Y',strtotime( $job->post_date)).'</td>
                      <td><a href="gardner/'.$job->username.'"><img src="'.base_url('assets/img/').$job->picture.'" class="rounded" height="50px"></a></td>
                      <td><a href="'.base_url('myhistory_detail_page/'.base64_encode($job->id)).'">'.$job->job_description.'</a></td>
                      <td>$'.$job->cost.'</td>
                    </tr>';
                    endforeach; 
                 echo '</tbody>
                </table>';
				else: echo "There is no Job Available right now !"; endif; 
				
              echo '</div>';
    }  
	
		
	 public function get_ajax_my_jobs(){
	   	$userid=$this->session->userdata('user_id');
	   $jobs=$this->UserModel->get_running_jobs($userid);
	  //echo "<pre>"; print_r($jobs); die;
    	echo '<div class="job-listing mt-5 table-responsive">';
                
			 if(!empty($jobs)):
				echo '<table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Date</th>
                      <th scope="col">Gardener</th>
                      <th scope="col">Description</th>
                      <th scope="col">Budget</th>
                      <th scope="col">Action</th> 
                    </tr>
                  </thead>
                  <tbody>';
				  foreach($jobs as $job): 
                    echo '<tr>
					
                      <td style="width: 12%;"><span class="time" title="'.$job->post_date.'">'.$job->post_date.'</span></td>
                      <td><a href="gardner/'.$job->username.'"><img src="'.base_url('assets/img/').$job->picture.'" class="rounded" height="50px"></a></td>
                      <td><a href="'.base_url('review_page/'.base64_encode($job->id)).'">'.$job->job_description.'</a></td>
                      <td>$'.$job->job_cost.'</td>
					  <td><div class="buttonsets">';
                       if($job->job_status==3){
                          echo '<a class="btn themebutton" href="'.base_url('review_page/'.base64_encode($job->id)).'">Complete Job</a> </div></td>';
					   } else{
					   	echo "Job is Running";
					   }
                    echo '</tr>';
                    endforeach; 
                 echo '</tbody>
                </table>';
				else: echo "There is no Job Available right now !"; endif; 
				
              echo '</div>';
    }  
	
public function get_ajax_new_jobs(){
	   	$userid=$this->session->userdata('user_id');
	   $jobs=$this->UserModel->get_new_jobs($userid);
	 // echo "<pre>"; print_r($jobs); die;
    	echo '<div class="job-listing mt-5 table-responsive">';
                
			 if(!empty($jobs)):
				echo '<table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Date</th>
                      <th scope="col">Description</th>
                      <th scope="col">Budget</th>
					  
                    </tr>
                  </thead>
                  <tbody>';
				  foreach($jobs as $job): 
                    echo '<tr>
					
                      <td style="width: 12%;"><span class="time" title="'.$job->post_date.'">'.$job->post_date.'</span></td>
                     
                      <td><a href="'.base_url().'ucurrent_d_page/'.base64_encode($job->id).'">'.$job->job_description.'</a></td>
                      <td>$'.$job->cost.'</td>
                    </tr>';
                    endforeach; 
                 echo '</tbody>
                </table>';
				else: echo "There is no Job Available right now !"; endif; 
				
              echo '</div>';
    }  
	
public function get_ajax_applied_jobs(){
	   	$userid=$this->session->userdata('user_id');
	   $jobs=$this->UserModel->applied_jobs($userid);
	  //echo "<pre>"; print_r($jobs); die;
    	echo '<div class="job-listing mt-5 table-responsive">';
                
			 if(!empty($jobs)):
				echo '<table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Date</th>
                      <th scope="col">Gardener</th>
                      <th scope="col">Description</th>
                      <th scope="col">Bid Rate</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>';
				  foreach($jobs as $job): 
                    echo '<tr id="'.$job->job_id.'">
					
                      <td style="width: 12%;"><span class="time" title="'.$job->post_date.'">'.$job->post_date.'</span></td>
                      <td><a href="'.base_url().'gardner/'.$job->username.'"><img src="'.base_url('assets/img/').$job->picture.'" class="rounded" height="50px"></a></td>
                      <td><a href="'.base_url().'uaplied_d_page/'.base64_encode($job->job_id).'/'.base64_encode($job->gardner_id). '">'.$job->description.'</a></td>
                      <td>$'.$job->cost.'</td>
					   <td>
					  <form action="'.base_url('accept_job').'" method="post" >
						  <div class="buttonsets">
							 <button onClick="rejectjob('.$job->job_id.','.$job->gardner_id.');" type="button" class="btn  btn-grey">Reject</button>
							  <button type="submit" class="btn themebutton">Accept</button>
							  <input type="hidden" value="'.$job->job_id.'" name="job_id">
							  <input type="hidden" value="'.$job->gardner_id.'" name="gardner_id">
							  
						  </div>
						</form>  
					 </td>
                    </tr>';
                    endforeach; 
                 echo '</tbody>
                </table>';
				else: echo "There is no Job Available right now !"; endif; 
				
              echo '</div>';
    }  



	public function do_ajax_reject_job($job_id,$gid)
   {
	   $this->UserModel->reject_job($job_id,$gid);
   }
   


































































































































































	public function settings(){

		$id=$this->session->userdata('user_id');
	   	$data['info']=$this->UserModel->get_user_setting($id);
	   	//print_r($data);
	   	//die();
	   	$data['main_content']='users/settings';
	   	$this->load->view('layout/template',$data);
    }

    public function updatesettings(){

    	$this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        
        $this->form_validation->set_rules('phone', 'Phone', 'required');
        $this->form_validation->set_rules('location', 'Address', 'required');
		$this->form_validation->set_rules('lat', 'Location you have selected is not valid  , Please select location from dropdown list this, ', 'required');
		
		//$this->form_validation->set_rules('description', 'Something About You', 'required');
		
		 //$this->form_validation->set_rules('password', 'Password', 'required|matches[confirm_password]');
		 
        
        //$this->form_validation->set_rules('phone', 'phone', 'required|regex_match[/^[0-9]{11}$/]');
		
		if($this->form_validation->run()==false){
			$this->session->set_flashdata('error', validation_errors());
            //redirect('user/register',$data);
           redirect('mysettings');
		}
		else{
				$file_name ='';

			if(isset($_FILES['picture']['name'])){
					
					$config['upload_path']='./assets/img';
					$config['allowed_types']='jpg|jpeg|png';
					
					$this->load->library('upload',$config);
					
					if($this->upload->do_upload('picture')){
						$file_name = $this->upload->data('file_name');
//die('1');
					}
				}	
					





				if($file_name==''){

				$data = array(
				'f_name' => $this->input->post('first_name'),
				'l_name' => $this->input->post('last_name'),
				'location' => $this->input->post('location'),
				'phone' => $this->input->post('phone'),
				'description' => $this->input->post('description'),
				//'picture' => $file_name,
				'lat' => $this->input->post('lat'),
				'log' => $this->input->post('lng')
				
			    );
			} 
			else{

				$data = array(
				'f_name' => $this->input->post('first_name'),
				'l_name' => $this->input->post('last_name'),
				'location' => $this->input->post('location'),
				'phone' => $this->input->post('phone'),
				'description' => $this->input->post('description'),
				'picture' => $file_name,
				'lat' => $this->input->post('lat'),
				'log' => $this->input->post('lng')
			
			    );

			}

			$id=$this->session->userdata('user_id');
		if ($user = $this->UserModel->update_user($id,$data)) {
			//$this->session->set_userdata('user_id', $user);
			$this->session->set_flashdata('success', "Your Settings Updated Successfully");
			//die("selected");
			redirect('mysettings');
		}
		}



    }

   
    
	public function gardnerregister(){
    	//die();
    $data['main_content']='gardners/becomegardner';
    $this->load->view('layout/template',$data);

	
    }


    public function buying(){

    	$userid=$this->session->userdata('user_id');
    	
   	    $data['job_history']= $this->UserModel->user_job_history($userid);
		$data['new_jobs']=$this->UserModel->get_new_jobs($userid);
		$data['running_jobs']=$this->UserModel->get_running_jobs($userid);
		$data['applied_jobs']=$this->UserModel->applied_jobs($userid);
		
		//echo $this->db->last_query(); die;
       //echo "<pre>"; print_r($data['new_jobs']); die;
    	$data['main_content']='users/buying';
    	$this->load->view('layout/template',$data);

    }
   public function acceptjob ()
   { 

		$gardner_id= $this->input->post('gardner_id');
		$job_id= $this->input->post('job_id');
		
	      $data['accepted']=$this->UserModel->accept_job($job_id,$gardner_id);
	 
	 //$this.notify(  'type'  ,  to_id eg. grd_id 0 for all , from eg my_id , data )
			
			$notify_data=array(
			"description"=>"Your job have been accepted.",
			"job_id" => $job_id,
			"gardner_id" => $gardner_id
			);
			
			$this->GardnerModel->notify('jobaccepted',0,0,$notify_data);
       redirect('buying');	  

   }
   
   public function user_job_history(){

   	$id=$this->session->userdata('user_id');
   	$this->UserModel->user_job_history($id);

   }


   public function postajob(){


   	
   	$this->form_validation->set_rules('title', 'Title', 'required');
   	$this->form_validation->set_rules('description', 'Description', 'required');
   	$this->form_validation->set_rules('cost', 'Budget', 'required');
   	$this->form_validation->set_rules('work_date', 'Work Date', 'required');
   	$this->form_validation->set_rules('lat', 'Location you have selected is not valid this, ', 'required');

   	$this->form_validation->set_rules('lat', 'Location you have selected is not valid this, ', 'required');


     $now= $this->input->post('work_date');
   	 $Date1 = strtotime($now);
     $Date2 = strtotime(date('Y-m-d'));
  
	if( $Date1 < $Date2){
	$this->form_validation->set_rules('date', 'Selected date is wrong please choose it correctly, this', 'required');
	}
	
   
   //	$this->form_validation->set_rules('picture1', 'Picture1', 'required');


   	if($this->form_validation->run()==false){
			$this->session->set_flashdata('error', validation_errors());
            //redirect('user/register',$data);
           redirect('buying');
		}
		else{  

			$file_name1=$file_name2=$file_name3=$file_name4="";
		
			$config['upload_path']='./assets/img';
					$config['allowed_types']='jpg|jpeg|png';
					
					$this->load->library('upload',$config);
			  if(isset($_FILES['picture1']['name'])){
					
					if($this->upload->do_upload('picture1')){
						$file_name1 = $this->upload->data('file_name');

					}
				}	
					
					if(isset($_FILES['picture2']['name'])){
					
					if($this->upload->do_upload('picture2')){
						$file_name2 = $this->upload->data('file_name');

					}
				}	
					
					if(isset($_FILES['picture3']['name'])){
					
					if($this->upload->do_upload('picture3')){
						$file_name3 = $this->upload->data('file_name');

					}
				}	
					
					if(isset($_FILES['picture4']['name'])){
					
					if($this->upload->do_upload('picture4')){
						$file_name4 = $this->upload->data('file_name');

					}
				}	
					//echo $file_name1."<br>".$file_name2."<br>".$file_name3."<br>".$file_name4;die;
					
			$job_lat=$this->input->post('lat');
			$job_lng=$this->input->post('lng');

				if(empty($job_lat) || empty($job_lng )){
					
				$job_lat=0;
				$job_lng=0;
				}
			
			$id=$this->session->userdata('user_id');
			$data = array(
				'job_title' => $this->input->post('title'),
				'job_description' => $this->input->post('description'),
				'cost' => $this->input->post('cost'),
				'location' => $this->input->post('location'),
				'lat' => $job_lat,
				'log' => $job_lng,
				'expire_date' => $this->input->post('work_date'),
				'post_date' => date("Y-m-d"),
				'job_picture1' => $file_name1,
				'job_picture2' => $file_name2,
				'job_picture3' => $file_name3,
				'job_picture4' => $file_name4,
				'job_status' => 0,
				'user_id' => $id
				
			);
			
		if ($newjobid = $this->UserModel->postajob($data)) {
			//$this->session->set_userdata('user_id', $user);
			$this->session->set_flashdata('success', "Your New Job Posted Successfully");

			//die("selected");
			//$this.notify(  'type'  ,  to_id eg. grd_id 0 for all , from eg my_id , data )
			
			$notify_data=array(
			"description"=>"New Job has been posted Near to you",
			"job_lat" => $job_lat,
			"job_lng" => $job_lng,
			"job_id" => $newjobid
			);
			
			//print_r($notify_data);
			//die("helo");
			
			$this->GardnerModel->notify('newjob',0,$id,$notify_data);


			redirect('buying');
		   }
		}


    }

    public function historydetailpage($id)
    {
        $job_id=base64_decode($id);
		if(!is_numeric($job_id))
	  {
		  echo '<script>
		 window.history.go(-1);	
		 </script>';
		  die;
	  }
        //die();
	//echo <pre>''print_r($data); die();
        $data['details']=$this->UserModel->jobreview($job_id);
   	    if ($data['details']) {

   		$this->load->model('GardnerModel');
   		$data['rev_details']=$this->GardnerModel->get_grd_review($job_id);
   		
    	$data['ex_details']=$this->UserModel->historydetailpage($job_id);

    
    		$data['main_content']='users/history_detail_page';
    	    $this->load->view('layout/template',$data);
    	} else{

    		redirect('buying');
    	}
    	//echo <pre>''print_r($data); die();
    	//$data['main_content']='users/history_detail_page';
    	//$this->load->view('layout/template',$data);
    }
	
	

	
	
	

   public function jobreview($job_id)
   {


   	$job_id=base64_decode($job_id);
	if(!is_numeric($job_id))
	  {
		  echo '<script>
		 window.history.go(-1);	
		 </script>';
		  die;
	  }

    	$data['details']=$this->UserModel->jobreview($job_id);
   	    if ($data['details']) {
   		$this->load->model('GardnerModel');
   		$data['rev_details']=$this->GardnerModel->get_grd_review($job_id);
   		//echo <pre>''print_r($data); die();
    	$data['main_content']='users/review_page';
    	$this->load->view('layout/template',$data);

				
			
		} else {
					redirect("buying");

				}

	
    }
	
   
    public function savereview(){

    $this->form_validation->set_rules('rating', 'Rating', 'required');
   	$this->form_validation->set_rules('review', 'Review', 'required');
   
   	if($this->form_validation->run()==false){
			$this->session->set_flashdata('error', validation_errors());
            //redirect('user/register',$data);
           redirect('job_review');
		}
		else{
    	$rating=$this->input->post('rating');
    	$review=$this->input->post('review');
    	$job_id=$this->input->post('job_id');
    	$user_id=$this->session->userdata('user_id');
    	$data = array(
    		'job_id' =>$job_id , 
    		'review' =>$review , 
    		'rates' =>$rating,
    		'user_id'=>$user_id
    	);
    	$this->UserModel->savereview($data);
    	$this->UserModel->complete_order($job_id);
         $this->session->set_flashdata('success',"You have completed job Successfully");
		 redirect('buying');
        }
	}
    
    public function jobdetailpage($id){
    	$id=base64_decode($id);
		if(!is_numeric($id))
	  {
		  echo '<script>
		 window.history.go(-1);	
		 </script>';
		  die;
	  }
	  	$data['details']=$this->UserModel->jobdetailpage($id);
	  	//echo <pre>''print_r($data); die();
	  	if ($data['details']) {

				 $data['main_content']="users/job_detail_page";
     			 $this->load->view('layout/template',$data);
			
		} else {
			redirect("buying");

		}
	  
     
	}

	public function ucurrentdetailpage($id){
		 $id=base64_decode($id);
		if(!is_numeric($id))
	  {
		  echo '<script>
		 window.history.go(-1);	
		 </script>';
		  die;
	  }
		
		$data['details']=$this->UserModel->ucurrentdetailpage($id);
		//echo <pre>''print_r($data); die();
		if ($data['details']) {

				$data['main_content']="users/ucurrent_detail_page";
		        $this->load->view('layout/template',$data);
			
		} else {
			redirect("buying");

		}
		
	     
	  

	}

	public function uaplieddetailpage($id,$gid){
		//die("hello");
			 $gid=$this->uri->segment(3);
			 $id=base64_decode($id);
			 if(!is_numeric($id))
	  {
		  echo '<script>
		 window.history.go(-1);	
		 </script>';
		  die;
	  }
		//die();
		$data['details']=$this->UserModel->uaplieddetailpage($id);
		$data['gardner_id']=base64_decode($gid);
		//echo '<pre>'; print_r($data); die();
		
		

		if ($data['details'] and $data['gardner_id'] ) {
			
				
	            $data['main_content']='users/uaplied_detail_page';
	            $this->load->view('layout/template',$data);
			
		} else {

			redirect("buying");

		}
	  
	  
		

	}


	












}	