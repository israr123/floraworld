<?php
class SocialModel extends MY_Model {


 public function follow_up($following_id,$userid){
    $data=array("following_id"=>$following_id,"follower_id"=>$userid);
	$this->db->insert('follow',$data);
	//sending notification to user	
	$username=$this->get_username_from_id($userid);
	$desc=$username." is following you now.";
			
			$indata=array(
						array(
							"description"=>$desc,
							"user_id"=>$following_id
							)
						);
		$this->submit_notification('user',$indata);
		//sending app notification 
		if($deice_id=$this->get_keydeviceid_by_id('users',$following_id))
					{
						$this->push_notification($deice_id,$desc);
					}
	
	
}
public function savesocial($data){

	$this->db->insert('posts',$data);
	$userid=$this->session->userdata('user_id');
	$username=$this->get_username_from_id($userid);
	$follower_idies=$this->get_followers_idie($userid);
	
	//echo "<pre>"; print_r($follower_idies); die;
	//sending notification t user	
	
	if(!empty($follower_idies)){
		$desc=$username." have created new post.";
			
			$indata=array();
			foreach($follower_idies as $follower)
			{
				$indata[]=array("user_id"=>$follower->follower_id,"description"=>$desc);
			}
			
		$this->submit_notification('user',$indata);
	}
	return true;
}

 public function submit_notification($table,$data){
		if($table=="grd")
		$this->db->insert_batch('gardner_notifications', $data);
		if($table=="user")
		$this->db->insert_batch('user_notifications', $data);
		return true;

	}
	
   public function get_username_from_id($userid)
	{
		
		$this->db->where('id',$userid);
		
		return $this->db->get('users')->row()->username;
		
		
	}

   public function get_followers_idie($userid)
	{
		$this->db->select('follower_id');
		$this->db->where('following_id',$userid);
		
		return $this->db->get('follow')->result();
		
		
	}




	
}
