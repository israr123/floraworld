<?php
class GardnerModel extends My_Model {


public function get_my_jobs($gardner_id,$offset1,$offset2)
{
	/*
	$this->db->select('jb.post_date,users.username,jb.id,users.picture,jb.job_description,aj.cost as job_cost');
	
	$this->db->from('jobs as jb');
	$this->db->join('users', 'users.id = jb.user_id');
	$this->db->join('applied_jobs as aj', 'jb.id = aj.job_id');
	$this->db->where('jb.assigned_to','aj.gardner_id');
	$this->db->where('jb.job_status',1);
	$this->db->or_where('jb.job_status',3);
	$this->db->where('jb.assigned_to',$gardner_id);
	

	return 

	$this->db->query("SELECT `jb`.`post_date`, `users`.`username`, `jb`.`id`, `users`.`picture`, `jb`.`job_description`, `aj`.`cost` as `job_cost`
		FROM `jobs` as `jb`
		JOIN `users` ON `users`.`id` = `jb`.`user_id` 
		JOIN `applied_jobs` as `aj` ON `jb`.`id` = `aj`.`job_id` 
		where `jb`.`assigned_to` =".$gardner_id."
		AND `jb`.`assigned_to` = `aj`.`gardner_id`
		AND `jb`.`job_status` = 1 
		OR `jb`.`job_status` = 3 
		 order by jb.id desc ")->result();

	return $this->db->query("SELECT jb.post_date, users.username,jb.id,users.picture, jb.job_description , (SELECT cost FROM applied_jobs where gardner_id=".$gardner_id." and job_id=jb.id) as job_cost FROM jobs as jb,users where jb.assigned_to=".$gardner_id." and jb.user_id=users.id AND jb.job_status=1 OR jb.job_status=3 
      ")->result();
*/
	//$query = $this->db->get();
	//echo $this->db->last_query(); die;
   //return $query->result();

   
   return $this->db->query("
   SELECT `users`.`username` , `users`.`picture`, `users`.`f_name`, `users`.`l_name`, `users`.`phone` , `users`.`email`, `users`.`lat`, `users`.`log`, `users`.`location`, `applied_jobs`.`cost` as job_cost , `jobs`.`post_date`, `jobs`.`job_status`,`jobs`.`job_picture1`,`jobs`.`job_picture2`,`jobs`.`job_picture3`,`jobs`.`job_picture4` , `jobs`.`job_description` , `jobs`.`id` FROM `applied_jobs` INNER JOIN `jobs` ON (`applied_jobs`.`job_id` = `jobs`.`id`) AND (`jobs`.`assigned_to` = `applied_jobs`.`gardner_id`) INNER JOIN `users` ON (`users`.`id` = `jobs`.`user_id`) WHERE (`jobs`.`assigned_to` =".$gardner_id." AND jobs.job_status=1 OR jobs.job_status=3 ) group by jobs.id limit 
   ".$offset1.",".$offset2)->result();
	   
   }
public function get_previous_jobs($gardner_id,$offset1,$offset2)
{
	/* 
	$this->db->select('jh.comp_date as complition_date,users.username,users.picture,jobs.job_description,jobs.id,aj.cost as job_cost');
	$this->db->from('job_history jh');
	//$this->db->from('applied_jobs aj');
	$this->db->join('jobs', 'jobs.id = jh.job_id');
	$this->db->join('users', 'users.id = jh.user_id');
	$this->db->join('applied_jobs as aj', 'aj.job_id = jobs.id');
	$this->db->where('aj.gardner_id ','jobs.assigned_to');
	$this->db->where('jobs.job_status',2);
	$this->db->where('jobs.assigned_to',$gardner_id);
*/
	return $this->db->query("SELECT `jh`.`comp_date` as `complition_date`, `users`.`username`, `users`.`picture`, `users`.`f_name`, `users`.`l_name`, `users`.`phone`, `users`.`email`, `users`.`log`, `users`.`lat`, `users`.`location`, `jobs`.`job_description`, `jobs`.`id`, `jobs`.`job_picture1`, `jobs`.`job_picture2`, `jobs`.`job_picture3`, `jobs`.`job_picture4`, `aj`.`cost` as `job_cost` FROM `job_history` `jh` JOIN `jobs` ON `jobs`.`id` = `jh`.`job_id` JOIN `users` ON `users`.`id` = `jh`.`user_id` JOIN `applied_jobs` as `aj` ON `aj`.`job_id` = `jobs`.`id` WHERE `aj`.`gardner_id` = `jobs`.`assigned_to` AND `jobs`.`job_status` = 2 AND `jobs`.`assigned_to` = ".$gardner_id." order by complition_date DESC limit ".$offset1.",".$offset2 )->result();

	//$query = $this->db->get();
		//echo $this->db->last_query(); die;
   return $query->result();
}

public function get_new_jobs($gardner_id,$offset1,$offset2)
{
	/* $this->db->select('jb.post_date,jb.id,u.username,u.picture,jb.job_description,jb.cost as job_cost ,111.111 *
    DEGREES(ACOS(COS(RADIANS(jb.lat))
         * COS(RADIANS(grd.lat))
         * COS(RADIANS(jb.log - grd.log))
         + SIN(RADIANS(jb.lat))
         * SIN(RADIANS(grd.lat)))) AS distance_in_km');
	$this->db->from('jobs as jb,gardners as grd');
	$this->db->join('users as u', 'u.id = jb.user_id');
	$this->db->having('distance_in_km < 10');
	$this->db->where('grd.id', $gardner_id);
	$query="SELECT job_id FROM applied_jobs WHERE gardner_id =".$gardner_id;
	$this->db->where_not_in('jb.id',$query );

	$this->db->group_by('jb.id');
	 */
	
	$query = $this->db->query("SELECT `jb`.`post_date`,`jb`.`job_picture1`,`jb`.`job_picture2`,`jb`.`job_picture3`,`jb`.`job_picture4`, `jb`.`id`, `u`.`username`, `u`.`picture`,`u`.`f_name`,`u`.`l_name`,`u`.`phone`,`u`.`email`,`u`.`log`,`u`.`lat`,`u`.`location`, `jb`.`job_description`, `jb`.`cost` as `job_cost`, 111.111 * DEGREES(ACOS(COS(RADIANS(jb.lat)) * COS(RADIANS(grd.lat)) * COS(RADIANS(jb.log - grd.log)) + SIN(RADIANS(jb.lat)) * SIN(RADIANS(grd.lat)))) AS distance_in_km FROM (`jobs` as `jb`, `gardners` as `grd`) JOIN `users` as `u` ON `u`.`id` = `jb`.`user_id` WHERE `grd`.`id` = ".$gardner_id." AND `jb`.`id` NOT IN(SELECT job_id FROM applied_jobs WHERE gardner_id =".$gardner_id.") AND `jb`.`id` NOT IN(SELECT job_id FROM ignored_jobs WHERE gardner_id =".$gardner_id.") AND `jb`.`job_status` = 0  GROUP BY `jb`.`id` HAVING `distance_in_km` < 100 order by jb.id desc limit ".$offset1.",".$offset2);
	//echo $this->db->last_query(); die;
   return $query->result();
   
   
}

public function get_near_by_gardners($lat=0,$lng=0,$dist=100){
	
	$this->db->select('f_name,l_name,username,picture,grd.id as gardner_id,lat,log,description,111.111 *
    DEGREES(ACOS(COS(RADIANS('.$lat.'))
         * COS(RADIANS(grd.lat))
         * COS(RADIANS('.$lng.' - grd.log))
         + SIN(RADIANS('.$lat.'))
         * SIN(RADIANS(grd.lat)))) AS distance_in_km');
	$this->db->from('gardners as grd');
	$this->db->limit('6');
	$this->db->having('distance_in_km <'.$dist);
	$query=$this->db->get();
	//echo $this->db->last_query(); die;
	return $query->result();
}


public function deliveredwork($job_id,$id){
	
	$this->db->query("update jobs set job_status=3  where id=".$job_id." ");
		//echo $this->db->last_query(); die;
		
		
		$gardner_id=$id;
		
		//sending notification t user	
			$desc="Gardener has delivered work.";
			$user_id=$this->get_user_id($job_id);
			
			
			$indata=array(
						array(
							"description"=>$desc,
							"job_id"=>$job_id,
							"user_id"=>$user_id
							)
						);
		$this->submit_notification('user',$indata);
		//sending app notification 
		if($deice_id=$this->get_keydeviceid_by_id('users',$user_id))
					{
						$this->push_notification($deice_id,$desc);
					}
	return true;
}

public function get_job_details($job_id){
	/*
	$this->db->select('jb.post_date,jb.id,jb.job_title,jb.job_status,u.username,u.picture,jb.job_description,jb.job_picture1,jb.job_picture2,jb.job_picture3,jb.job_picture4,aj.cost');
	$this->db->from("jobs as jb");	
	$this->db->join('users as u','u.id=jb.user_id');
	$this->db->join('applied_jobs as aj','jb.id=aj.job_id');
	$this->db->where('aj.gardner_id','aj.assigned_to');
	$this->db->where('jb.id',$job_id);
	$query=$this->db->get();
	*/
	return $this->db->query("SELECT `jb`.`post_date`, `jb`.`id`, `jb`.`job_title`, `jb`.`job_status`, `u`.`username`, `u`.`picture`, `jb`.`job_description`, `jb`.`job_picture1`, `jb`.`job_picture2`, `jb`.`job_picture3`, `jb`.`job_picture4`, `aj`.`cost` FROM `jobs` as `jb` JOIN `users` as `u` ON `u`.`id`=`jb`.`user_id` JOIN `applied_jobs` as `aj` ON `jb`.`id`=`aj`.`job_id` AND `aj`.`gardner_id` = `jb`.`assigned_to` AND `jb`.`id` = ".$job_id." ")->result();

	//echo $this->db->last_query(); die;
  // return $query->result();
}
public function get_user_id($job_id){
	
	$this->db->where('id',$job_id);
   return $this->db->get('jobs')->row()->user_id;
}
function count_notifications($user_id)
	{
		
		$this->db->where('gardner_id',$user_id);
		$this->db->where('status',0);
				
		return $this->db->count_all_results('gardner_notifications');
	 	
	}
	public function get_new_notifications($user_id)
{
		$this->db->select('description');
		$this->db->where('gardner_id',$user_id);
		$this->db->where('status <',2);
		$this->db->order_by('id','DESC');
   return $this->db->get('gardner_notifications',10)->result();
	
}

public function hide_notifications($user_id)
	{
		
		$this->db->where('gardner_id',$user_id);
		$this->db->where('status',0);
		$this->db->set('status', '1');		
		return $this->db->update('gardner_notifications');
	 	
	}






public function notify($type,$to,$from,$data)
    {
		if($type=="jobapplied")
		{
			
			$desc=$data['description'];
			$job_id=$data['job_id'];
			$user_id=$this->GardnerModel->get_user_id($job_id);
			
			$indata=array(
						array(
							"description"=>$desc,
							"job_id"=>$job_id,
							"user_id"=>$user_id
							)
						);
			
		 $inserted=$this->GardnerModel->submit_notification("user",$indata);
		 
		 //aap push notification started

		 if($deice_id=$this->get_keydeviceid_by_id('users',$user_id))
					{
						$this->push_notification($deice_id,$desc);
					}
					//end app
		}	
    
		if($type=="newjob")
		{
			$lat=$data['job_lat'];
			$lng=$data['job_lng'];
			$desc=$data['description'];
			$job_id=$data['job_id'];
			
			
			
			$nearby_gardners=$this->get_near_by_gardners($lat,$lng,100);
			//echo "<pre>"; print_r($nearby_gardners);die;
			if(!empty($nearby_gardners))
			{
				$indata=array();
				foreach($nearby_gardners as $grd)
				{
					$indata[]=array("gardner_id"=>$grd->gardner_id,"description"=>$desc,"job_id"=>$job_id);

					if($deice_id=$this->get_keydeviceid_by_id('gardners',$grd->gardner_id))
					{
						$this->push_notification($deice_id,$desc);
					}

				}
				 $inserted=$this->submit_notification("grd",$indata);
			}
		}	
		
		if($type=="jobaccepted")
		{
			$desc=$data['description'];
			$job_id=$data['job_id'];
			$gardner_id=$data['gardner_id'];
			
			$indata=array(
						array(
							"description"=>$desc,
							"job_id"=>$job_id,
							"gardner_id"=>$gardner_id
							)
						);
			if($deice_id=$this->get_keydeviceid_by_id('gardners',$grd->gardner_id))
					{
						$this->push_notification($deice_id,$desc);
					}

			 $inserted=$this->GardnerModel->submit_notification("grd",$indata);
		}
		
		
		
    	
    }































































































































































































	public function get_gardner_setting($id){

 	return $this->db->where('id',$id)->get('gardners')->result();;
 	}


	public function profile_get($id){

	//$this->db->where('id',$id)->get('gardners')->result();
    return $this->db->query("SELECT f_name,l_name,location,description,picture,phone,review,rates from gardners LEFT OUTER JOIN gardner_reviews on gardners.id=gardner_reviews.gardner_id where gardners.id=".$id." ")->result(); 
	}

	public function update_gardner($id,$data){

     $this->db->where('id',$id)->update('gardners',$data);
     return true;

	}


	public function jobdetailpage($id){

		//return $this->db->where('id',$id)->get('jobs')->result();
		  return $this->db->query("SELECT jobs.*, users.username  from jobs JOIN users on jobs.user_id=users.id
		       where jobs.id=".$id."")->result();
		 
		 
	}


	public function applyforjob($data,$gid){
		$this->db->insert('applied_jobs',$data);
		return true;


	}
	
	public function submit_notification($table,$data){
		if($table=="grd")
		$this->db->insert_batch('gardner_notifications', $data);
		if($table=="user")
		$this->db->insert_batch('user_notifications', $data);
		return true;

	}
	
	public function ignore_job($job_id,$gid){
		$data=array('job_id'=>$job_id,'gardner_id'=>$gid);
		$this->db->insert('ignored_jobs',$data);
		return true;

	}

	
	
	public function appliedornot($id,$gid){

		//return $this->db->query("SELECT applied_jobs.gardner_id from jobs RIGHT OUTER JOIN applied_jobs on applied_jobs.job_id=jobs.id AND applied_jobs.gardner_id=jobs.assigned_to")->result();
		return $this->db->query('select * from applied_jobs where gardner_id='.$gid.' AND job_id='.$id.' ')->result();
	}

	public function applied_jobs($gardner_id,$offset1,$offset2)
	{
	/* return $this->db->query("SELECT jb.post_date,aj.description,aj.cost,user.username,user.picture from users as user,applied_jobs as aj,jobs as jb where aj.gardner_id=".$gardner_id." and aj.job_id=jb.id and user.id=jb.user_id and jb.assigned_to=0")->result(); */
	
	return $this->db->query("SELECT jb.post_date,jb.id,jb.job_picture1,jb.job_picture2,jb.job_picture3,jb.job_picture4,user.f_name,user.l_name,user.phone,user.email,jb.job_description,aj.cost,user.username,user.picture from users as user,applied_jobs as aj,jobs as jb where aj.gardner_id=".$gardner_id." and aj.job_id=jb.id and user.id=jb.user_id and jb.job_status=0 and jb.id not in (select job_id from rejected_jobs where user_id = ".$gardner_id.") group by jb.id  order by aj.id DESC limit ".$offset1.",".$offset2)->result();
	
	}

	public function jobreview($id){

		//return $this->db->where('id',$id)->get('jobs')->result();
		  return $this->db->query("SELECT jobs.*, gardners.username  from jobs JOIN gardners on jobs.assigned_to=gardners.id
		       where jobs.id=".$id."")->result();
		 
		 
	}

	public function historydetailpage($id)
	{
		return $this->db->query("select aj.description,aj.apply_date,jb.job_description,jb.job_picture1,jb.job_picture2,jb.job_picture3,jb.job_picture4,jb.id,jb.job_title,aj.cost,jh.comp_date from jobs as jb,applied_jobs as aj,gardners as grd, job_history as jh where jh.job_id = jb.id AND jb.id=".$id." and jb.assigned_to=grd.id and aj.job_id=jb.id and aj.gardner_id=jb.assigned_to")->result();
		
	}
	
	public function savereview($data){

		$this->db->insert('gardner_reviews',$data);
		return true;
	}
	
	public function get_job_user_review($job_id)
	{
	
     $this->db->select('user.id,ur.rates,ur.review,ur.review_date,user.username,user.picture');
	
	$this->db->from('user_reviews as ur');
	$this->db->join('jobs as jb', 'jb.id = ur.job_id');
	$this->db->join('users as user', 'user.id = jb.user_id');
	
	$this->db->where('ur.job_id',$job_id);
	
	
	$query = $this->db->get();
	//echo $this->db->last_query(); die;
   return $query->result();
		
	}
	
	
	
	public function get_grd_review($job_id)
	{
	
     $this->db->select('gr.id,gr.rates,gr.review,gr.review_date,grd.username,grd.picture'); 
	
	$this->db->from('gardner_reviews as gr');
	$this->db->join('jobs as jb', 'jb.id = gr.job_id');
	$this->db->join('gardners as grd', 'grd.id = jb.assigned_to');
	
	$this->db->where('gr.job_id',$job_id);
	
	
	$query = $this->db->get();
	//echo $this->db->last_query(); die;
   return $query->result();
		
	}
	public function chek_grd_review($job_id,$grd_id)
	{
		$this->db->where('job_id',$job_id);
		$this->db->where('gardner_id',$grd_id);
		return $this->db->count_all_results('gardner_reviews');
		//echo $this->db->last_query(); die; 
		
	}
	
	
}
