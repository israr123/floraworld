<?php
class Email_model extends MY_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		//$this->load->database();
		$this->load->library('email');
    }
 

 function sendverificationemail($email,$email_verification_code,$app_verification_code){
  
  $config = Array(
     'protocol' => 'smtp',
     'smtp_host' => 'mail.host.com',
     'smtp_port' => 26,
     'smtp_user' => 'advert@Youremail.com', // change it to yours
     'smtp_pass' => '@thedabmedia', // change it to yours
     'mailtype' => 'html',
     'charset' => 'iso-8859-1',
     'wordwrap' => TRUE
  );
  $logo_html="<a class='navbar-brand' href='http://www.floraworld.com'><img class='img-responsive' src='http://www.floraworld.com/assets/img/logo1.png' height='170'></a>";
  
  $this->load->library('email', $config);
  $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
  $this->email->set_header('Content-type', 'text/html');
  $this->email->set_newline("\r\n");
  $this->email->from('advert@youremail.com', "The Media");
  $this->email->to($email);  
  $this->email->subject("The Floraworld Email Verification");
  $this->email->message("<br><br>Dear customer.! <br>Thank you for registration in our Website.<br>Please click on below URL or paste into your browser to verify your Email Address<br><br> http://www.floraworld.com/verify_email/".$email_verification_code."<br><br>To verify your Email Address on mobile app use this code: ".$app_verification_code."<br><b><br><br>".$logo_html."<br><br><br>Thanks\n
  http://www.floraworld.com<br>The Floraworld Team");
  if($this->email->send())
  {
	return true;
  }
  else
  {
	return false;
  }
  
 } 
 
 function sendEmailfornewpassword($email,$email_verification_code,$app_verification_code){
  
  $config = Array(
     'protocol' => 'smtp',
     'smtp_host' => 'mail.youremail.com',
     'smtp_port' => 26,
     'smtp_user' => 'advert@user.com', // change it to yours
     'smtp_pass' => '@password', // change it to yours
     'mailtype' => 'html',
     'charset' => 'iso-8859-1',
     'wordwrap' => TRUE
  );
  
  
  $this->load->library('email', $config);
  $this->email->set_newline("\r\n");
  $this->email->from('advert@floraworld.com', "floraworld");
  $this->email->to($email);  
  $this->email->subject("The Floraworld Reset Password");
  $this->email->message("Dear Member! \nPlease click on below URL or paste into your browser to reset your password .\n\n http://www.floraworld.com/verify_forget_password/".$email_verification_code."\n"."If your are using our mobile app use this code : ".$app_verification_code."\n\nThanks\n
  www.floraworld.com\nAdmin Team");
  if($this->email->send())
  {
	return true;
  }
  else
  {
	return false;
  }
  
 }
 
 function send_email($email,$email_subject,$email_message)
 {	
	  
	  
	  
  $config = Array(
     'protocol' => 'sendmail',
     'smtp_host' => 'mail.thedabmedia.com',
     'smtp_port' => 26,
     'smtp_user' => 'advert@thedabmedia.com', // change it to yours
     'smtp_pass' => '@thedabmedia', // change it to yours
     'mailtype' => 'html',
     'charset' => 'iso-8859-1',
     'wordwrap' => TRUE
	);
  
  
	  $this->load->library('email', $config);
	  $this->email->set_newline("\r\n");
	  $this->email->from('advert@floraworld.com', "floraworld.com");
	  $this->email->to($email);  
	  $this->email->subject($email_subject);
	  $this->email->message($email_message);
	  $this->email->set_mailtype("html");
	  if($this->email->send())
	  {
		return true;
	  }
	  else
	  {
		return false;
	  }
		echo $this->email->print_debugger();
 }
 
}
?>