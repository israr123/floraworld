<?php 

class ApiModel extends MY_Model{
	



	public function get_gardner_profile($gid){
		return $this->db->where('id',$gid)->get('gardners')->row();

	}

	public function get_user_profile($uid){
		return $this->db->where('id',$uid)->get('users')->row();

	}

	public function replace_user_device_token($uid,$rep){
		$this->db->where('id',$uid)->set('device_token',$rep)->update('users',$rep);
		return true;
	}

	public function replace_gardner_device_token($gid,$rep){
		$this->db->where('id',$gid)->set('device_token',$rep)->update('gardners',$rep);
		return true;
	}

	public function user_authenticate($device_token){
	return 
	$this->db->where('device_token',$device_token)->get('users')->row();;
	//echo $this->db->last_query(); die;
	

		
	}

	public function gardner_authenticate($device_token){
	return $this->db->where('device_token',$device_token)->get('gardners')->row();
		
	}

	public function check_user_username_exist($value){

		$this->db->where('username',$value)
		  ->or_where('email',$value);
		  $user=$this->db->count_all_results('users'); 
		  //echo $this->db->last_query(); die;
		  $this->db->where('username',$value)
		  ->or_where('email',$value);
		  $gardner=$this->db->count_all_results('gardners');
		  if($user) 
		  return true ;
		   elseif($gardner)
		  return true ;
		       else
		  return false ;
	//return $this->db->where('username',$username)->get('users')->row();
	}

	public function check_user_email_exist($value){

		$this->db->where('username',$value)
		  ->or_where('email',$value);
		  $user=$this->db->count_all_results('users'); 
		  //echo $this->db->last_query(); die;
		  $this->db->where('username',$value)
		  ->or_where('email',$value);
		  $gardner=$this->db->count_all_results('gardners');
		  if($user) 
		  return true ;
		   elseif($gardner)
		  return true ;
		       else
		  return false ;
	//return $this->db->where('email',$email)->get('users')->row();
	}

	public function check_gardner_username_exist($value){

		 $this->db->where('username',$value)
		  ->or_where('email',$value);
		  $user=$this->db->count_all_results('users'); 
		  //echo $this->db->last_query(); die;
		  $this->db->where('username',$value)
		  ->or_where('email',$value);
		  $gardner=$this->db->count_all_results('gardners');
		  if($user) 
		  return true ;
		   elseif($gardner)
		  return true ;
		       else
		  return false ;

	//return $this->db->where('username',$username)->get('gardners')->row();
	}

	public function check_gardner_email_exist($value){

		$this->db->where('username',$value)
		  ->or_where('email',$value);
		  $user=$this->db->count_all_results('users'); 
		  //echo $this->db->last_query(); die;
		  $this->db->where('username',$value)
		  ->or_where('email',$value);
		  $gardner=$this->db->count_all_results('gardners');
		  if($user) 
		  return true ;
		   elseif($gardner)
		  return true ;
		       else
		  return false ;
	//return $this->db->where('email',$email)->get('gardners')->row();
	}

	public function update_user_device_token($email,$device_token){
	$this->db->where('email',$email)->set('device_token',$device_token)->update('users');
		return true;
	}
	public function update_gardner_device_token($email,$device_token){
		//die();
	$this->db->where('email',$email)->set('device_token',$device_token)->update('gardners');
	
		return true;
	}

	public function update_user($device_token,$data){
    $this->db->where('device_token',$device_token)->update('users',$data);
     return true;
	}

	public function update_gardner($device_token,$data){
    $this->db->where('device_token',$device_token)->update('gardners',$data);
     return true;
	}

	public function get_userdata($device_token){
		return $this->db->where('device_token',$device_token)->get('users')->row();
	}
	

	public function get_gardnerdata($device_token){
		return $this->db->where('device_token',$device_token)->get('gardners')->row();
	}

	public function savesociall($id,$data){
		//print_r($data);

	 $this->db->insert('posts',$data);
	 //$this->db->last_query();
	$userid=$id;  //die('ji');
	$username=$this->get_username_from_id($userid);
	$follower_idies=$this->get_followers_idie($userid);
	
	//echo "<pre>"; print_r($follower_idies); die;
	//sending notification t user	
	if(!empty($follower_idies)){
		$desc=$username." have created new post.";
			
			$indata=array();
			foreach($follower_idies as $follower)
			{
				$indata[]=array("user_id"=>$follower->follower_id,"description"=>$desc);
			}
			
		$this->submit_notification('user',$indata);
	}
			
	return true;
}
public function get_username_from_id($userid)
	{
		
		$this->db->where('id',$userid);
		
		return $this->db->get('users')->row()->username;
		
		
	}

	public function get_followers_idie($userid)
	{
		$this->db->select('follower_id');
		$this->db->where('following_id',$userid);
		
		return $this->db->get('follow')->result();
		
		
	}

	public function user_count_all_reviews($user_id){


  $this->db->where('user_id',$user_id);
     
  return $this->db->count_all_results('user_reviews');
   
 
	}

	public function gardner_count_all_reviews($gid){


  $this->db->where('gardner_id',$gid);
     
  return $this->db->count_all_results('gardner_reviews');
   
 
	}

public function user_reviews($uid,$offset1,$offset2)
 {
  
  $this->db->where('user_id',$uid);
  $this->db->limit($offset1,$offset2);
  
     
  return $this->db->count_all_results('user_reviews');
   
 }

 public function gardner_reviews($gid,$offset1,$offset2)
 {
  
  $this->db->where('gardner_id',$gid);
  $this->db->limit($offset1,$offset2);
  
     
  return $this->db->count_all_results('gardner_reviews');
   
 }

 public function verify_gardner_by_app_verification_code($email,$code)
 {
  $this->db->where('email',$email);
  $this->db->where('app_verification_code',$code);
 $this->db->update('gardners',array('verified' => 1));
  return $this->db->affected_rows();
  //echo $this->db->last_query(); die; 
  
 }
 public function verify_user_by_app_verification_code($email,$code)
 {
  
  $this->db->where('app_verification_code',$code);
   $this->db->update('users',array('verified' => 1));
   return $this->db->affected_rows();
  //echo $this->db->last_query(); die; 
  
 }

 public function get_userdata_by_username($username){

 	 return $this->db->where('username',$username)->get('users')->row();	
 }

 public function get_gardnerdata_by_username($username){

 	return $this->db->where('username',$username)->get('gardners')->row();
 }

 public function get_previous_jobs_api($gid,$offset1,$offset2){


 	return $this->db->query("SELECT
    `jobs`.`id` AS `id`

    , `jobs`.`job_description`
    , `jobs`.`location`
    , `jobs`.`lat`
    , `jobs`.`log`
    , `jobs`.`job_picture1`
    , `jobs`.`job_picture2`
    , `jobs`.`job_picture3`
    , `jobs`.`job_picture4`
    , `job_history`.`comp_date`
    , `users`.`f_name` AS `user_fname`
    , `users`.`l_name` AS `user_lname`
    , `users`.`username` AS `user_username`
    , `users`.`picture` AS `user_picture`
    , `users`.`email` AS `user_email`
    , (SELECT count(`id`)  FROM `user_reviews` WHERE `user_id`= `users`.`id`) as user_total_reviews
    , (SELECT round(AVG(rates),0) FROM `user_reviews` WHERE `user_id`= `users`.`id`) as user_avg_reviews 
   
    , `gardners`.`f_name` AS `gardner_fname`
    , `gardners`.`l_name` AS `gardner_lname`
    , `gardners`.`username` AS `gardner_username`
    , `gardners`.`picture` AS `gardner_picture`
    , `gardners`.`email` AS `gardner_email`
    , `gardners`.`email` AS `gardner_email`
    , `user_reviews`.`rates` AS `user_rate`
    , `user_reviews`.`review` AS `user_review`
    , `user_reviews`.`review_date` AS `user_review_date`
    , `gardner_reviews`.`rates` AS `gardner_rate`
    , `gardner_reviews`.`review` AS `gardner_review`
    , `gardner_reviews`.`review_date` AS `gardner_review_date`
    , `applied_jobs`.`cost` AS `job_cost`
FROM
    `job_history`
    INNER JOIN `jobs` 
        ON (`job_history`.`job_id` = `jobs`.`id`)
    INNER JOIN `user_reviews` 
        ON (`user_reviews`.`job_id` = `job_history`.`job_id`)
    LEFT OUTER JOIN `gardner_reviews` 
        ON (`gardner_reviews`.`job_id` = `job_history`.`job_id`)
    INNER JOIN `users` 
        ON (`job_history`.`user_id` = `users`.`id`)
    INNER JOIN `gardners` 
        ON (`job_history`.`gardner_id` = `gardners`.`id`)
    INNER JOIN `applied_jobs` 
        ON (`applied_jobs`.`job_id` = `jobs`.`id`) AND `applied_jobs`.`gardner_id` = `jobs`.`assigned_to`
WHERE `jobs`.`assigned_to` =".$gid." 
GROUP BY `jobs`.`id`  ORDER BY job_history.comp_date DESC LIMIT ".$offset1.",".$offset2)->result();
 

 }


 public function user_job_history_api($userid,$offset1,$offset2){

 	return $this->db->query("SELECT
    `jobs`.`id` AS `id`
    , `jobs`.`job_description`
    , `jobs`.`location`
    , `jobs`.`lat`
    , `jobs`.`log`
    , `jobs`.`job_picture1`
    , `jobs`.`job_picture2`
    , `jobs`.`job_picture3`
    , `jobs`.`job_picture4`
    , `job_history`.`comp_date`
    , `users`.`f_name` AS `user_fname`
    , `users`.`l_name` AS `user_lname`
    , `users`.`username` AS `user_username`
    , `users`.`picture` AS `user_picture`
    , `users`.`email` AS `user_email`
    , `gardners`.`f_name` AS `gardner_fname`
    , `gardners`.`l_name` AS `gardner_lname`
    , `gardners`.`username` AS `gardner_username`
    , `gardners`.`picture` AS `gardner_picture`
    , `gardners`.`email` AS `gardner_email`
    , `user_reviews`.`rates` AS `user_rate`
    , `user_reviews`.`review` AS `user_review`
    , `user_reviews`.`review_date` AS `user_review_date`
    , `gardner_reviews`.`rates` AS `gardner_rate`
    , `gardner_reviews`.`review` AS `gardner_review`
    , `gardner_reviews`.`review_date` AS `gardner_review_date`
    , `applied_jobs`.`cost` AS `job_cost`
    , (SELECT count(`id`)  FROM `gardner_reviews` WHERE `gardner_id`= `gardners`.`id`) as gardner_total_reviews
    , (SELECT round(AVG(rates),0) FROM `gardner_reviews` WHERE `gardner_id`= `gardners`.`id`)  as gardner_avg_reviewsws 
   
FROM
    `job_history`
    INNER JOIN `jobs` 
        ON (`job_history`.`job_id` = `jobs`.`id`)
    INNER JOIN `user_reviews` 
        ON (`user_reviews`.`job_id` = `job_history`.`job_id`)
    LEFT OUTER JOIN `gardner_reviews` 
        ON (`gardner_reviews`.`job_id` = `job_history`.`job_id`)
    INNER JOIN `users` 
        ON (`job_history`.`user_id` = `users`.`id`)
    INNER JOIN `gardners` 
        ON (`job_history`.`gardner_id` = `gardners`.`id`)
    INNER JOIN `applied_jobs` 
        ON (`applied_jobs`.`job_id` = `jobs`.`id`) AND `applied_jobs`.`gardner_id` = `jobs`.`assigned_to`
WHERE `jobs`.`user_id` = ".$userid." 
GROUP BY `jobs`.`id`  ORDER BY job_history.comp_date DESC LIMIT ".$offset1.",".$offset2)->result();
 }

public function update_device_id($device_token,$device_id){
	$data=array('device_id'=>$device_id);
	$this->db->where('device_token',$device_token)->update('users',$data);
	$this->db->where('device_token',$device_token)->update('gardners',$data);
}






public function gardner_get_new_jobs($gardner_id,$offset1,$offset2)
{
	
	$query = $this->db->query("SELECT `jb`.`post_date`,`jb`.`job_picture1`,`jb`.`job_picture2`,`jb`.`job_picture3`,`jb`.`job_picture4`, `jb`.`id`, `u`.`username` as `user_username`, `u`.`picture` as `user_picture`,`u`.`f_name`  as `user_fname`,`u`.`l_name` as `user_lname`,`u`.`phone`,`u`.`email` as `user_email`,`u`.`log`,`u`.`lat`,`u`.`location`, `jb`.`job_description`, `jb`.`cost` as `job_cost`
		, (SELECT count(`id`)  FROM `user_reviews` WHERE `user_id`= `u`.`id`) as user_total_reviews
, (SELECT round(AVG(rates),0) FROM `user_reviews` WHERE `user_id`= `u`.`id`) as user_avg_reviews , 111.111 * DEGREES(ACOS(COS(RADIANS(jb.lat)) * COS(RADIANS(grd.lat)) * COS(RADIANS(jb.log - grd.log)) + SIN(RADIANS(jb.lat)) * SIN(RADIANS(grd.lat)))) AS distance_in_km FROM (`jobs` as `jb`, `gardners` as `grd`) JOIN `users` as `u` ON `u`.`id` = `jb`.`user_id` WHERE `grd`.`id` = ".$gardner_id." AND `jb`.`id` NOT IN(SELECT job_id FROM applied_jobs WHERE gardner_id =".$gardner_id.") AND `jb`.`id` NOT IN(SELECT job_id FROM ignored_jobs WHERE gardner_id =".$gardner_id.") AND `jb`.`job_status` = 0  GROUP BY `jb`.`id` HAVING `distance_in_km` < 100 order by jb.id desc limit ".$offset1.",".$offset2);
	//echo $this->db->last_query(); die;
   return $query->result();
   
   
}


public function gardner_get_my_jobs($gardner_id,$offset1,$offset2)
{
	
   
   return $this->db->query("
   SELECT `users`.`username` , `users`.`picture` as `user_picture`, `users`.`f_name` as `user_fname`, `users`.`l_name` as `user_lname`, `users`.`phone` , `users`.`email` as `user_email`, `users`.`lat`, `users`.`log`, `users`.`location`, `applied_jobs`.`cost` as job_cost , `jobs`.`post_date`, `jobs`.`job_status`,`jobs`.`job_picture1`,`jobs`.`job_picture2`,`jobs`.`job_picture3`,`jobs`.`job_picture4` , `jobs`.`job_description` , `jobs`.`id` , (SELECT count(`id`)  FROM `user_reviews` WHERE `user_id`= `users`.`id`) as user_total_reviews
, (SELECT round(AVG(rates),0) FROM `user_reviews` WHERE `user_id`= `users`.`id`) as user_avg_reviews  FROM `applied_jobs` INNER JOIN `jobs` ON (`applied_jobs`.`job_id` = `jobs`.`id`) AND (`jobs`.`assigned_to` = `applied_jobs`.`gardner_id`) INNER JOIN `users` ON (`users`.`id` = `jobs`.`user_id`) WHERE (`jobs`.`assigned_to` =".$gardner_id." AND jobs.job_status=1 OR jobs.job_status=3 ) group by jobs.id limit 
   ".$offset1.",".$offset2)->result();
	   
   }

public function gardner_applied_jobs($gardner_id,$offset1,$offset2)
	{
	
	
	return $this->db->query("SELECT jb.post_date,jb.id,jb.job_picture1,jb.job_picture2,jb.job_picture3,jb.job_picture4,user.f_name as  user_fname,user.l_name as  user_lname,user.phone,user.email as  user_email,jb.job_description,aj.cost as job_cost,user.username as  user_username, user.picture as  user_picture , (SELECT count(`id`)  FROM `user_reviews` WHERE `user_id`= `user`.`id`) as user_total_reviews
, (SELECT round(AVG(rates),0) FROM `user_reviews` WHERE `user_id`= `user`.`id`) as user_avg_reviews from users as user,applied_jobs as aj,jobs as jb where aj.gardner_id=".$gardner_id." and aj.job_id=jb.id and user.id=jb.user_id and jb.job_status=0 and jb.id not in (select job_id from rejected_jobs where user_id = ".$gardner_id.") group by jb.id  order by aj.id DESC limit ".$offset1.",".$offset2)->result();
	
	}


public function user_get_new_jobs($user_id,$offset1,$offset2)
{
	return $this->db->query("SELECT * FROM  `jobs`
	       WHERE job_status = 0 AND user_id=".$user_id." order by id desc limit ".$offset1.",".$offset2)->result();
		
}

public function user_get_running_jobs($user_id,$offset1,$offset2)
{

	 return $this->db->query("SELECT `jb`.`id`,`jb`.`job_status`, `jb`.`post_date`, `grd`.`username`, `grd`.`picture`, `jb`.`job_status`, `jb`.`job_description`, `aj`.`cost` as `job_cost`, (SELECT count(`id`)  FROM `gardner_reviews` WHERE `gardner_id`= `grd`.`id`) as gardner_total_reviews
, (SELECT round(AVG(rates),0) FROM `gardner_reviews` WHERE `gardner_id`= `grd`.`id`)  as gardner_avg_reviews
	 	FROM `jobs` as `jb` JOIN `gardners` as `grd` ON `grd`.`id` = `jb`.`assigned_to` 
	 	JOIN `applied_jobs` as `aj` ON `aj`.`job_id` = `jb`.`id` 
	 	where `jb`.`user_id` =".$user_id." AND `aj`.`gardner_id` = `jb`.`assigned_to` 
	 	AND `jb`.`job_status` ='1' OR `jb`.`job_status` ='3'
	 	 AND `jb`.`assigned_to` = `aj`.`gardner_id`  order by `jb`.`id` DESC limit ".$offset1.",".$offset2)->result();

	
	//$query = $this->db->get();
	//echo $this->db->last_query(); die;
   //return $query->result();
   
}


public function user_applied_jobs($user_id,$offset1,$offset2)
{
	return $this->db->query("SELECT jb.post_date , grd.username,grd.picture,grd.id as gardner_id,jb.id as job_id,aj.cost,jb.job_description as description FROM jobs as jb , gardners as grd , applied_jobs as aj 
		, (SELECT count(`id`)  FROM `gardner_reviews` WHERE `gardner_id`= `grd`.`id`) as gardner_total_reviews
		, (SELECT round(AVG(rates),0) FROM `gardner_reviews` WHERE `gardner_id`= `grd`.`id`)  as gardner_avg_reviews 

WHERE jb.id in(select job_id from applied_jobs) AND jb.id not in(select job_id from rejected_jobs where user_id=aj.gardner_id) AND jb.user_id=".$user_id." AND jb.job_status = 0 and grd.id=aj.gardner_id and aj.job_id= jb.id order by aj.id desc limit ".$offset1.",".$offset2)->result();
	
	
}





























































































































































































































	
}
