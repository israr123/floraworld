<?php
class UserModel extends MY_Model {


public function get_wall_feeds($user_id)
{

   $this->db->select('post.id,post.description,post.post_date,user.f_name,user.l_name,post.picture1,post.picture2,post.picture3,post.picture4');
	
	$this->db->from('posts as post');
	
	$this->db->join('users as user', 'post.user_id = user.id');
	$this->db->join('follow as fol', 'fol.following_id = post.user_id');
	$this->db->where('fol.follower_id',$user_id);
	$this->db->order_by('post.id','DESC');
	
	$query = $this->db->get();
	//echo $this->db->last_query(); die;
   return $query->result();
   
}
public function hide_notifications($user_id)
	{
		
		$this->db->where('user_id',$user_id);
		$this->db->where('status',0);
		$this->db->set('status', '1');		
		return $this->db->update('user_notifications');		
		
	 	
	}
	
	public function reject_job($job_id,$uid){
		$data=array('job_id'=>$job_id,'user_id'=>$uid);
		$this->db->insert('rejected_jobs',$data);


		//sending notification to gardner	
		
			$desc="Your Job you applied was rejected";
			$gardener_id= $uid;
			
			$indata=array(
						array(
							"description"=>$desc,
							"job_id"=>$job_id,
							"gardner_id"=>$gardener_id
							)
						);
		$this->submit_notification('grd',$indata);

		return true;

	}
public function get_user_posts_by_ajax($user_id,$post_ids)
{

   $this->db->select('post.id,post.description,post.post_date,user.f_name,user.l_name,post.picture1,post.picture2,post.picture3,post.picture4');
	
	$this->db->from('posts as post');
	
	$this->db->join('users as user', 'post.user_id = user.id');
	$this->db->join('follow as fol', 'fol.following_id = post.user_id');
	if(!empty($post_ids)) $this->db->where_not_in('post.id',$post_ids);
	$this->db->where('fol.follower_id',$user_id);
	$this->db->order_by('post.id','DESC');
	
	$query = $this->db->get();
	//echo $this->db->last_query(); die;
   return $query->result();
   
}
function check_already_done($post_id,$user_id,$table)
	{
		$this->db->where('post_id',$post_id)
				 ->where('user_id',$user_id);
		return $this->db->count_all_results($table);
	 	
	}
	
function check_user_exist($value)
	{
		$this->db->where('username',$value)
		->or_where('email',$value);
		$user=$this->db->count_all_results('users'); 
		//echo $this->db->last_query(); die;
		$this->db->where('username',$value)
		->or_where('email',$value);
		$gardner=$this->db->count_all_results('gardners');
		if($user)	
		return 1 ;
			elseif($gardner)
		return 1 ;
	 	    else
		return 0 ;
	}
	
function count_likes($post_id)
	{
		$post_id=str_replace("like","",$post_id);
		$this->db->where('post_id',$post_id);
				
		return $this->db->count_all_results('post_likes');
	 	
	}

function count_notifications($user_id)
	{
		
		$this->db->where('user_id',$user_id);
		$this->db->where('status',0);
				
		return $this->db->count_all_results('user_notifications');
	 	
	}
	
public function get_running_jobs($user_id)
{
	//assigned jobs
/* 	$this->db->select('jb.post_date,jb.job_description,jb.cost as job_cost ');
	$this->db->from('jobs as jb');
	$this->db->where('user_id', $user_id);
	$this->db->where('user_id', $user_id);
	
	
	$query = $this->db->get();
	//echo $this->db->last_query(); die;
   return $query->result(); */
   
   /*$this->db->select('jb.id,jb.post_date,grd.username,grd.picture,jb.job_status,jb.job_description,aj.cost as job_cost');
	
	$this->db->from('jobs as jb');
	$this->db->join('gardners as grd', 'grd.id = jb.assigned_to');
	$this->db->join('applied_jobs as aj', 'aj.gardner_id = jb.assigned_to');
	$this->db->where('jb.id =','aj.job_id');
	$this->db->where('jb.job_status',1);
	$this->db->or_where('jb.job_status',3);
	$this->db->where('jb.user_id',$user_id); 
	*/
	 return $this->db->query("SELECT `jb`.`id`,`jb`.`job_status`, `jb`.`post_date`, `grd`.`username`, `grd`.`picture`, `jb`.`job_status`, `jb`.`job_description`, `aj`.`cost` as `job_cost` 
	 	FROM `jobs` as `jb` JOIN `gardners` as `grd` ON `grd`.`id` = `jb`.`assigned_to` 
	 	JOIN `applied_jobs` as `aj` ON `aj`.`job_id` = `jb`.`id` 
	 	where `jb`.`user_id` =".$user_id." AND `aj`.`gardner_id` = `jb`.`assigned_to` 
	 	AND `jb`.`job_status` ='1' OR `jb`.`job_status` ='3'
	 	 AND `jb`.`assigned_to` = `aj`.`gardner_id`  order by `jb`.`id` DESC ")->result();

	
	//$query = $this->db->get();
	//echo $this->db->last_query(); die;
   //return $query->result();
   
}

function add_likes($post_id,$user_id)	
  {
	  
		if($this->check_already_done($post_id,$user_id,'post_likes'))
		{
			return 0;
		}
		else
		{
			
			 if($this->db->insert('post_likes',array('post_id'=>$post_id,'user_id'=>$user_id)))
			 return 1 ;
		}
  } 
  
public function get_new_notifications($user_id)
{
		$this->db->select('description');
		$this->db->where('user_id',$user_id);

		$this->db->where('status >=',0);
		$this->db->order_by('id','DESC');
   return $this->db->get('user_notifications',10)->result();
	
}

public function applied_jobs($user_id)
{
	return $this->db->query("SELECT jb.post_date , grd.username,grd.picture,grd.id as gardner_id,jb.id as job_id,aj.cost,jb.job_description as description FROM jobs as jb , gardners as grd , applied_jobs as aj WHERE jb.id in(select job_id from applied_jobs) AND jb.id not in(select job_id from rejected_jobs where user_id=aj.gardner_id) AND jb.user_id=".$user_id." AND jb.job_status = 0 and grd.id=aj.gardner_id and aj.job_id= jb.id order by aj.id desc ")->result();
	
	
}
public function accept_job($job_id,$gid)
{
	$this->db->query("update jobs set assigned_to='".$gid."', job_status=1 where id='".$job_id."' ");
	//echo $this->db->last_query();die();
	return true;
}
public function get_new_jobs($user_id)
{
	return $this->db->query("SELECT * FROM `jobs` WHERE job_status = 0 AND user_id=".$user_id." order by id desc ")->result();
	
	
}








































































































































































































	public function get_user_setting($id){

 	    return $this->db->where('id',$id)->get('users')->result();;
    }

	public function update_user($id,$data){

     $this->db->where('id',$id)->update('users',$data);
     return true;

	}

	public function postajob($data){
		$this->db->insert('jobs',$data);
     return $this->db->insert_id();

	}

	public function user_job_history($id){

		return $this->db->query('select jh.comp_date as post_date,assigned_to,jobs.id,job_description,aj.cost,gardners.picture,gardners.username from jobs JOIN gardners on jobs.assigned_to=gardners.id
			JOIN applied_jobs as aj on jobs.assigned_to=aj.gardner_id
			JOIN job_history as jh on jh.job_id=jobs.id
			where jobs.id=aj.job_id
         AND jobs.user_id="'.$id.'" and jobs.job_status=2 order by post_date desc ' )->result();
	
	}

	public function historydetailpage($id)
	{
		return $this->db->query('select aj.cost as aj_cost,aj.description as aj_description,history.comp_date from applied_jobs as aj ,job_history as history, jobs where jobs.id="'.$id.'" and history.job_id=jobs.id and jobs.assigned_to=aj.gardner_id and jobs.id=aj.job_id')->result();
	}

	public function jobreview($id){

		//return $this->db->where('id',$id)->get('jobs')->result();
		  return $this->db->query("SELECT jobs.*,aj.cost as aj_cost,
		  aj.description, gardners.username  from jobs JOIN gardners on jobs.assigned_to=gardners.id join applied_jobs as aj on jobs.id=aj.job_id AND jobs.assigned_to=aj.gardner_id
		       where jobs.id='".$id."' ")->result();
		 
		 
	}

	public function savereview($data){

		$this->db->insert('user_reviews',$data);
		return true;
	}
	public function save_job_history($job_id,$user_id,$gardener_id){

		$data=array('user_id'=>$user_id,'gardner_id'=>$gardener_id,'job_id'=>$job_id,);
		$this->db->insert('job_history',$data);
		return true;
	}
    public function complete_order($job_id)
	{
		$data=array('job_status'=>2);
		$this->db->where('id',$job_id)->update('jobs',$data);
		$user_id =$this->session->userdata('user_id');
		$gardener_id=$this->get_job_info($job_id,'assigned_to');
		$this->save_job_history($job_id,$user_id,$gardener_id);
		
		//sending notification to user	
		
			$desc="Your Job has been completed";
			$gardener_id=$this->get_job_info($job_id,'assigned_to');
			
			$indata=array(
						array(
							"description"=>$desc,
							"job_id"=>$job_id,
							"gardner_id"=>$gardener_id
							)
						);
		$this->submit_notification('grd',$indata);
		//sending app notification 
		if($deice_id=$this->get_keydeviceid_by_id('gardners',$gardener_id))
					{
						$this->push_notification($deice_id,$desc);
					}
		return true;
	}
   public function submit_notification($table,$data){
		if($table=="grd")
		$this->db->insert_batch('gardner_notifications', $data);
		if($table=="user")
		$this->db->insert_batch('user_notifications', $data);
		return true;

	}
	
   public function get_job_info($job_id,$field)
	{
		
		$this->db->where('id',$job_id);
		
		return $this->db->get('jobs')->row()->$field;
		
		
	}
	public function get_users_not_followed_yet()
	{
		$userid=$this->session->userdata('user_id');
		return $this->db->query('SELECT `user`.`id`, `user`.`username`, `user`.`picture`, `user`.`f_name` FROM `users` `user`, `users` WHERE `user`.`id` != '.$userid.' AND `user`.`id` NOT IN (SELECT following_id from follow WHERE follower_id='.$userid.') GROUP BY `user`.`id` LIMIT 4')->result();
		
	}
	
	public function get_user_review($job_id)
	{
	/* 	$user_id=$this->get_job_info($job_id,'user_id');
		
		$this->db->where('job_id',$job_id);
		return $this->db->get('user_reviews')->row();
		
		 */
     $this->db->select('ur.id,ur.rates,ur.review,ur.review_date,user.username,user.picture');
	
	$this->db->from('user_reviews as ur');
	$this->db->join('jobs as jb', 'jb.id = ur.job_id');
	$this->db->join('users as user', 'user.id = jb.user_id');
	
	$this->db->where('ur.job_id',$job_id);
	
	
	$query = $this->db->get();
	//echo $this->db->last_query(); die;
   return $query->result();
		
	}
	
	
	
	public function get_data($table,$data,$cond)
	{
		$this->db->where($cond);
		return $this->db->get($table)->row();
	}

	public function jobdetailpage($id){

		//return $this->db->where('id',$id)->get('jobs')->result();
		  return $this->db->query("SELECT jobs.*, gardners.username  from jobs JOIN gardners on jobs.assigned_to=gardners.id
		       where jobs.id='".$id."' ")->result();
	
	}

	public function ucurrentdetailpage($id){

		//return $this->db->where('id',$id)->get('jobs')->result();
		  return $this->db->query("SELECT jobs.*, users.username  from jobs JOIN users on jobs.user_id=users.id
		       where jobs.id='".$id."' ")->result();
	
	}

	public function uaplieddetailpage($id){

		//return $this->db->where('id',$id)->get('jobs')->result();
		  return $this->db->query("SELECT jobs.cost as job_cost,jobs.job_description,aj.description,jobs.id,jobs.job_picture1,jobs.job_picture2,jobs.job_picture3,jobs.job_picture4,jobs.job_title,aj.apply_date, aj.description,aj.cost, users.username  from jobs JOIN users on jobs.user_id=users.id
		  	join applied_jobs as aj on jobs.id=aj.job_id
 
		       where jobs.id='".$id."' ")->result();
	
	}
	public function viewuserprofileprivate($id){

		return $this->db->where('id',$id)->get('users')->row();

	}
	function getuserposts($id)
	{
		$this->db->order_by('id','DESC');
		return $this->db->where('user_id',$id)->get('posts')->result();
		
	}


}
