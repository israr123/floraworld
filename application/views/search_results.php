



<!-- Navigation -->

<!-- Masthead -->
<header class="breadcrumb-div text-white text-center">
  <div class="overlay"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12 mx-auto">
<h3>Job Listing</h3>
      </div>
    </div>
  </div>
</header>
<section id="signup">
  <div class="container">
    <div class="row">
     

      
      <div class="col-md-12">
        <div class="btn-group  pull-right" role="group">
          <button type="button" id="btn-list" class="btn  themebutton list"> <i class="fa fa-th-list"></i> &nbsp; List </button>
          <button type="button" id="btn-grid" class="btn  themebutton grid"> <i class="fa fa-th-large"></i> &nbsp; Grid</button>
        </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-3">
        <div class="sidebarlist">
         <!-- <ul class="list-group">
            <li class="list-group-item"><strong class="text-uppercase">WIllingness to Travel</strong></li>
            <li class="list-group-item">
              <div class="form-check">
                <label class="form-check-label">
                  <input class="form-check-input" type="checkbox">
                  Less than 10 miles </label>
              </div>
            </li>
            <li class="list-group-item">
              <div class="form-check">
                <label class="form-check-label">
                  <input class="form-check-input" type="checkbox">
                  Less than 20 miles </label>
              </div>
            </li>
            <li class="list-group-item">
              <div class="form-check">
                <label class="form-check-label">
                  <input class="form-check-input" type="checkbox">
                  Less than 50 miles </label>
              </div>
            </li>
            <li class="list-group-item">
              <div class="form-check">
                <label class="form-check-label">
                  <input class="form-check-input" type="checkbox">
                  Less than 100 miles </label>
              </div>
            </li>
            <li class="list-group-item">
              <div class="form-check">
                <label class="form-check-label">
                  <input class="form-check-input" type="checkbox">
                  All Distances </label>
              </div>
            </li>
          </ul>
          <hr>
          -->
          <ul class="list-group">
            <li class="list-group-item mb-4"><strong class="text-uppercase">Top rated</strong></li>
	<?php foreach($topgardners as $topgardner): if($topgardner->picture=="") $topgardner->picture="smallman.png"; ?>				
     <li class="list-group-item">
     <a href="<?=base_url('gardner/').$topgardner->username;?>">
     <div class="media">
  <img src="<?php echo base_url('assets/img/');?><?=$topgardner->picture;?>" height="50px" width="50px">
  <div class="media-body" style="
    font-size:  12px;
    margin-left:  8px;
    text-align:  justify;
">
    <?=substr($topgardner->description,0,100);?>
  </div></div></a>
     </li>
     <hr>
	  <?php endforeach; ?>
        
          </ul>
        </div>
      </div>
      <div class="col col-md-9">
      <?php if(empty($gardners)): ?>
       <div class="row">
        <h6>Sorry, we couldn't find any results for this search!</center></h6>
      </div>
     <?php else: ?>
        	<div id="img-listing-grid" style="display:none;">
          	<div class="row">
              <?php foreach ($gardners as $gardner) { ?>
       
              		<div class="col-md-4">
                      	<div class="widget"><a href="<?php echo base_url('gardner/'.$gardner->username); ?>">
                      	<div class="wrapper-listing"> <div class="img-bg"> <img src="<?php echo base_url('assets/img/'); ?><?php echo $gardner->picture; ?>" class="img-fluid"> <div class="overlay"></div> <!--<div class="pricetag"> <span><strong>$500</strong> </span> </div> <div class="img-title"> Green Tree </div>--> </div></div> </a>
                        <a href="<?php echo base_url('gardner/'.$gardner->username); ?>">  <div class="bottom-title"> <?php echo $gardner->f_name." ".$gardner->l_name; ?> </div> </a></div> 
                  </div> 
                    <?php } ?>
              </div>
        	</div>
        
         


        <div id="img-listing-list">
        	
          <?php foreach ($gardners as $gardner) { ?>
        	 <div class="widget">
            	<div class="row">
                  <div class="col-md-12">
                    <div class="media"><a href="<?php echo base_url('gardner/'.$gardner->username); ?>">
                            <img src="<?php echo base_url('assets/img/'); ?><?php echo $gardner->picture; ?>" class="img-fluid img-thumbnail"></a>
                        <div class="media-body">
                              
                            <div class="row">
                              	<div class="col-md-10"><div class="media-left">
                                <a href="<?php echo base_url('gardner/'.$gardner->username); ?>"><h5 class="mt-0"><?php echo $gardner->f_name." ".$gardner->l_name; ?></h5>
                                <?php echo $gardner->description; ?></a></div></div>
                              	<div class="col-md-2"> <div class="media-right">
                              	<!--<a href="#"><h2>$500</h2> 
                                Per hour</a>-->
                               </div></div>
                            </div>
                         </div> 
							       </div>      
                  </div>
              </div>
          </div>
         <hr>
            
      <?php } ?>
      
  </div>
        
    <!-- <div id="img-listing-toprated" style="display:none;">
        	<div class="widget">
            		<div class="row">
                    	<div class="col-md-12">
                        	<div class="media"><a href="#">
                            <img src="img/bg01.jpg" class="img-fluid img-thumbnail"></a>
                              <div class="media-body">
                              
                              <div class="row">
                              	<div class="col-md-10"><div class="media-left">
                                <a href="#"><h5 class="mt-0">Green Tree</h5>
                                 Lorem Ipsum copy in various charsets and languages for layouts. Lorem Ipsum copy in various charsets and languages for layouts.</a></div></div>
                              	<div class="col-md-2"> <div class="media-right">
                              	<a href="#"><h2>$500</h2>
                                Per hour</a>
                              
                              </div></div>
                              
                              </div>
         
                              </div>
                             
							             </div>
              
                        </div>
                    </div>
            
            </div>
        
        <hr>
        
        	  <div class="widget">
            		<div class="row">
                    	<div class="col-md-12">
                        	<div class="media"><a href="#">
                            <img src="img/bg01.jpg" class="img-fluid img-thumbnail"></a>
                              <div class="media-body">
                              
                              <div class="row">
                              	<div class="col-md-10"><div class="media-left">
                                <a href="#"><h5 class="mt-0">Green Tree</h5>
                                Lorem Ipsum copy in various charsets and languages for layouts. Lorem Ipsum copy in various charsets and languages for layouts. Lorem Ipsum copy in various charsets and languages for layouts. Lorem Ipsum copy in various charsets and languages for layouts. Lorem Ipsum copy in various charsets and languages for layouts.</a></div></div>
                              	<div class="col-md-2"> <div class="media-right">
                              	<a href="#"><h2>$500</h2>
                                Per hour</a>
                              
                              </div></div>
                              
                              </div>
                            
                              </div>
                             
						            	</div>
                 
                        </div>
                    </div>
            
                 </div>
      <hr>       
    </div>
-->





      
      </div>
    <?php endif; ?>
    </div>
  </div>
</section>
<script>
$(document).ready(function(){
	
	
	$('.grid').click(function(){
		   $('#img-listing-list').hide();
			$('#img-listing-grid').show();
			$('#btn-list').removeClass('active');		
			$('#btn-grid').addClass('active');
			
			
		});
		
		$('.list').click(function(){
		   $('#img-listing-list').show();
			$('#img-listing-grid').hide();
			$('#btn-list').addClass('active');		
			$('#btn-grid').removeClass('active');
			
		});
		
	
	
});

  function activeClasses(){
			$('#btn-list').addClass('active');
	
	}
</script>
