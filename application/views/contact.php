<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<style>
.error {
	color:red;
}
</style>
    <!-- Masthead -->
    <header class="breadcrumb-div text-white text-center">
      <div class="overlay"></div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-xl-12 mx-auto">
          
     <h3>Contact Us Now</h3>
          </div>
         </div>
      </div>
    </header>


<section id="signup">
<div class="container">
	<div class="row">
    <div class="col col-md-12">
    	<h1 class="text-center"> Contact the Flora World team!</h1>
        <p class="text-center"> We would happy to see your message</p>
        </div>
        </div>
        <div class="row justify-content-md-center">
    	<div class="col col-md-6">
        
    
        
        <form id="contactform" action="<?php echo base_url('save_contactus/'); ?>" method="post" class="mt-4">
              <div class="form-group">
                <label for="email">Email address:</label>
                <input name="email" required type="email" class="form-control" id="email">
              </div>
        
         <div class="form-group">
                <label for="message">Leave a Comment Here:</label>
                <textarea name="message" required type="text" class="form-control" id="message" rows="5"> </textarea>
              </div>

  <button type="Submit" class="btn btn-dark themebutton btn-block mt-4">Send</button>
              
              
     </form>             
        
        
        
        </div>
    </div>
</div>

</section>
<script>
$("#contactform").validate();
  rules: {
		email: {
      required:true
    }

    message: {
      required:true
    }
		
		}
	  

 
</script>