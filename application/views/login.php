
<!-- Masthead -->
<header class="breadcrumb-div text-white text-center">
  <div class="overlay"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12 mx-auto">
       <h3>Login</h3>
      </div>
    </div>
  </div>
</header>
<section id="signup">
  <div class="container">
    <div class="row">
      <div class="col col-md-12">
        <h1 class="text-center"> Login now to see your World</h1>
        <p class="text-center"> </p>
      </div>
    </div>
    <div class="row justify-content-md-center">
      <div class="col col-md-6">
        <form id="loginform" action="<?php echo base_url(); ?>users/verifylogin" method="post" class="mt-4" enctype="multipart/form-data">
          <div class="form-group">
            <label for="email">Email address:</label>
            <input type="text" name="email" class="form-control" id="email"required >
          </div>
          <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" name="password" class="form-control" id="pwd" required>
          </div>
          <button id="submit" action="submit" class="btn btn-dark themebutton btn-block mt-4" >Login</button>
          <br>
          <a href="users/register">Create a new account</a> <br>
          <a href="javascript:void(0)" id="forgot-password-toggle">Forgot username or password</a>
        </form>


        <form action="<?php echo base_url('forget_password'); ?>" method="post" enctype="multipart/form-data">
          <div id="hidden-form" style="display:none;" class="mt-5">
          <p>Enter your email address to receive your username and reset your password.</p>
          <div class="form-group">
            <label for="email-2">Email:</label>
            <input type="email" name="email" class="form-control" id="email-2">
          </div>
          <button type="Submit" class="btn btn-dark themebutton btn-block mt-4">Request New Password</button>
        </div>
        </form>
      </div>
    </div>
  </div>
</section>
<script>
$(document).ready(function(){
  
  
  $('#forgot-password-toggle').click(function(){
       $('#hidden-form').toggle();
      
      
    });
    
    
    });
    
    $("#submit button").click(function(event){
	var form_data=$("#loginform").serializeArray();
	var error_free=true;
	for (var input in form_data){
		var element=$("#loginform"+form_data[input]['name']);
		var valid=element.hasClass("valid");
		var error_element=$("span", element.parent());
		if (!valid){error_element.removeClass("error").addClass("error_show"); error_free=false;}
		else{error_element.removeClass("error_show").addClass("error");}
	}
	if (!error_free){
		event.preventDefault(); 
	}
	else{
		alert('No errors: Form will be submitted');
	}
});


    </script>

