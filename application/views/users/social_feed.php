<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<style>
.error {
	color:red;
}
</style>

<!-- Masthead -->
<header class="breadcrumb-div text-white text-center">
  <div class="overlay"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12 mx-auto">
        <h3>Social Feed</h3>
      </div>
    </div>
  </div>
</header>
<section id="profile">
  <div class="container">     
                       
  
  
             <div class="mt-5"></div>
             
             
             
             
 <form id="socialform" action="<?php echo base_url('save_social_feed'); ?>" method="post"  enctype="multipart/form-data" >

        <div class="form-group">
          <label>Write something about you </label>
          <textarea name="description"  class="form-control" required ></textarea>
        </div>

            <div class="form-group">
          <label>Select Image 1</label>
          <input title="Please Select a valid image format."   name="picture1" required type="file" class="form-control">
        </div>
           <div class="form-group">
          <label>Select Image 2 (optional)</label>
          <input title="Please Select a valid image format."  name="picture2" type="file" class="form-control">
        </div>
             <div class="form-group">
          <label>Select Image 3 (optional)</label>
          <input  title="Please Select a valid image format."  name="picture3" type="file" class="form-control">
        </div>
             <div class="form-group">
          <label>Select Image 4 (optional)</label>
          <input  title="Please Select a valid image format."   name="picture4" type="file" class="form-control">
        </div>
        <button type="submit" class="btn themebutton">Submit</button>
</form>
 
    
        
             <div class="mt-5"></div>
        
  </div>
  
  
  
  
  
</section>

<script>
$(document).ready(function(){
	
	
    $('[data-toggle="tooltip"]').tooltip(); 
	$("#socialform").validate({
	
		  rules: {
		   picture1: {
      required: true,
      extension: "png|PNG|JPG|jpg|JPEG|jpeg|GIF|gif"
			},
		picture2: {
      required: false,
      extension: "png|PNG|JPG|jpg|JPEG|jpeg|GIF|gif"
			},
		picture3: {
      required: false,
      extension: "png|PNG|JPG|jpg|JPEG|jpeg|GIF|gif"
			},
		picture4: {
      required: false,
      extension: "png|PNG|JPG|jpg|JPEG|jpeg|GIF|gif"
			}
		  }
	  
	  
	  
	  
 
});
});
	
</script>
