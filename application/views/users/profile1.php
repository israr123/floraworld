
<!-- Masthead -->
<header class="breadcrumb-div text-white text-center">
  <div class="overlay"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12 mx-auto">
        <h3>Profile</h3>
      </div>
    </div>
  </div>
</header>
<section id="profile">
  <div class="container">
    <div class="row">
      <div class="col col-md-12"> 
      <div class="mt-5"></div>
      <div class="media">
  <div class="media-left">
    <img src="<?php echo base_url('assets/img/');?><?php echo $info[0]->picture; ?>" class="media-object rounded" width="95px">
  </div>
  <div class="media-body ml-4">
    <h4 class="media-heading text-green"><?php echo $info[0]->username; ?></h4>
    <p> <i class="fa fa-home"></i>&nbsp;  <?php echo $info[0]->location; ?> 10:09 pm
    <br> 
    <span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star checked"></span>
<span class="fa fa-star"></span>
<span class="fa fa-star"></span>
    </p>
  </div>
</div>
      
      <div class="pro-body">
      
      <p><?php echo $info[0]->description; ?></p>
      
      </div>
      
      
      </div>
    </div>
    
    <!--
    <div class="features-icons text-center">
    <div class="row">
        	<div class="col-lg-3">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                <h3 class="text-green">$5k</h3>
              <p class="lead mb-0">Per hour </p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <h3 class="text-green">20+</h3>
              <p class="lead mb-0">Jobs</p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                <h3 class="text-green">$500k</h3>
              <p class="lead mb-0">Earnings</p>
            </div>
          </div>
            <div class="col-lg-3">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <h3 class="text-green">100+</h3>
              <p class="lead mb-0">Happy Clients <br>
+ <br>

Maintenance Plan (Optional)
</p>
            </div>
          </div>
        </div>
        </div>
        -->
           
             
             
    <div id="signup">
      <div class="col col-md-12">
        <div class="index-tab-info">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item"> <a class="nav-link  active show" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Reviews </a> </li>
            <li class="nav-item"> <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile-2" role="tab" aria-controls="profile-2" aria-selected="false">My posts</a> </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
              <div class="job-listing mt-5 table-responsive">
           <div class="media">
  <div class="media-left" style="padding-left: 0; ">
    <img src="<?=base_url('assets/img/');?>smallman.png" class="media-object rounded" width="75px">
  </div>
  <div class="media-body ml-4">

    <?php if(!empty($rev_details)): ?>
    <div class="review-detail">
      <h4> Reviews <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> 4.5 </h4>
      <hr>
      <div class="mt-4"></div>
      <div class="media">
        <div class="media-left" style="padding-left: 0; "> <img src="<?=base_url('assets/img/');?><?php echo $rev_details[0]->picture; ?>" class="media-object rounded" width="75px"> </div>
        <div class="media-body ml-4">
          <h5 class="media-heading"><span class=" text-green"><?php echo $rev_details[0]->username; ?></span> 
      
      <?php
      $rate=$rev_details[0]->rates;
      for($t=0;$t<5;$t++){
         if($t<$rate)
        echo "<span class='fa fa-star checked'></span>";  
        else
        echo "<span class='fa fa-star text-muted'></span>";  
      }
    
     

      ?>
      </h5>
          <p> <?php echo $rev_details[0]->review; ?> <br>
             </p>
            <!-- <p><span class="text-muted">2 minutes ago</span></p>-->
        </div>
      </div>
      <hr>
    </div>
  <?php endif; ?>
  </div>
  
  
  
  
  
</section>

<!-- Footer --> 

<!-- Bootstrap core JavaScript --> 
<script src="../plugin/jquery/jquery.min.js"></script> 
<script src="../plugin/bootstrap/js/bootstrap.js"></script> 
<script>
$(document).ready(function(){
	
	
    $('[data-toggle="tooltip"]').tooltip(); 
});

</script>
</body>
</html>