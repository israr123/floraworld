<?php 
if(empty($details))
{
	?><script>
	 window.history.go(-1);	
	 </script>
	<?php	
	die;
}
?>



<!-- Masthead -->
<header class="breadcrumb-div text-white text-center">
  <div class="overlay"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12 mx-auto">
        <h3>Job Detail</h3>
      </div>
    </div>
  </div>
</header>
<section id="signup">
  <div class="container">
    <div class="row">
      <div class="col col-md-12">
        <div class="detail-list">
          <div class="row">
            <div class="col-md-12">
              <div class="heading">
                <h6> <?php echo $details[0]->job_title; ?></h6>
                <hr>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-9">
              <div class="detail-job">
                <p> <strong>Completed by:<a href="<?php echo base_url('gardner/'.$details[0]->username);  ?>"><span class="text-green"> <?php echo $details[0]->username; ?></strong></span></strong></a> <br>
                  <span class="f-12">Posted: <?php echo date('d-m-Y',strtotime($details[0]->post_date)); ?></span>
                  <br>
                  <span class="f-12">Completed: <?php echo date('d-m-Y',strtotime($ex_details[0]->comp_date)); ?></span>
                </p>
                <div class="mt-3"></div>
               
                <p>
                  <h6>Job Description</h6>
                  <?php echo $details[0]->job_description; ?>
                </p>
                <br>
                <br>
                <br>
                <p>
                  <h6>Gardener Message</h6>
                  <?php echo $ex_details[0]->aj_description; ?>
                </p>
              </div>
            </div>
            <div class="col-md-3">
              <div class="detail-contact">
                <ul>
                  <li> <i class="fa fa-money"></i> <span><strong><?php echo " ". $ex_details[0]->aj_cost; ?> </strong> <br>
                    Fixed Price </span> </li>
                  <li> <i class="fa fa-clock-o"></i> <span><strong>Time Completed</strong> <br>
                     </span> </li>
                  <?php if ($details[0]->job_status==3): ?>
          <li>
                    <button class="btn themebutton btn-block" data-toggle="modal" data-target="#reviewjob"> <i class="fa fa-star"></i> &nbsp; Complete Order and rate</button>
                  </li>
           <?php endif; ?>
                  <div class="mt-5"></div>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="mt-5"></div>
    <div class="gallery-images">
      <h4 class="text-center"> Featured Gallery</h4>
      <div class="mt-4"></div>
      <div class="row">
	   <?php if($details[0]->job_picture1){?>
        <div class="col-md-3"> <img src="<?=base_url('assets/img/');?><?php echo $details[0]->job_picture1; ?>" alt=""  class="img-fluid"></div>
	   <?php } if($details[0]->job_picture2){?>
        <div class="col-md-3"><img src="<?=base_url('assets/img/');?><?php echo $details[0]->job_picture2; ?>" alt="" class="img-fluid" ></div>
	   <?php } if($details[0]->job_picture3){?>
        <div class="col-md-3"><img src="<?=base_url('assets/img/');?><?php echo $details[0]->job_picture3; ?>" alt=""  class="img-fluid"></div>
	   <?php } if($details[0]->job_picture4){?>
        <div class="col-md-3"><img src="<?=base_url('assets/img/');?><?php echo $details[0]->job_picture4; ?>" alt=""  class="img-fluid"></div>
	   <?php } ?>
      </div>
    </div>
    <div class="mt-5"></div>
   
    <?php if(!empty($rev_details)): ?>
    <div class="review-detail">
      <h4> Reviews <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span> <span class="fa fa-star checked"></span></h4>
      <hr>
      <div class="mt-4"></div>
      <div class="media">
	   <a href="<?=base_url('gardner/').$rev_details[0]->username;?>">
        <div class="media-left" style="padding-left: 0; "> <img src="<?=base_url('assets/img/');?><?php echo $rev_details[0]->picture; ?>" class="media-object rounded" width="75px"> </div></a>
        <div class="media-body ml-4">
          <h5 class="media-heading"><span class=" text-green"><?php echo $rev_details[0]->username; ?></span> 
      
      <?php
      $rate=$rev_details[0]->rates;
      for($t=0;$t<5;$t++){
         if($t<$rate)
        echo "<span class='fa fa-star checked'></span>";  
        else
        echo "<span class='fa fa-star text-muted'></span>";  
      }
    
     

      ?>
      </h5>
          <p> <?php echo $rev_details[0]->review; ?> <br>
             </p>
            <p><span class="text-muted time" title="<?php echo $rev_details[0]->review_date; ?>"><?php echo $rev_details[0]->review_date; ?></span></p>
        </div>
      </div>
      <hr>
    </div>
  <?php endif; ?>
  </div>
  
  <!-- Modal -->
  <form method="post" action="<?=base_url('save_review');?>" >
  
  <div class="modal fade" id="reviewjob" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Please enter Your experience with the seller</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <div class="rating_label"> Seller Responsed </div>
            </div>
            <div class="col-md-6">
              <div class="star-rating"> <span class="fa fa-star-o" data-rating="1"></span> <span class="fa fa-star-o" data-rating="2"></span> <span class="fa fa-star-o" data-rating="3"></span> <span class="fa fa-star-o" data-rating="4"></span> <span class="fa fa-star-o" data-rating="5"></span>
                <input type="hidden" name="rating" class="rating-value" value="3">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="mt-3"></div>
              <div class="form-group">
                <label>Public Review</label>
                <textarea required  name="review" class="form-control" placeholder="Write a Short Discription"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button  class="btn themebutton"> Submit Review</button>
          <input type="hidden" name="job_id" value="<?php echo $details[0]->id;?>" > 
        </div>
      </div>
    </div>
  </div>
  </form>
</section>

<script>
$(document).ready(function(){
  
  var $star_rating = $('.star-rating .fa');

var SetRatingStar = function() {
  return $star_rating.each(function() {
    if (parseInt($star_rating.siblings('input.rating-value').val()) >= parseInt($(this).data('rating'))) {
      return $(this).removeClass('fa-star-o').addClass('fa-star');
    } else {
      return $(this).removeClass('fa-star').addClass('fa-star-o');
    }
  });
};

$star_rating.on('click', function() {
  $star_rating.siblings('input.rating-value').val($(this).data('rating'));
  return SetRatingStar();
});

SetRatingStar();
$(document).ready(function() {

});
  
  
});

</script>
<!-- Footer --> 


