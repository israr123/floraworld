<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Masthead -->

<header class="breadcrumb-div text-white text-center">
  <div class="overlay"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12 mx-auto">
        <form method="post" action="search_gardeners" class="mb-5 mt-5">
              <div class="form-row">
              <div class="col-md-3"></div>
                <div class="col-12 col-md-4 mb-2 mb-md-0">
                  <input name="keyword" id="gardener" type="text" class="form-control form-control-lg" placeholder="Find a gardener">
                </div>
                <div class="col-12 col-md-2">
                  <button type="submit" class="btn btn-block btn-lg btn-dark themebutton">Search &nbsp; <i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
      </div>
    </div>
  </div>
</header>
<div class="follow-portion">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="leftbar sidebarlist">
        	<ul class="list-group">
            <!--<li class="list-group-item mb-4"><strong class="text-uppercase">Follow People</strong> <span class="pull-right"><a href="follow_people">See All</a></span></li> -->
    
<?php foreach($users as $user): if($user->picture=="") $user->picture="smallman.png"; ?>
 <li class="list-group-item">
     
     <div class="media"><a href="<?=base_url('user/').$user->username; ?>">
  <img alt="" src="<?=base_url('assets/img/').$user->picture;?>" height="65px" width="65px" class="rounded"></a>
  <div class="media-body" style="
    font-size:  12px;
    margin-left:  7%;
    text-align:  justify;
">

<h6><?php echo $user->f_name; ?></h6>
<a href="follow/<?=$user->id;?>" class="btn themebutton f-12 follow">Follow Up</a>
  </div></div>
     
	 </li>
   
   
       
<?php endforeach; ?>
  
          </ul>
        
        
        
        
        </div>
      </div>
	  
      <div class="col-md-6">
	  <?php if(!empty($feeds)):  ?>
	  <?php foreach($feeds as $feed): 
			$total_likes=$this->UserModel->count_likes($feed->id);
			?>
        <div class="body-content">
        	<div class="widget">
            	<div class="title text-green"><?php echo $feed->f_name." ".$feed->l_name;?> </div>
                <div class="time text-muted" title="<?php echo $feed->post_date; ?>" ><?php echo $feed->post_date; ?></div>
                <div class="discription"><?php echo $feed->description; ?></div>
                <div class="gallery">
                	<div class="row">
                    	<div class="col-md-6">
                        	<img alt="" src="<?php echo base_url('assets/img/');?>/<?php echo $feed->picture1; ?>" class="img-fluid" onError="this.src='';">
                        </div>
                        <div class="col-md-6">
                        	<img alt="" src="<?php echo base_url('assets/img/');?>/<?php echo $feed->picture2; ?>" class="img-fluid" onError="this.src='';">
                        </div>
                        <div class="col-md-6">
                        	<img alt="" src="<?php echo base_url('assets/img/');?>/<?php echo $feed->picture3; ?>" class="img-fluid" onError="this.src='';">
                        </div>
                        <div class="col-md-6">
                        	<img alt=""  src="<?php echo base_url('assets/img/');?>/<?php echo $feed->picture4; ?>" class="img-fluid" onError="this.src='';">
                        </div>
                    
                    </div>
                
                </div>
                <div class="actions">
                	<a herf="#" class="text-muted"> <i id="like<?=$feed->id;?>" class="fa fa-thumbs-up like"></i> <span id="total_likeslike<?=$feed->id;?>"><?=$total_likes;?></span> Likes</a>
                    <a herf="#" class="text-muted"> <i class="fa fa-share"></i> Share</a>
                
                </div>
            
            </div>
        
        </div>
       <?php endforeach; endif; ?>
      </div>
      <div class="col-md-3">
        <div class="rightbar sidebarlist">
        	<ul class="list-group">

            
			
	<?php foreach($gardners as $gardner): if($gardner->picture=="") $gardner->picture="smallman.png"; ?>		

            <li class="list-group-item mb-4"><strong class="text-uppercase">Gardeners near by you</strong> <span class="pull-right"><a href="<?php echo base_url('listing/') ;?>">See All</a></span></li>

     <li class="list-group-item">
     
     <div class="media"><a href="gardner/<?=$gardner->username;?>">
  <img alt="" src="<?php echo base_url('assets/img/');?><?=$gardner->picture;?>" height="65px" width="65px" class="rounded"></a>
  <div class="media-body" style="
    font-size:  12px;
    margin-left:  7%;
    text-align:  justify;
">
<h6 class="mb-0"><?=$gardner->f_name." ".$gardner->l_name;?></h6>
<p class="mb-1"><?=$gardner->description;?></p>
<a href="gardner/<?=$gardner->username;?>" class="btn themebutton f-12">Detail</a>
  </div></div>
     </li>   <div class="mt-3"></div>
	 <?php endforeach; ?>
  

  
     
     
          </ul>
        
        
        
        
        </div>
      </div>
    </div>
  </div>
</div>

<?php 
function getTimeInterval($date) {
          
        $date = new DateTime ($date);
        $now = date ('Y-m-d H:i:s', time()); 
        $now = new DateTime ($now);
        if ($now >= $date) {
            $timeDifference = date_diff ($date , $now);
            $tense = " ago";
        } else {
            $timeDifference = date_diff ($now, $date);
            $tense = " ago";
        }
            
        $period = array (" second", " minute", " hour", " day", " month", " year");
         $periodValue= array ($timeDifference->format('%s'), $timeDifference->format('%i'), $timeDifference->format('%h'), $timeDifference->format('%d'), $timeDifference->format('%m'), $timeDifference->format('%y'));
        
        for ($i = 0; $i < count($periodValue); $i++) {
              if ($periodValue[$i] != 1) {
                $period[$i] .= "s";
            }
              if ($periodValue[$i] > 0) {
                $interval = $periodValue[$i].$period[$i].$tense; // ie.: 3 months ago
            }
        }
        
        if (isset($interval)) {
            return $interval;
       
        } else {
            return "0 seconds" . $tense;
        }
    }
	?>
<script type="text/javascript">
	$( ".follow" ).click(function() {
		
		
		 
		 $followurl=$(this).attr("href");
		 $.post($followurl); 
		 $(this).closest('li').fadeOut( "slow" );
		   return false;
	});
	


            $( "#gardener" ).autocomplete({
              source: "<?php echo site_url('search_gardeners_ajax');?>"
            });

			
			$('#like').click(function() {
		$.post( "add_likes", { post_id:  $('#post_id').val() } , function( data ) { 
			if(data == 1)
			{
				$('#total_likes').text( parseInt($('#total_likes').text()) + 1 );
			}
		});
	 
	});
	$('.like').click(function() {
		var total_likes;
		var idnumber;
		idnumber=this.id;
		total_likes=parseInt($('#total_likes'+idnumber).text());
	
		$.post( "add_likes", { post_id: idnumber } , function( data ) { 
			if(data == 1)
			{
				$('#total_likes'+idnumber).text( total_likes + 1 );
			}
		});
	 
	});
</script>
