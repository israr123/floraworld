<pre>
<?php //print_r($info); 
$CI =& get_instance();
?>
</pre>

<header class="breadcrumb-div text-white text-center">
  <div class="overlay"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12 mx-auto">
        <h3>Profile</h3>
      </div>
    </div>
  </div>
</header>
<section id="profile">
  <div class="container">
    <div class="row">
      <div class="col col-md-12"> 
      <div class="mt-5"></div>
      <div class="media">
  <div class="media-left">
    <img src="<?php echo base_url('assets/img/');?><?php echo $user->picture; ?>" class="media-object rounded" width="95px">
  </div>
  <div class="media-body ml-4">
    <h4 class="media-heading text-green"><?php echo $user->f_name." ".$user->l_name;?></h4>
    <p> <i class="fa fa-home"></i>&nbsp;  <?php echo $info[0]->location; ?>
    <br> 
	<i class="fa fa-phone"></i>&nbsp; <?php echo $info[0]->phone; ?>
    <br>
      <?php $rate3=$avgreview;
                for($u=0;$u<5;$u++){
                   if($u<$rate3)
                        echo "<span class='fa fa-star checked'></span>";  
                   else
                        echo "<span class='fa fa-star text-muted'></span>";  
                  }
 
            ?>
    </p>
  </div>
</div>
      
      <div class="pro-body">
      
      <p><?php echo $info[0]->description; ?></p>
      
      </div>
      
      
      </div>
    </div>
    
    <!--
    <div class="features-icons text-center">
    <div class="row">
          <div class="col-lg-3">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                <h3 class="text-green">$5k</h3>
              <p class="lead mb-0">Per hour </p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <h3 class="text-green">20+</h3>
              <p class="lead mb-0">Jobs</p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                <h3 class="text-green">$500k</h3>
              <p class="lead mb-0">Earnings</p>
            </div>
          </div>
            <div class="col-lg-3">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <h3 class="text-green">100+</h3>
              <p class="lead mb-0">Happy Clients <br>
+ <br>

Maintenance Plan (Optional)
</p>
            </div>
          </div>
        </div>
        </div>
        -->
           
             
             
             <div id="signup">

      <div class="col col-md-12">
        <div class="index-tab-info">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item"> <a class="nav-link  active show" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Reviews </a> </li>
            <li class="nav-item"> <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile-2" role="tab" aria-controls="profile-2" aria-selected="false">My posts</a> </li>
          </ul>

    <div class="tab-content" id="myTabContent">
      <div class="tab-pane fade active show" id="home" role="tabpanel" aria-labelledby="home-tab">
        <div class="job-listing mt-5 table-responsive">
     

<?php foreach ($reviews as $review) { ?>
     <div class="media">
      <div class="media-left" style="padding-left: 0; ">
        <img src="<?=base_url('assets/img/');?><?php echo $review->picture; ?>" class="media-object rounded" width="75px">
      </div>
      <div class="media-body ml-4">
        <h5 class="media-heading text-green"><?php echo $review->username; ?> 
           
            <?php $rate1=$review->rates;
                for($g=0;$g<5;$g++){
                   if($g<$rate1)
                        echo "<span class='fa fa-star checked'></span>";  
                   else
                        echo "<span class='fa fa-star text-muted'></span>";  
                  }

            ?>

        </h5>
        <p><?php  echo $review->review; ?>
        </p>
        <p><span class="time text-muted" title="<?php echo $review->review_date; ?>"><?php echo $review->review_date; ?></span></p>
      </div>
  </div>
<?php } 
if($reviews==''){
  echo '<p> There are no reviews to show.</p>';
}

?>


<hr>



    </div>
      </div>






            <div class="tab-pane fade" id="profile-2" role="tabpanel" aria-labelledby="profile-tab">
              <div class="available-jobs mt-5">
              <div class="row">
             <div class="col-md-8">

<?php //print_r($posts);
        foreach ($posts as $post) { ?>
         
        
        <div class="body-content">
          <div class="widget">
              <div class="title text-green"></div>
                <div class="time text-muted" title="<?=$post->post_date; ?>" ><?=$post->post_date; ?></div>
                <div class="discription"><?php echo $post->description; ?></div>
                <div class="gallery">
                  <div class="row">
                    <?php if($post->picture2 =='' && $post->picture3=='' && $post->picture4=='' ){ ?>

                      <div class="col-md-12">
                          <img src="<?php echo base_url('assets/img/'.$post->picture1); ?>" alt="" class="img-fluid">
                        </div>


                    <?php } else{ ?>
                      <div class="col-md-6">
                          <img src="<?php echo base_url('assets/img/'.$post->picture1); ?>" alt=""  class="img-fluid">
                        </div>
                        <div class="col-md-6">
                          <img src="<?php echo base_url('assets/img/'.$post->picture2); ?>" alt=""  class="img-fluid">
                        </div>
                        <div class="col-md-6">
                          <img src="<?php echo base_url('assets/img/'.$post->picture3); ?>" alt=""  class="img-fluid">
                        </div>
                        <div class="col-md-6">
                          <img src="<?php echo base_url('assets/img/'.$post->picture4); ?>" alt=""  class="img-fluid">
                        </div>
                    <?php } ?>

                    </div>
                
                </div>
                <div class="actions">
                  <a herf="#" class="text-muted"> <i class="fa fa-thumbs-up"></i><?php echo $this->UserModel->count_likes($post->id);?> Likes</a>
                    <a herf="#" onClick="sharepost('post<?=$post->id?>');" class="text-muted"> <i class="fa fa-share"></i> Share</a>
                         
        <div class="share" id="post<?=$post->id?>" style="display:none">        
         <div data-network="twitter" class="st-custom-button">Twitter</div>
<div data-network="facebook" class="st-custom-button">Facebook</div> 
<div data-network="linkedin" class="st-custom-button">LinkedIn</div> 
<div data-network="email" class="st-custom-button">Email</div> 
            </div>
                </div>
            </div>
        </div>


        
        <hr> 
<?php  } ?>
        
        
        </div>
          </div>
              </div>
                </div>
            
          </div>
        </div>

  </div>
  </div>
  
  
             <div class="mt-5"></div>
    
    
        
             <div class="mt-5"></div>
        
  </div>
  

</section>


<script>
$(document).ready(function(){
  
    $('[data-toggle="tooltip"]').tooltip(); 
});

	function sharepost(postid){
			$('#'+postid).show();
	}


</script>
