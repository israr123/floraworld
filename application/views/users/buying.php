<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<style>
.error {
  color:red;
}
</style>
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaKN0aL5oRLrpyj-hl0Wd5e-UxXKuB2cM&libraries=places&callback=initAutocomplete"
        async defer></script>
     <script>
 function initAutocomplete(){
    var input = document.getElementById('autocomplete');
    // var options = {
    //   types: ['(regions)'],
    //   componentRestrictions: {country: "IN"}
    // };
    var options = {}

    var autocomplete = new google.maps.places.Autocomplete(input, options);

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var place = autocomplete.getPlace();
      var lat = place.geometry.location.lat();
      var lng = place.geometry.location.lng();
      var placeId = place.place_id;
     
      document.getElementById("latitude").value = lat;
      document.getElementById("longitude").value = lng;
      document.getElementById("location_id").value = placeId;
    });
  }
    </script>


<!-- Masthead -->
<header class="breadcrumb-div text-white text-center">
  <div class="overlay"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12 mx-auto">
        <h3>Profile</h3>
      </div>
    </div>
  </div>
</header>
<section id="profile">
  <div class="container">     
                       
  
  
             <div class="mt-5"></div>
             
             
             
             
             
             
             <div class="index-tab-info">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item"> <a class="nav-link active" id="home-tab" data-toggle="tab" data-target="#home"  href="get_user_job_history" role="tab" aria-controls="home" aria-selected="true">Job History</a> </li>
            <li class="nav-item"> <a class="nav-link" id="profile-tab" data-target="#profile-2"  data-toggle="tab" href="get_user_new_jobs"  role="tab" aria-controls="profile-2" aria-selected="false">Current Jobs</a> </li>
			<li class="nav-item"> <a class="nav-link" id="profile-tab" data-toggle="tab" data-target="#profile-1" href="get_user_my_jobs" role="tab" aria-controls="profile-1" aria-selected="false">Running Jobs</a> </li>
            <li class="nav-item"> <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Post a Job</a> </li>
            <li class="nav-item"> <a class="nav-link" id="request-tab" data-toggle="tab" href="get_user_applied_jobs" data-target="#request" role="tab" aria-controls="request" aria-selected="false">Job Requests </a> </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
             
                <div class=" mt-5 table-responsive">
				<?php if(!empty($job_history)):  ?>
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Completion Date</th>
                      <th scope="col">Gardener</th>
                      <th scope="col">Description</th>
                      <th scope="col">Budget</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
					<?php foreach($job_history as $prev_jobs): if($prev_jobs->picture=="") $prev_jobs->picture="smallman.png" ;?>
                    <tr>
                      <td style="width: 12%;"><?php echo date('d-m-Y',strtotime( $prev_jobs->post_date)); ?></td>
                      <td><a href="gardner/<?php echo $prev_jobs->username; ?>"><img src="<?php echo base_url('assets/img/').$prev_jobs->picture;?>" class="rounded" height="50px"></a></td>
                      <td><a href="<?php echo base_url('myhistory_detail_page/'.base64_encode($prev_jobs->id)); ?>"><?php echo $prev_jobs->job_description; ?></a></td>
                      <td>$<?php echo $prev_jobs->cost; ?></td>
                    </tr>
                    <?php endforeach;?>
                  </tbody>
                </table>
          
			  <?php else: echo "There is no Job Available right now !"; endif; ?>
            </div>
            </div>
            <div class="tab-pane fade" id="profile-2" role="tabpanel" aria-labelledby="profile-tab">
         
              <div class="job-listing mt-5 table-responsive"> 
			  <?php if(!empty($new_jobs)):?>
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Date</th>
                      <th scope="col">Discription</th>
                      <th scope="col">Budget</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($new_jobs as $new_job): ?>
                    <tr>
                      <td style="width: 12%;"><?php echo $new_job->post_date; ?></td>
                      <td><?php echo $new_job->job_description; ?></td>
                      <td>$<?php echo $new_job->cost; ?></td>
                      <td>
                       </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
             
			  <?php else: echo "There is no Job Available right now !"; endif; ?>
            </div>
			 </div>
			
			
			
			
			
			<div class="tab-pane fade" id="profile-1" role="tabpanel" aria-labelledby="profile-tab">
        
              <div class="job-listing mt-5 table-responsive">
                 <?php if(!empty($running_jobs)):?>
				<table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Date</th>
                      <th scope="col">Discription</th>
                      <th scope="col">Budget</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($running_jobs as $running_job): if($running_job->picture=="") $running_job->picture="smallman.png" ; ?>
                    <tr>
                      <td style="width: 12%;"><?php echo $running_job->post_date; ?></td>
                      <td><?php echo $running_job->job_description; ?></td>
                      <td>$<?php echo $running_job->job_cost; ?></td>
                      <td><div class="buttonsets">
                       <?php if($running_job->job_status==3){ ?>
                          <a class="btn themebutton" href="<?php echo base_url('review_page/'.$running_job->id); ?>">Complete Job</a> </div></td>
					   <?php } ?>
					</tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
				<?php else: echo "There is no Job Available right now !"; endif; ?>
              </div>
            </div>
            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
            
            <div class="mt-5"></div>
            <form method="post" id="buyingform" action="<?php echo base_url(); ?>postajob" enctype="multipart/form-data">
            <div class="form-group">
                <label>Title</label>
                <input type="text" required name="title" class="form-control">
              </div>
              <div class="form-group">
                <label>Description:</label>
                <textarea name="description" required class="form-control"></textarea>
              </div>

               <div class="form-group">
                <label>Budget:</label>
                <input type="number" required name="cost" placeholde="$5200" class="form-control">
              </div>
               
              <div class="form-group" id="locationField">
                <label for="Location">Location:</label>
                <input required type="text" value="<?php //echo $location; ?>" name="location" class="form-control" id="autocomplete">
              </div>

              <div class="form-group" >
             <label >Date for work</label>
             <input required type="text" name="work_date" name="bday"  
                   class="form-control" id="datetimepicker4"  >
            </div>
			
              
                  <div class="form-group">
                <label>Select Image 1</label>
                <input required  title="Please Select a valid image format." type="file" name="picture1" class="form-control" id="pp">
              </div>
                 <div class="form-group">
                <label>Select Image 2 (optional)</label>
                <input type="file"  title="Please Select a valid image format."  name="picture2" class="form-control">
              </div>
                   <div class="form-group">
                <label>Select Image 3 (optional)</label>
                <input type="file" title="Please Select a valid image format."  name="picture3" class="form-control">
              </div>
                   <div class="form-group">
                <label>Select Image 4 (optional)</label>
                <input type="file"  title="Please Select a valid image format."  name="picture4" class="form-control">
              </div>
              <button type="submit" class="btn themebutton">Submit</button>
				<input type="hidden" name="lat" id="latitude" value="">
				<input type="hidden" name="lng" id="longitude" value="">
				<input type="hidden" name="locid" id="location_id" value="">
      </form>
           
            </div>





            <div class="tab-pane fade" id="request" role="tabpanel" aria-labelledby="request-tab"> 
            <div class="available-jobs mt-5 table-responsive">
                 <?php //echo "<pre>"; print_r($applied_jobs); die;?>
                 <?php if(!empty($applied_jobs)):?>
				<table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Date</th>
                      <th scope="col">Buyer</th>
                      <th scope="col">Description</th>
                      <th scope="col">Price Offer</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
				  		  <?php foreach($applied_jobs as $applied_job): if($applied_job->picture=="") $prev_jobs->picture="smallman.png" ; ?>
                    <tr>
                      <td style="width: 12%;"><?php echo  $applied_job->post_date; ?></td>
                      <td><a href="gardeners/<?php echo $applied_job->username; ?>"><img src="<?php echo base_url('assets/img/').$applied_job->picture;?>" class="rounded" height="50px"></a></td>
                      <td><?php echo $applied_job->description; ?></td>
                      <td>$<?php echo $applied_job->cost; ?></td>
                      <td>
					  <form action="<?php echo base_url('accept_job'); ?>" method="post" >
						  <div class="buttonsets">
							  <button type="button" class="btn  btn-grey">Reject</button>
							  
							  <button type="submit" class="btn themebutton">Accept</button>
							  <input type="hidden" value="'.$applied_job->job_id.'" name="job_id">
							  <input type="hidden" value="'.$applied_job->gardner_id.'" name="gardner_id">
							  
						  </div>
						</form>  
					 </td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
				<?php else: echo "There is no Job Available right now !"; endif; ?>
              </div>
            
            </div>










          </div>
        </div>
    
    
        
             <div class="mt-5"></div>
        
  </div>
  
  
  
  
  
</section>
<script type="text/javascript">
$(document).ready(function () {
	

$('#datetimepicker4').datepicker({ 
   maxDate: '+3m',
   minDate:  '+0d'
 });

    $('[data-toggle="tooltip"]').tooltip();           
	$('#datetimepicker4').datepicker({
	dateFormat: "yy-mm-dd"
		});

    
	
$('[data-toggle="tab"]').click(function(e) {
	    var $this = $(this),
        loadurl = $this.attr('href'),
        targ = $this.attr('data-target');

    $.get(loadurl, function(data) {
        $(targ).html(data);
		$('.time').timeago();
    });

    $this.tab('show');
    return false;
});

$("#buyingform").validate({
		  rules: {
		 password: "required",
				confirm_password: {
				  equalTo: "#password"
				},
		  phone: {
				  phoneUS: false,
				  matches:"[0-9]+()"
				},

		   picture1: {
      required: true,
      extension: "png|jpg|jpeg|gif"
      },
      picture2: {
      required: false,
      extension: "png|jpg|jpeg|gif"
      },
      picture3: {
      required: false,
      extension: "png|jpg|jpeg|gif"
      },
      picture4: {
      required: false,
      extension: "png|jpg|jpeg|gif"
			}


		  }
	   
	  });



});
function rejectjob(jobid,gardner_id)
{
	$.get('rejectjob/'+jobid+'/'+gardner_id);
	 $('#'+jobid).slideUp();
	return false;
}
</script>







