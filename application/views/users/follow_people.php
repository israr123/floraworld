

<header class="breadcrumb-div text-white text-center">
  <div class="overlay"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12 mx-auto">
        <form class="mb-5 mt-5">
              <div class="form-row">
              <div class="col-md-3"></div>
                <div class="col-12 col-md-4 mb-2 mb-md-0">
                  <input type="email" class="form-control form-control-lg" placeholder="Find a friend to follow">
                </div>
                <div class="col-12 col-md-2">
                  <button type="submit" class="btn btn-block btn-lg btn-dark themebutton">Search &nbsp; <i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
      </div>
    </div>
  </div>
</header>
<div class="follow-portion">
  <div class="container">
    <div class="row">
    	 <div class="col-md-3"></div>
      <div class="col-md-6">
        <div class="leftbar sidebarlist">
        	<ul class="list-group">
            <li class="list-group-item mb-4"><strong class="text-uppercase">Follow People</strong></li>
     <li class="list-group-item">
     
     <div class="media"><a href="#">
  <img src="../img/smallman.png" height="65px" width="65px" class="rounded" style="margin-top: -15px;margin-bottom: 5px;"></a>
  <div class="media-body" style="
    font-size:  12px;
    margin-left:  7%;
    text-align:  justify;
">
<h6>Rehman <a href="#" class="btn themebutton f-12 pull-right">Follow Up</a></h6>

  </div></div>
     </li>
     <hr>
   
       <li class="list-group-item">
     
     <div class="media"><a href="#">
  <img src="../img/smallman.png" height="65px" width="65px" class="rounded" style="margin-top: -15px;margin-bottom: 5px;"></a>
  <div class="media-body" style="
    font-size:  12px;
    margin-left:  7%;
    text-align:  justify;
">
<h6>Adnan 
<a href="#" class="btn themebutton f-12 pull-right">Follow Up</a></h6>
  </div></div>
     </li>
     <hr>
         <li class="list-group-item">
     
     <div class="media"><a href="#">
  <img src="../img/smallman.png" height="65px" width="65px" class="rounded" style="margin-top: -15px;margin-bottom: 5px;"></a>
  <div class="media-body" style="
    font-size:  12px;
    margin-left:  7%;
    text-align:  justify;
">
<h6>Faizan <a href="#" class="btn themebutton f-12 pull-right">Follow Up</a></h6>
  </div></div>
     </li>
            
          </ul>
        
        
        
        
        </div>
      </div>
    </div>
  </div>
</div>

