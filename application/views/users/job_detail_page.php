<?php 
if(empty($details))
{
	?><script>
	 window.history.go(-1);	
	 </script>
	<?php	
	die;
}
?>


<!-- Masthead -->
<header class="breadcrumb-div text-white text-center">
  <div class="overlay"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12 mx-auto">
        <h3>Job Detail</h3>
      </div>
    </div>
  </div>
</header>
<section id="signup">
  <div class="container">
    <div class="row">
      <div class="col col-md-12">
        <div class="detail-list">
          <div class="row">
            <div class="col-md-12">
              <div class="heading">
                <h6><?php echo $details[0]->job_title; ?></h6>
                <hr>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-9">
              <div class="detail-job">
                <p> <span class="text-green"><strong><?php echo $details[0]->username; ?></strong></span> <br>
                  <span class="f-12"><?php echo $details[0]->post_date; ?></span></p>
                <div class="mt-3"></div>
                <p><?php echo $details[0]->job_description; ?></p>
                
              </div>
            </div>
            <div class="col-md-3">
              <div class="detail-contact">
                <ul>
                  <li> <i class="fa fa-money"></i> <span><strong>$<?php echo $details[0]->cost; ?></strong> <br>
                    Fixed Price </span> </li>
                  <li>
                    <button  class="btn themebutton btn-block"  data-toggle="modal" data-target="#applybtnjob"> Apply</button>
                  </li>
                  <li>
                    <button type="button" class="btn btn-grey btn-block">Ignore</button>
                  </li>
                  <div class="mt-5"></div>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="mt-5"></div>
    <div class="gallery-images">
    <h4 class="text-center"> Featured Gallery</h4>
     <div class="mt-4"></div>
    <div class="row">
    
      <div class="col-md-3"> <img src="<?php echo base_url(); echo $details[0]->job_picture1; ?>" class="img-fluid" alt="" ></div>
      <div class="col-md-3"><img src="<?php echo base_url(); echo $details[0]->job_picture1; ?>" class="img-fluid" alt="" ></div>
      <div class="col-md-3"><img src="<?php echo base_url(); echo $details[0]->job_picture1; ?>" class="img-fluid" alt="" ></div>
      <div class="col-md-3"><img src="<?php echo base_url(); echo $details[0]->job_picture1; ?>" class="img-fluid" alt="" ></div>
      </div>
    </div>
  </div>
  
  <!-- Modal -->

  <div class="modal fade" id="applybtnjob" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Please enter amount</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        </div>

      <form method="post" action="<?php echo base_url(); ?>apply_for_job/<?php echo $details[0]->id; ?>" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6 align-content-center">
              <div class="form-group pull-right">
                <label>Amount</label>
                <input type="number" name="cost" class="form-control" placeholder="$54000">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Discription</label>
                <textarea  class="form-control" name="description" placeholder="Write a Short Discription"></textarea>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button  class="btn themebutton"> Send</button>
        </div>
     </form>
      </div>
    </div>
  </div>

</section>

