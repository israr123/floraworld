
<!-- Masthead -->

<header class="breadcrumb-div text-white text-center">
  <div class="overlay"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12 mx-auto">
        <form class="mb-5 mt-5">
              <div class="form-row">
              <div class="col-md-3"></div>
                <div class="col-12 col-md-4 mb-2 mb-md-0">
                  <input type="email" class="form-control form-control-lg" placeholder="Find a gardener">
                </div>
                <div class="col-12 col-md-2">
                  <button type="submit" class="btn btn-block btn-lg btn-dark themebutton">Search &nbsp; <i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
      </div>
    </div>
  </div>
</header>
<div class="follow-portion">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="leftbar sidebarlist">
        	<ul class="list-group">
            <li class="list-group-item mb-4"><strong class="text-uppercase">Follow People</strong> <span class="pull-right"><a href="follow_people.html">See All</a></span></li>
     <li class="list-group-item">
     
     <div class="media"><a href="#">
  <img src="<?php echo base_url('assets/img');?>/smallman.png" height="65px" width="65px" class="rounded"></a>
  <div class="media-body" style="
    font-size:  12px;
    margin-left:  7%;
    text-align:  justify;
">
<h6>Rehman</h6>
<a href="#" class="btn themebutton f-12">Follow Up</a>
  </div></div>
     </li>
     <hr>
   
       <li class="list-group-item">
     
     <div class="media"><a href="#">
  <img src="<?php echo base_url('assets/img/');?>/smallman.png" height="65px" width="65px" class="rounded"></a>
  <div class="media-body" style="
    font-size:  12px;
    margin-left:  7%;
    text-align:  justify;
">
<h6>Adnan</h6>
<a href="#" class="btn themebutton f-12">Follow Up</a>
  </div></div>
     </li>
     <hr>
         <li class="list-group-item">
     
     <div class="media"><a href="#">
  <img src="<?php echo base_url('assets/img/');?>/smallman.png" height="65px" width="65px" class="rounded"></a>
  <div class="media-body" style="
    font-size:  12px;
    margin-left:  7%;
    text-align:  justify;
">
<h6>Faizan</h6>
<a href="#" class="btn themebutton f-12">Follow Up</a>
  </div></div>
     </li>
            
          </ul>
        
        
        
        
        </div>
      </div>
      <div class="col-md-6">
	  
        <div class="body-content">
        	<div class="widget">
            	<div class="title text-green">Amjad Azeem</div>
                <div class="time text-muted">10 minutes ago</div>
                <div class="discription">Lorem Ipsum copy in various charsets and languages for layouts</div>
                <div class="gallery">
                	<div class="row">
                    	<div class="col-md-6">
                        	<img src="<?php echo base_url('assets/img/');?>/bg01.jpg" class="img-fluid">
                        </div>
                        <div class="col-md-6">
                        	<img src="<?php echo base_url('assets/img/');?>/bg01.jpg" class="img-fluid">
                        </div>
                        <div class="col-md-6">
                        	<img src="<?php echo base_url('assets/img/');?>/bg01.jpg" class="img-fluid">
                        </div>
                        <div class="col-md-6">
                        	<img src="<?php echo base_url('assets/img/');?>/bg01.jpg" class="img-fluid">
                        </div>
                    
                    </div>
                
                </div>
                <div class="actions">
                	<a herf="#" class="text-muted"> <i class="fa fa-thumbs-up"></i> Like</a>
                    <a herf="#" class="text-muted"> <i class="fa fa-share"></i> Share</a>
                
                </div>
            
            </div>
        
        </div>
        
        <hr> 
        
            <div class="body-content">
        	<div class="widget">
            	<div class="title text-green">Sadia Azeem</div>
                <div class="time text-muted">10 minutes ago</div>
                <div class="discription">Lorem Ipsum copy in various charsets and languages for layouts</div>
                <div class="gallery">
                	<div class="row">
                    	<div class="col-md-6">
                        	<img src="<?php echo base_url('assets/img/');?>/bg01.jpg" class="img-fluid">
                        </div>
                        <div class="col-md-6">
                        	<img src="<?php echo base_url('assets/img/');?>/bg01.jpg" class="img-fluid">
                        </div>
                       
                    
                    </div>
                
                </div>
                <div class="actions">
                	<a herf="#" class="text-muted"> <i class="fa fa-thumbs-up"></i> Like</a>
                    <a herf="#" class="text-muted"> <i class="fa fa-share"></i> Share</a>
                
                </div>
            
            </div>
        
        </div>
        
         <hr> 
        
            <div class="body-content">
        	<div class="widget">
            	<div class="title text-green">Sadia Azeem</div>
                <div class="time text-muted">10 minutes ago</div>
                <div class="discription">Lorem Ipsum copy in various charsets and languages for layouts</div>
                <div class="gallery">
                	<div class="row">
                    	<div class="col-md-12">
                        	<img src="<?php echo base_url('assets/img/');?>/bg-masthead.jpg" class="img-fluid">
                        </div>
                      
                       
                    
                    </div>
                
                </div>
                <div class="actions">
                	<a herf="#" class="text-muted"> <i class="fa fa-thumbs-up"></i> Like</a>
                    <a herf="#" class="text-muted"> <i class="fa fa-share"></i> Share</a>
                
                </div>
            
            </div>
        
        </div>
        
         <hr> 
        
            <div class="body-content">
        	<div class="widget">
            	<div class="title text-green">Sadia Azeem</div>
                <div class="time text-muted">10 minutes ago</div>
                <div class="discription">Lorem Ipsum copy in various charsets and languages for layouts</div>
                <div class="gallery">
                	<div class="row">
                    	<div class="col-md-6">
                        	<img src="<?php echo base_url('assets/img/');?>/bg01.jpg" class="img-fluid">
                        </div>
                        <div class="col-md-6">
                        	<img src="<?php echo base_url('assets/img/');?>/bg01.jpg" class="img-fluid">
                        </div>
                          <div class="col-md-6">
                        	<img src="<?php echo base_url('assets/img/');?>/bg01.jpg" class="img-fluid">
                        </div>
                       
                    
                    </div>
                
                </div>
                <div class="actions">
                	<a herf="#" class="text-muted"> <i class="fa fa-thumbs-up"></i> Like</a>
                    <a herf="#" class="text-muted"> <i class="fa fa-share"></i> Share</a>
                
                </div>
            
            </div>
        
        </div>
      </div>
      <div class="col-md-3">
        <div class="rightbar sidebarlist">
        	<ul class="list-group">
            <li class="list-group-item mb-4"><strong class="text-uppercase">Gardeners near by you</strong> <span class="pull-right"><a href="../listing.html">See All</a></span></li>
     <li class="list-group-item">
     
     <div class="media"><a href="#">
  <img src="<?php echo base_url('assets/img/');?>/smallman.png" height="65px" width="65px" class="rounded"></a>
  <div class="media-body" style="
    font-size:  12px;
    margin-left:  7%;
    text-align:  justify;
">
<h6 class="mb-0">Green tree</h6>
<p class="mb-1">
Lorem Ipsum copy in various charsets and languages for layouts</p>
<a href="#" class="btn themebutton f-12">Detail</a>
  </div></div>
     </li>
     <div class="mt-3"></div>
         <li class="list-group-item">
     
     <div class="media"><a href="#">
  <img src="<?php echo base_url('assets/img/');?>/smallman.png" height="65px" width="65px" class="rounded"></a>
  <div class="media-body" style="
    font-size:  12px;
    margin-left:  7%;
    text-align:  justify;
">
<h6 class="mb-0">Green tree</h6>
<p class="mb-1">
Lorem Ipsum copy in various charsets and languages for layouts</p>
<a href="#" class="btn themebutton f-12">Detail</a>
  </div></div>
     </li>
     <div class="mt-3"></div>
     
         <li class="list-group-item">
     
     <div class="media"><a href="#">
  <img src="<?php echo base_url('assets/img/');?>/smallman.png" height="65px" width="65px" class="rounded"></a>
  <div class="media-body" style="
    font-size:  12px;
    margin-left:  7%;
    text-align:  justify;
">
<h6 class="mb-0">Green tree</h6>
<p class="mb-1">
Lorem Ipsum copy in various charsets and languages for layouts</p>
<a href="#" class="btn themebutton f-12">Detail</a>
  </div></div>
     </li>
     <div class="mt-3"></div>
     
     
          </ul>
        
        
        
        
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Footer -->


<!-- Bootstrap core JavaScript --> 
<script src="../plugin/jquery/jquery.min.js"></script> 
<script src="../plugin/bootstrap/js/bootstrap.js"></script>
</body>
</html>
