<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- Masthead -->
<script>

	function sharepost(postid){
			$('#'+postid).show();
	}
</script>
<header class="breadcrumb-div text-white text-center">
  <div class="overlay"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12 mx-auto">
        <form method="post" action="search_gardeners" class="mb-5 mt-5">
              <div class="form-row">
              <div class="col-md-3"></div>
                <div class="col-12 col-md-4 mb-2 mb-md-0">
                  <input name="keyword" id="gardener" type="text" class="form-control form-control-lg" placeholder="Find a gardener">
                </div>
                <div class="col-12 col-md-2">
                  <button type="submit" class="btn btn-block btn-lg btn-dark themebutton">Search &nbsp; <i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
      </div>
    </div>
  </div>
</header>
<div class="follow-portion">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="leftbar sidebarlist">
        	<ul class="list-group">
            <li class="list-group-item mb-4"><strong class="text-uppercase">Follow People</strong> <span class="pull-right"><a href="follow_people"></a></span></li> 
    
<?php foreach($users as $user): if($user->picture=="") $user->picture="smallman.png"; ?>
 <li class="list-group-item">
     
     <div class="media"><a href="<?=base_url('user/').$user->username; ?>">
  <img alt="" src="<?=base_url('assets/img/').$user->picture;?>" height="65px" width="65px" class="rounded"></a>
  <div class="media-body" style="
    font-size:  12px;
    margin-left:  7%;
    text-align:  justify;
">

<h6><?php echo $user->f_name; ?></h6>
<a href="follow/<?=$user->id;?>" class="btn themebutton f-12 follow">Follow Up</a>
  </div></div>
     
	 </li>
   
   
       
<?php endforeach; ?>
  
          </ul>
        
        
        
        
        </div>
      </div>
	  
      <div class="col-md-6" id="wall">
	  <?php if(!empty($feeds)):  ?>
	  <?php foreach($feeds as $feed): 
			$total_likes=$this->UserModel->count_likes($feed->id);
			?>
        <div class="body-content">
        	<div class="widget">
            	<div class="title text-green"><?php echo $feed->f_name." ".$feed->l_name;?> </div>
                <div class="time text-muted" title="<?php echo $feed->post_date; ?>"><?php echo $feed->post_date; ?></div>
                <div class="discription"><?php echo substr($feed->description,0,200); ?>. . .</div>
                <div class="gallery">
                	<div class="row">
					
                    	<div class="col-md-<?php if($feed->picture2 =='' && $feed->picture3=='' && $feed->picture4=='' ) echo '12'; else echo '6';?>">
                        	<img alt="" src="<?php echo base_url('assets/img/');?>/<?php echo $feed->picture1; ?>" class="img-fluid" >
                        </div>
                        <div class="col-md-6">
                        	<img alt="" src="<?php echo base_url('assets/img/');?>/<?php echo $feed->picture2; ?>" class="img-fluid" >
                        </div>
                        <div class="col-md-6">
                        	<img alt="" src="<?php echo base_url('assets/img/');?>/<?php echo $feed->picture3; ?>" class="img-fluid" >
                        </div>
                        <div class="col-md-6">
                        	<img alt=""  src="<?php echo base_url('assets/img/');?>/<?php echo $feed->picture4; ?>" class="img-fluid" >
                        </div>
                    
                    </div>
                
                </div>
                <div class="actions">
                	<a herf="#" class="text-muted"> <i id="like<?=$feed->id;?>" class="fa fa-thumbs-up like"></i> <span id="total_likeslike<?=$feed->id;?>"><?=$total_likes;?></span> Likes</a>
                    <a herf="#" onClick="sharepost('post<?=$feed->id?>');" class="text-muted"> <i class="fa fa-share"></i> Share</a>
                
        <div class="share" id="post<?=$feed->id?>" style="display:none">        
         <div data-network="twitter" class="st-custom-button">Twitter</div>
<div data-network="facebook" class="st-custom-button">Facebook</div> 
<div data-network="linkedin" class="st-custom-button">LinkedIn</div> 
<div data-network="email" class="st-custom-button">Email</div> 
            </div>
			
			</div>
			
			</div>
        
        </div>
       <?php endforeach; endif; ?>
      </div>
      <div class="col-md-3">
        <div class="rightbar sidebarlist">
        	<ul class="list-group">



     <li class="list-group-item mb-4"><strong class="text-uppercase">Gardeners near by you</strong> <span class="pull-right"><a href="<?php echo base_url('listing/') ;?>">See All</a></span></li>       
			
	<?php foreach($gardners as $gardner): if($gardner->picture=="") $gardner->picture="smallman.png"; ?>		

            

     <li class="list-group-item">
     
     <div class="media"><a href="gardner/<?=$gardner->username;?>">
  <img alt="" src="<?php echo base_url('assets/img/');?><?=$gardner->picture;?>" height="65px" width="65px" class="rounded"></a>
  <div class="media-body" style="
    font-size:  12px;
    margin-left:  7%;
    text-align:  justify;
">
<h6 class="mb-0"><?=$gardner->f_name." ".$gardner->l_name;?></h6>
<p class="mb-1"><?=substr($gardner->description,0,100);?></p>
<a href="gardner/<?=$gardner->username;?>" class="btn themebutton f-12">Detail</a>
  </div></div>
     </li>   <div class="mt-3"></div>
	 <?php endforeach; ?>
  

  
     
     
          </ul>
        
        
        
        
        </div>
      </div>
    </div>
  </div>
</div>

	<?php $baseurl=base_url(); ?>
<script type="text/javascript">

	$( ".follow" ).click(function() {
		
		
		 
		 $followurl=$(this).attr("href");
		 $.post($followurl); 
		 $(this).closest('li').fadeOut( "slow" );
		 
		   return false;
	});
	


            $( "#gardener" ).autocomplete({
              source: "<?php echo site_url('search_gardeners_ajax');?>"
            });

			
			$('#like').click(function() {
		$.post( "add_likes", { post_id:  $('#post_id').val() } , function( data ) { 
			if(data == 1)
			{
				$('#total_likes').text( parseInt($('#total_likes').text()) + 1 );
			}
		});
	 
	});
	$('.like22').click(function() {
		var total_likes;
		var idnumber;
		idnumber=this.id;
		total_likes=parseInt($('#total_likes'+idnumber).text());
	
		$.post( "add_likes", { post_id: idnumber } , function( data ) { 
			if(data == 1)
			{
				$('#total_likes'+idnumber).text( total_likes + 1 );
			}
		});
	 
	});
	
	$(document).on('click', '.like', function() {
		
		var total_likes;
		var idnumber;
		idnumber=this.id;
		total_likes=parseInt($('#total_likes'+idnumber).text());
	
		$.post( "add_likes", { post_id: idnumber } , function( data ) { 
			if(data == 1)
			{
				$('#total_likes'+idnumber).text( total_likes + 1 );
			}
		});
		
	});
	
	
</script>
