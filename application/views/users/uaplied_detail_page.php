 <?php 
if(empty($details))
{
	?><script>
	 window.history.go(-1);	
	 </script>
	<?php	
	die;
}
?>
<!-- Masthead -->
<header class="breadcrumb-div text-white text-center">
  <div class="overlay"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12 mx-auto">
        <h3>Job Detail</h3>
      </div>
    </div>
  </div>
</header>
<section id="signup">
  <div class="container">
    <div class="row">
      <div class="col col-md-12">
        <div class="detail-list">
          <div class="row">
            <div class="col-md-12">
              <div class="heading">
                <h6><?php echo $details[0]->job_title; ?></h6>
                <hr>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-9">
              <div class="detail-job">
                <p> <span class="text-green"><strong><?php echo $details[0]->username; ?></strong></span> <br>
                  <span class="f-12 time" title="<?php echo " ".$details[0]->apply_date; ?>" >Applied: <?php echo " ".$details[0]->apply_date; ?></span></p>
                <div class="mt-3"></div>
                <h6>Job Description: </h6>
                <p><?php echo $details[0]->job_description; ?></p>
                <br>
                <br>

                <h6>Gardener Message: </h6>
                <p><?php echo $details[0]->description; ?></p>
                
              </div>
            </div>
            <div class="col-md-3">
              <div class="detail-contact">
                <ul>
                  <li> <i class="fa fa-money"></i> <span>Actual Price: <strong>$<?php echo $details[0]->job_cost; ?></strong> <br>
                     </span> </li>

                  <li> <i class="fa fa-money"></i> <span>Bid Price: <strong>$<?php echo $details[0]->cost; ?></strong> <br>
                     </span> </li>
                <form action="<?php echo base_url('accept_job'); ?>"
                   method="post">

                  <li>
                    <button type="submit" class="btn themebutton btn-block"  data-toggle="modal" data-target="#applybtnjob"> Accept</button>
                  </li>
                  <input type="hidden" value="<?php echo $gardner_id; ?>" name="gardner_id">
                  <input type="hidden" value="<?php echo $details[0]->id; ?>" name="job_id">

                </form>
                  <!--<li>
                    <button type="button" class="btn btn-grey btn-block">Reject</button>
                  </li> -->
                  <div class="mt-5"></div>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="mt-5"></div>
    <div class="gallery-images">
    <h4 class="text-center"> Featured Gallery</h4>
     <div class="mt-4"></div>
    <div class="row">
    
      <div class="col-md-3"> <img src="<?php echo base_url(); echo $details[0]->job_picture1; ?>" class="img-fluid" alt="" ></div>
      <div class="col-md-3"><img src="<?php echo base_url(); echo $details[0]->job_picture1; ?>" class="img-fluid" alt="" ></div>
      <div class="col-md-3"><img src="<?php echo base_url(); echo $details[0]->job_picture1; ?>" class="img-fluid" alt="" ></div>
      <div class="col-md-3"><img src="<?php echo base_url(); echo $details[0]->job_picture1; ?>" class="img-fluid" alt="" ></div>
      </div>
    </div>
  </div>
  
  <!-- Modal -->


</section>

