<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- Masthead -->
    <header class="masthead text-white text-center">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h1 class="mb-5">Gardeners for Organic Gardens | Palo Alto Based</h1>
            <h2>Find experts. Grow organic gardens</h2>
          </div>
          <div class="col-md-10 col-lg-8 col-xl-7 mx-auto mt-5">
            <form method="post" action="search_gardeners">
              <div class="form-row">
                <div class="col-12 col-md-6 mb-2 mb-md-0">
                  <input name="keyword" id="gardener" type="text" class="form-control form-control-lg" placeholder="Find a gardener">
                </div>
                <!--<div class="col-12 col-md-3 mb-2 mb-md-0">
                  <input type="email" class="form-control form-control-lg" placeholder="Location">
                </div>-->
                <div class="col-12 col-md-3">
                  <button type="submit" class="btn btn-block btn-lg btn-dark themebutton">Search &nbsp; <i class="fa fa-search"></i></button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </header>

    <!-- about -->
    <section class="about text-center bg-light">
      <div class="container">
        <h2 class="mb-5">Grow Your Own Food in an Edible Garden for the Benefit of All</h2>
        <div class="row">
          <div class="col-lg-12 col-xs-12">
            <div class="testimonial-item mx-auto mb-5 mb-lg-0">
               <p class="font-weight-light mb-0">We believe in the power of the individual and our ability to transform our lives and planet one edible garden at a time. Flora World makes it easy to install and maintain an edible garden in your home or business by connecting you with highly experienced edible landscape specialists in your area. <br>
Healthy Diet - Reduced Carbon Footprint - Healthier Planet</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Icons Grid -->
    <section class="features-icons bg-light text-center">
      <div class="container">
        <div class="row">
        
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-layers m-auto text-success"></i>
              </div>
              <h3>Find a Terraformer</h3>
              <p class="lead mb-0">Our pool of highly experienced edible landscape specialists are ready to assist you in creating the garden to support your needs.</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-check m-auto text-success"></i>
              </div>
              <h3>Book Instantly</h3>
              <p class="lead mb-0">Schedule the appointment and pay online. No need to deal with cash or invoices</p>
            </div>
          </div>
            <div class="col-lg-4">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <div class="features-icons-icon d-flex">
                <i class="icon-emotsmile m-auto text-success"></i>
              </div>
              <h3>Create Your Garden</h3>
              <p class="lead mb-0">Specify your herb, vegetable and fruit needs to your edible landscaper. <br>

+ <br>

Maintenance Plan (Optional)
<br>
+ <br>

Donate Your Excess (Optional)</p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Image Showcases -->
    <section class="showcase">
      <div class="container-fluid p-0">
        <div class="row no-gutters">

          <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url(<?php echo base_url('assets/img/bg-showcase-1.jpg');  ?>);"></div>
          <div class="col-lg-6 order-lg-1 my-auto showcase-text">
            <h2>Beautiful Edible Landscaping, Floral, Beekeeping, Preserving</h2>
            <p class="lead mb-0">$197/ Hour </p>
          </div>
        </div>
        <div class="row no-gutters">
          <div class="col-lg-6 text-white showcase-img" style="background-image: url(<?php echo base_url('assets/img/bg-showcase-2.jpg');  ?> );"></div>
          <div class="col-lg-6 my-auto showcase-text">
            <h2>Farmscape</h2>
            <p class="lead mb-0">$197/ Hour </p>
          </div>
        </div>
        <div class="row no-gutters">
          <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url(<?php echo base_url('assets/img/bg-showcase-3.jpg');  ?>);"></div>
          <div class="col-lg-6 order-lg-1 my-auto showcase-text">
            <h2>Edible Landscape Design</h2>
            <p class="lead mb-0">$197/ Hour </p>
          </div>
        </div>
      </div>
    </section>



    <!-- Call to Action -->
    <section class="call-to-action text-white text-center mt-5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <h2 class="mb-2">Are You An Edible Gardening Professional?</h2>
            <p class="mb-4">Flora World is constantly adding to our community and would like to invite you to join us.</p>
            
                 <!-- <button type="submit" class="btn btn-dark themebutton">Join our Flora World Community!</button>-->
          </div>

        </div>
      </div>
    </section>
<script type="text/javascript">

            $( "#gardener" ).autocomplete({
              source: "<?php echo site_url('search_gardeners_ajax');?>"
            });

</script>
    