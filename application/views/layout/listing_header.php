<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Flora World</title>

<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(); ?>assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template -->
<link href="<?php echo base_url(); ?>assets/plugin/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- Custom styles for this template -->
<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/plugin/jquery/jquery.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/plugin/bootstrap/js/bootstrap.js"></script> 
</head>

<body onload="activeClasses()">

<!-- Navigation -->

<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light"> <a class="navbar-brand" href="index.html"><img src="<?php echo base_url(); ?>assets/img/logo2.png"> </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active"> <a class="nav-link" href="about.html">About <span class="sr-only">(current)</span></a> </li>
      <li class="nav-item"> <a class="nav-link" href="listing.html">Listings</a> </li>
      <li class="nav-item"> <a class="nav-link" href="contact.html">Contact Us</a> </li>
    </ul>
    <ul class="navbar-nav justify-content-end">
      <li class="nav-item"> <a class="nav-link" href="singup.html">Sign Up </a> </li>
      <li class="nav-item"> <a class="nav-link" href="login.html">Login</a> </li>
      <li class="nav-item">
        <a type="button" class="btn btn-dark themebutton" href="becomegardener.html">+ Become a Gardener</a>
      </li>
    </ul>
  </div>
</nav>
