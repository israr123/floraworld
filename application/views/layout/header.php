<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Flora World</title>

<!-- Bootstrap core CSS -->
<link href="<?php echo base_url(); ?>assets/plugin/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom fonts for this template -->
<link href="<?php echo base_url(); ?>assets/plugin/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

<!-- Custom styles for this template -->
<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">

<script src="<?php echo base_url(); ?>assets/plugin/jquery/jquery.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/plugin/bootstrap/js/bootstrap.js"></script> 
<script type='text/javascript' src="<?php echo base_url(); ?>assets/plugin/jquery/jquery.timeago.js"></script>
<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5b0dd1e33dfc7c0011da10ca&product=inline-share-buttons' async='async'></script> 
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5b0dd1e33dfc7c0011da10ca&product=custom-share-buttons"></script>

<?php 
	$user="";
	if($this->session->userdata('user_id')) $user='user'; 
	if($this->session->userdata('gardner_id')) $user='grd'; 
	$baseurl=base_url();
	 
?>
<script type="text/javascript">
$(document).ready(function() {

$('.time').timeago();

var baseurl='<?=$baseurl;?>';


	
	if('<?php echo $user;?>'=='grd')
	{
		$.get( baseurl+"get_grd_notifications", function( data ) {
			  $( ".nlist" ).html( data);
			  
			}, "json" );

			$.get(baseurl+"get_grd_total_notifications", function( data ) {
				if (data >0 )
			  $( "#total_notification" ).html( data);
			  
			});
    
	
			$('#navbarDropdownNotificationLink').click(function(){
				$.get( baseurl+"get_grd_notifications", function( data ) {
						$( ".nlist" ).html( data);

				}, "json" );

				$.get( baseurl+"get_grd_total_notifications", function( data ) {
					if (data >0 )
						$( "#total_notification" ).html( data);
			  
				});
			
				$.get( baseurl+"hide_grd_notifications");
				$( "#total_notification" ).fadeOut( "slow" );
			});
	
	
	
			window.setInterval(function() {
				
		   $.get( baseurl+"get_grd_notifications", function( data ) {
			  $( ".nlist" ).html( data);
			  
			}, "json" );

			$.get(baseurl+"get_grd_total_notifications", function( data ) {
			if (data >0 )
			{
				 $( "#total_notification" ).html( data);
				// $('#chatAudio')[0].play();
			}
			 
			  
			});
			
		}, 30000);
	}
	
	if('<?php echo $user;?>'=='user')
	{
			
			$.get( baseurl+"get_user_notifications", function( data ) {
			  $( ".nlist" ).html( data);
			  
			}, "json" );

			$.get( baseurl+"get_user_total_notifications", function( data ) {
				if (data >0 )
			  $( "#total_notification" ).html( data);
			  
			});
            $( ".follow" ).click(function() {
				$.get( baseurl+"get_posts", function( data ) {
			  $( "#wall" ).prepend( data);
			  
				}, "json" );
			
			});
			$('#navbarDropdownNotificationLink').click(function(){
		
				$.get( baseurl+"get_user_notifications", function( data ) {
						$( ".nlist" ).html( data);

				}, "json" );

				$.get( baseurl+"get_user_total_notifications", function( data ) {
					if (data >0 )
						$( "#total_notification" ).html( data);
			  
				});
			
				$.get( baseurl+"hide_user_notifications");
				$( "#total_notification" ).fadeOut( "slow" );
			});
			
		
		
			window.setInterval(function() {
				if('<?=$this->session->userdata("user_id");?>'=="") location.reload();
		   $.get( baseurl+"get_user_notifications", function( data ) {
			  $( ".nlist" ).html( data);
			  
			}, "json" );
				
			$.get( baseurl+"get_user_total_notifications", function( data ) {
				if(data>0)
				{
					 //$('#chatAudio')[0].play();
					 $( "#total_notification" ).html( data);
				}
			  
			  
			});
			
			$.get( baseurl+"get_posts", function( data ) {
			  $( "#wall" ).prepend( data);
			  
			}, "json" );
			

		}, 30000);
	}

	
	
});
(function(document) {
   var shareButtons = document.querySelectorAll(".st-custom-button[data-network]");
   for(var i = 0; i < shareButtons.length; i++) {
      var shareButton = shareButtons[i];
      
      shareButton.addEventListener("click", function(e) {
         var elm = e.target;
         var network = elm.dataset.network;
         
         console.log("share click: " + network);
      });
   }
})(document);
</script>

<style>

.st-custom-button[data-network] {
   background-color: #78ab83;
   display: inline-block;
   padding: 5px 10px;
   cursor: pointer;
   font-weight: bold;
   color: #fff;
   }
#wall{
	max-height:800px;
	overflow:scroll;
}

</style>
</head>

<body>
<audio id="chatAudio"><source src="<?=base_url('assets/sound/');?>water_droplet_3.mp3" atype="audio/mp3"></audio>
<!-- Navigation -->

<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light"> <a class="navbar-brand" href=" <?php echo base_url();?>"><img src="<?php echo base_url(); ?>assets/img/logo2.png"> </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
    <?php if($this->session->userdata('user_id')){ ?>
      <li class="nav-item"> <a class="nav-link" href="<?php echo base_url();?>uindex">Home <span class="sr-only">(current)</span></a> </li>
      <?php }?>

      <?php if($this->session->userdata('gardner_id')){ ?>
      <li class="nav-item"> <a class="nav-link" href="<?php echo base_url();?>gindex ">Home <span class="sr-only">(current)</span></a> </li>
      <?php }?>
      <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('about/'); ?>">About <span class="sr-only">(current)</span></a> </li>

      <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('listing'); ?>">Listings</a> </li>
      <li class="nav-item"> <a class="nav-link" href="<?php echo base_url('contactus/'); ?>">Contact Us</a> </li>
    </ul>

<!-- section1 -->
<?php if($this->session->userdata('user_id')){ ?>
         <ul class="navbar-nav justify-content-end">
          <li class="nav-item dropdown dropdown-menu-right notification-list"> <a class="notification-fw nav-link dropdown-toggle" href="buying" id="navbarDropdownNotificationLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-bell"></i> <span id="total_notification" class="badge badge-danger"></span> </a>
            <div class="dropdown-menu nlist" aria-labelledby="navbarDropdownNotificationLink"> 
			
			<a class="dropdown-item" href="">
			<strong> There is no any notification to show. </strong>
			</a>
			 
			  
			</div>
          </li>
          <li class="nav-item dropdown account-list"> <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-user-circle-o fa-2x"></i> </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> <a class="dropdown-item" href="<?php echo base_url();?>mysettings">Setting</a> <a class="dropdown-item" href="<?php echo base_url();?>myprofile">Profile</a> <a class="dropdown-item" href="<?php echo base_url();?>logout">Logout</a> </div>
          </li>
          <li class="nav-item mynavclass-02"> <a href="<?php echo base_url(); ?>create_social_feed" class="btn themebutton"> Social</a> </li>
           <li class="nav-item mynavclass-02"> <a href="<?php echo base_url(); ?>buying" class="btn themebutton"> Buying</a> </li>
        </ul>
<?php } 
        else{
          
          if($this->session->userdata('gardner_id')){ ?>

              <ul class="navbar-nav justify-content-end">
             <li class="nav-item dropdown dropdown-menu-right notification-list"> <a class="notification-fw nav-link dropdown-toggle" href="buying" id="navbarDropdownNotificationLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-bell"></i> <span id="total_notification" class="badge badge-danger"></span> </a>
            <div class="dropdown-menu nlist" aria-labelledby="navbarDropdownNotificationLink"> 
			
			<a class="dropdown-item" href="">
			<strong> There is no any notification to show. </strong>
			</a> 
			 
			  
			</div>
          </li>
              <li class="nav-item dropdown account-list"> <a class="nav-link dropdown-toggle" href="" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-user-circle-o fa-2x"></i> </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> <a class="dropdown-item" href="<?php echo base_url();?>settings">Setting</a> <a class="dropdown-item" href="<?php echo base_url();?>profile">Profile</a> <a class="dropdown-item" href="<?php echo base_url();?>logout">Logout</a> </div>
              </li>
             <li class="nav-item mynavclass-02"> <a class="btn themebutton25"> </a> </li>
            </ul>
     
    <?php }
           else{ ?>

<!-- section2 -->
            <ul class="navbar-nav justify-content-end">
              <li class="nav-item"> <a class="nav-link" href="<?php echo base_url();?>users/register">Sign Up </a> </li>
              <li class="nav-item"> <a class="nav-link" href="<?php echo base_url();?>login">Login</a> </li>
              <li class="nav-item">
                <a type="button" class="btn btn-dark themebutton" href="<?php echo base_url();?>gardners/register">+ Become a Gardener</a>
              </li>
            </ul>

       <?php } 

      } ?>

  </div>
</nav>




<?php
              if($this->session->flashdata('error'))
                {
                  echo "
                <div class='well' style='margin-top: 100px;'>
                  <div class='alert alert-danger alert-dismissable'>
                          <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>
                            &times;
                             </button>
                             <strong> Error! </strong> ".$this->session->flashdata('error')."
                          </div>
                          </div>

                          ";
                }
  
                if($this->session->flashdata('success'))
                {
                  echo "
                  <div class='well' style='margin-top: 100px;'>
                  <div class='alert alert-success alert-dismissable'>
                          <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>
                            &times;
                             </button>
                             <strong> Success! </strong> ".$this->session->flashdata('success')."
                          </div>
                          </div>
                          ";
                }
        
              ?>
       