<!-- Masthead -->
<header class="breadcrumb-div text-white text-center">
  <div class="overlay"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12 mx-auto">
        <h3>Landing Page</h3>
      </div>
    </div>
  </div>
</header>
<section id="signup">
  <div class="container">
    <div class="row">
      <div class="col col-md-12">
        <div class="index-tab-info">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">  
		
			
			<a class="nav-link active" data-target="#home" id="home-tab" data-toggle="tab" href="get_grd_job_history" role="tab" aria-controls="home" aria-selected="true">Job History</a>


			</li>
            <li class="nav-item"> <a class="nav-link" id="profile-tab" data-toggle="tab" data-target="#profile"  href="get_grd_new_jobs" role="tab" aria-controls="profile" aria-selected="false">Avalibale Jobs</a> </li>
            <li class="nav-item"> <a class="nav-link"  data-target="#contact" id="contact-tab" data-toggle="tab" href="get_grd_my_jobs" role="tab" aria-controls="contact" aria-selected="false">Running Jobs</a> </li>
            <li class="nav-item"> <a class="nav-link" id="contact-tab" data-toggle="tab" data-target="#applied" href="get_grd_applied_jobs" role="tab" aria-controls="contact" aria-selected="false">Applied Jobs</a> </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
              <div class="job-listing mt-5 table-responsive">
                 <?php if(!empty($job_history)):?>
				<table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Date</th>
                      <th scope="col">Buyer</th>
                      <th scope="col">Discription</th>
                      <th scope="col">Budget</th>
                    </tr>
                  </thead>
                  <tbody>
				  <?php foreach($job_history as $prev_job): if($prev_job->picture=="") $prev_job->picture="smallman.png" ; ?>
                    <tr>
					
                      <td style="width: 12%;"><?php echo date('d-m-Y',strtotime($prev_job->complition_date)) ?></td>
                      <td><a href="user/<?php echo $prev_job->username; ?>"><img src="<?php echo base_url('assets/img/').$prev_job->picture;?>" class="rounded" height="50px"></a></td>
                      <td><a href="<?php echo base_url('history_detail_page/'.base64_encode($prev_job->id));?>"><?php echo $prev_job->job_description; ?></a></td>
                      <td>$<?php echo $prev_job->job_cost; ?></td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
				<?php else: echo "There is no Job Available right now !"; endif; ?>
              </div>
            </div>
            



            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
              <div class="available-jobs mt-5 table-responsive">
			  <?php if(!empty($new_jobs)):?>
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Date</th>
                      <th scope="col">Buyer</th>
                      <th scope="col">Discription</th>
                      <th scope="col">Budget</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
				  <?php foreach($new_jobs as $new_job): if($new_job->picture=="") $new_job->picture="smallman.png" ; ?>
                    <tr>
                      <td style="width: 12%;"><?php echo $new_job->post_date; ?></td>
                      <td><a href="user/<?php echo $new_job->username; ?>"><img src="<?php echo base_url('assets/img/').$new_job->picture;?>" class="rounded" height="50px"></a></td>
                      <td><?php echo $new_job->job_description; ?></td>
                      <td>$<?php echo $new_job->job_cost; ?></td>
                      <td><div class="buttonsets">
                          <a class="ignore" href="jobignore/<?php echo $new_job->id ;?>"><button type="button" class="btn btn-grey">Ignore3</button></a>
                    
                          <a class="btn themebutton" href="<?php echo base_url(); ?>job_detail_page/<?php echo $new_job->id; ?>"> Apply</a> 

                        </div></td>
                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
				<?php else: echo "There is no Job Available right now !"; endif; ?>
              </div>
            </div>




            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
              <div class=" mt-5 table-responsive">
               
               <?php if(!empty($my_jobs)):?>
			   <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Date</th>
                      <th scope="col">Buyer</th>
                      <th scope="col">Discription</th>
                      <th scope="col">Budget</th>
                    </tr>
                  </thead>
                  <tbody>
				  <?php foreach($my_jobs as $my_job): if($my_job->picture=="") $my_jobs->picture="smallman.png" ; ?>
                    <tr>
                      <td style="width: 12%;"><?php echo $my_job->post_date; ?></td>
                      <td><a href="user/<?php echo $my_job->username; ?>"><img src="<?php echo base_url('assets/img/').$my_job->picture;?>" class="rounded" height="50px"></a></td>
                      <td><a href="<?php echo base_url('deliver_work/'); echo $my_job->id; ?>"><?php echo $my_job->job_description; ?></a></td>
                      <td>$<?php echo $my_job->job_cost; ?></td>
                    </tr>
                      <?php endforeach; ?>
                  </tbody>
                </table>
				<?php else: echo "There is no Job Available right now !"; endif; ?>
				
              </div>
            </div>




             <div class="tab-pane fade" id="applied" role="tabpanel" aria-labelledby="contact-tab">
              <div class=" mt-5 table-responsive">
               
               <?php if(!empty($applied_jobs)):?>
         <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Date</th>
                      <th scope="col">Buyer</th>
                      <th scope="col">Discription</th>
                      <th scope="col">Budget</th>
                    </tr>
                  </thead>
                  <tbody>
          <?php foreach($applied_jobs as $applied_job): if($applied_job->picture=="") $applied_job->picture="smallman.png" ;  ?>
                    <tr>
                      <td style="width: 12%;"><?php echo $applied_job->post_date; ?></td>
                      <td><a href="user/<?php echo $applied_job->username; ?>"><img src="<?php echo base_url('assets/img/').$applied_job->picture;?>" class="rounded" height="50px"></a></td>
                      <td><?php echo $applied_job->job_description; ?></td>
                      <td>$<?php echo $applied_job->cost; ?></td>
                    </tr>
                      <?php endforeach; ?>
                  </tbody>
                </table>
        <?php else: echo "There is no Job Available right now !"; endif; ?>
        
              </div>
            </div>





          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Modal -->
  <div class="modal fade" id="applybtnjob" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        </div>
        <div class="modal-body"> ... </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function () {
	$('[data-toggle="tab"]').click(function(e) {
	    var $this = $(this),
        loadurl = $this.attr('href'),
        targ = $this.attr('data-target');

    $.get(loadurl, function(data) {
        $(targ).html(data);
		$('.time').timeago();
    });

    $this.tab('show');
    
});

});
function ignorejob(jobid)
{
	$.get('ignorejob/'+jobid);
	 $('#'+jobid).slideUp();
	return false;
}
</script>
