<!-- Masthead -->
<?php $CI=&get_instance(); ?>
<header class="breadcrumb-div text-white text-center">
  <div class="overlay"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12 mx-auto">
        <h3>Profile</h3>
      </div>
    </div>
  </div>
</header>
<section id="profile">
  <div class="container">
    <div class="row">
      <div class="col col-md-12"> 
      <div class="mt-5"></div>
      <div class="media">
  <div class="media-left">
    <img src="<?php echo base_url('assets/img/').$profile[0]->picture; ?>" class="media-object rounded" width="95px">
  </div>
  <div class="media-body ml-4">
    <h4 class="media-heading text-green"><?php echo $profile[0]->f_name; ?></h4>
    <p> 
      <i class="fa fa-home"></i>&nbsp;  <?php echo $profile[0]->location; ?>
    <br> 
    <i class="fa fa-phone"></i>&nbsp; <?php echo $profile[0]->phone; ?>
    <br>
   <?php if($avgreview){ ?>
     
              <?php $rate=$avgreview;

                for($i=0;$i<5;$i++){
                   if($i<$rate)
                        echo "<span class='fa fa-star checked'></span>";  
                   else
                        echo "<span class='fa fa-star text-muted'></span>";  
                }
               }   
            ?>
    </p>
  </div>
</div>
      
      <div class="pro-body">
      
      <p><?php echo $profile[0]->description; ?></p>
      
      </div>
      
      
      </div>
    </div>
    
 <!--   
    <div class="features-icons text-center">
    <div class="row">
        	<div class="col-lg-3">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
                <h3 class="text-green">$5k</h3>
              <p class="lead mb-0">Per hour </p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <h3 class="text-green">20+</h3>
              <p class="lead mb-0">Jobs</p>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="features-icons-item mx-auto mb-0 mb-lg-3">
                <h3 class="text-green">$500k</h3>
              <p class="lead mb-0">Earnings</p>
            </div>
          </div>
            <div class="col-lg-3">
            <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">
              <h3 class="text-green">100+</h3>
              <p class="lead mb-0">Happy Clients <br>
+ <br>

Maintenance Plan (Optional)
</p>
            </div>
          </div>
        </div>
        </div>
        -->
             <div class="mt-5"></div>
    
    <div class="review-detail">
    <?php if($avgreview){ ?>
    <h4> Total Reviews 
              <?php $rate=$avgreview;

                for($i=0;$i<5;$i++){
                   if($i<$rate)
                        echo "<span class='fa fa-star checked'></span>";  
                   else
                        echo "<span class='fa fa-star text-muted'></span>";  
                }
                  echo " ".round($avgreview,1);
            ?>
    
    </h4>
    <hr>
<?php } 

    else {?>
     
     <hr>

<h5><center>Not reviewed yet!</center> </h5>
<hr>

   
<?php
 }
?>




   <?php foreach ($reviews as $review) { ?>

    <div class="mt-4"></div>
    
    <div class="media">
  <div class="media-left" style="padding-left: 0; ">
    <img src="<?php echo base_url('assets/img/'.$review->picture) ?>" class="media-object rounded" width="75px">
  </div>
  <div class="media-body ml-4">
    <h5 class="media-heading"><?php echo $review->username; ?>  

               <?php 

               $rate1=$review->rates;
                for($g=0;$g<5;$g++){
                   if($g<$rate1)
                      echo "<span class='fa fa-star checked'></span>";  
                   else
                      echo "<span class='fa fa-star text-muted'></span>";
                  }
                ?>

        </h5>
          <p>
             <?php echo $review->review; ?>
          </p>
          <p>
             <span class="text-muted time" title="<?php echo $review->review_date; ?>"><?php echo $review->review_date; ?>
             </span>
        </p>
  </div>
</div>
    <hr>



 <?php } ?>
     
     

    
    
    </div>
    
    
        
             <div class="mt-5"></div>
        
  </div>
</section>



