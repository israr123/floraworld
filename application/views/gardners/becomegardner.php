<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<style>
.error {
	color:red;
}
</style>
<?php 

$fname='';
$lname='';
$username='';
$phone='';
$email='';
$location='';
$desc='';
$lat='';
$lng='';

if(isset($info)){

$fname=$info['fname'];
$lname=$info['lname'];
$username=$info['username'];
$phone=$info['phone'];
$email=$info['email'];
$location=$info['location'];
$desc=$info['description'];
$lat=$info['lat'];
$lng=$info['log'];

}

?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaKN0aL5oRLrpyj-hl0Wd5e-UxXKuB2cM&libraries=places&callback=initAutocomplete"
        async defer></script>
		 <script>
function initAutocomplete(){
    var input = document.getElementById('autocomplete');
    // var options = {
    //   types: ['(regions)'],
    //   componentRestrictions: {country: "IN"}
    // };
    var options = {}

    var autocomplete = new google.maps.places.Autocomplete(input, options);

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var place = autocomplete.getPlace();
      var lat = place.geometry.location.lat();
      var lng = place.geometry.location.lng();
      var placeId = place.place_id;
     
      document.getElementById("latitude").value = lat;
      document.getElementById("longitude").value = lng;
      document.getElementById("location_id").value = placeId;
    });
  }
    </script>

    <!-- Masthead -->
    <header class="breadcrumb-div text-white text-center">
      <div class="overlay"></div>
      <div class="container-fluid">
        <div class="row">
          <div class="col-xl-12 mx-auto">
          
       <h3>Become A Gardener!</h3>
          </div>
         </div>
      </div>
    </header>

<section id="signup">
<div class="container">
  <div class="row">
    <div class="col col-md-12">
      <h1 class="text-center"> Register yourself to become a part of Flora World!</h1>
        <p class="text-center">  Welcome aboard! Complete a profile. If you are a gardener, request to set up a listing and get greening!</p>
        </div>
        </div>
        <div class="row justify-content-md-center">
      <div class="col col-md-6">

        <form id="signupform" onsubmit="return checkForm(this);" action="<?php echo base_url();?>gardners/registered" method="post" enctype="multipart/form-data" class="mt-4">
      <div class="form-group">
                <label for="fname">First Name:</label>
				
                <input type="text" value="<?php echo $fname; ?>" name="first_name" class="form-control" id="fname" required>
              </div>
        
         <div class="form-group">
                <label for="lname">Last Name:</label>
                <input type="text" value="<?php echo $lname; ?>" name="last_name" class="form-control" id="lname" required>
              </div>
        
         <div class="form-group">
                <label for="username">Username:</label>
                <input type="text" value="<?php echo $username; ?>" name="username" id="username" class="form-control" id="username" required>
              </div>       
		     <div class="form-group" id="locationField">
                <label for="Location">Location:</label>
                <input type="text" value="<?php echo $location; ?>" name="location" class="form-control" id="autocomplete" required>
              </div>

        <div class="form-group">
                <label for="phone">Phone No:</label>
				<div id="errmsg"></div>
                <input type="text" maxlength="14" value="<?php echo $phone; ?>" name="phone" class="form-control" id="phone" required>
              </div>

         <div class="form-group">
                <label for="email">Email address:</label>
                <input type="email" value="<?php echo $email; ?>" name="email" class="form-control" id="email" required>
              </div>

          <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" name="password" class="form-control" id="password" required>
              </div>
              
              
              <div class="form-group">
                <label for="c_password">Confirm Password:</label>
                <input type="password" name="confirm_password" class="form-control" id="c_password">
              </div>
              
              <div class="form-group">
                <label for="file">Profile Picture:</label>
                <input title="Please Select a valid image file." type="file" name="picture" class="form-control" id="file">
              </div>
              
              <div class="form-group">
               <label for="au">Something About you</label> 
               <textarea type="text" name="description" class="form-control" id="au" rows="5"><?php echo $desc; ?></textarea>
              </div>

                <div class="form-check">
    <label class="form-check-label">
      <input class="form-check-input" name="accept_terms" value=""  type="checkbox" id="agree"> I accept <a href="<?=base_url('about/terms');?>">Terms of use</a>
     
    </label>
  </div>
  <button type="Submit" id="submit" disabled="disabled" class="btn btn-dark themebutton btn-block mt-4">Create my account</button>
				<input type="hidden" name="lat" id="latitude" value="<?php echo $lat; ?>">
				<input type="hidden" name="lng" id="longitude" value="<?php echo $lng; ?>">
				<input type="hidden" name="locid" id="location_id" value=""> 
              
     </form>             
        
        
        
        </div>
    </div>
</div>

</section>

<script type="text/javascript">

 var checker = document.getElementById('agree');
 var sendbtn = document.getElementById('submit');
 // when unchecked or checked, run the function
 checker.onchange = function(){
      if(this.checked){
          sendbtn.disabled = false;
        } else {
          sendbtn.disabled = true;
            }

  }
  
$(document).ready(function () {
  //called when key is pressed in textbox
  $("#phone").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
  
  
  $("#signupform").validate({
		 rules: {
			 password: "required",
		 confirm_password: {
				  equalTo: "#password"
				},
		 
		 phone: {
			  required: true,
			  pattern: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/
			  	},
		picture: {
			  required: false,
			  extension: "png|jpg|jpeg|gif"
				}
	  }
    });

  });
   
 
   </script>

   