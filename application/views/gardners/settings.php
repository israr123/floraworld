<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<style>
.error {
	color:red;
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaKN0aL5oRLrpyj-hl0Wd5e-UxXKuB2cM&libraries=places&callback=initAutocomplete"
        async defer></script>
     <script>
	 
 function initAutocomplete(){
    var input = document.getElementById('autocomplete');
    // var options = {
    //   types: ['(regions)'],
    //   componentRestrictions: {country: "IN"}
    // };
    var options = {}

    var autocomplete = new google.maps.places.Autocomplete(input, options);

    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var place = autocomplete.getPlace();
      var lat = place.geometry.location.lat();
      var lng = place.geometry.location.lng();
      var placeId = place.place_id;
     
      document.getElementById("latitude").value = lat;
      document.getElementById("longitude").value = lng;
      document.getElementById("location_id").value = placeId;
    });
  }

    </script>
<!-- Masthead -->
<header class="breadcrumb-div text-white text-center">
  <div class="overlay"></div>
  <div class="container-fluid">
    <div class="row">
      <div class="col-xl-12 mx-auto">
			<h3>Settings</h3>
      </div>
    </div>
  </div>
</header>
<section id="signup">
  <div class="container">
  <div class="row">
  <div class="col-md-3">
<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
  <a class="nav-link active" id="v-pills-about-tab" data-toggle="pill" href="#v-pills-about" role="tab" aria-controls="v-pills-about" aria-selected="true">Profile Info</a>
  <!--<a class="nav-link" id="v-pills-work-tab" data-toggle="pill" href="#v-pills-work" role="tab" aria-controls="v-pills-work" aria-selected="false">Account</a>-->

</div> </div>
<div class="col-md-9">
<div class="tab-content" id="v-pills-tabContent">
  <div class="tab-pane fade show active" id="v-pills-about" role="tabpanel" aria-labelledby="v-pills-home-tab">
  	
    <div class="row justify-content-md-center">
      <div class="col col-md-10">
        <form id="settingform" action="<?php echo base_url(); ?>update_settings" method="post" enctype="multipart/form-data" >
        <span><strong>This information is visible to all Flora World users in your profile page.</strong></span>
          <div class="form-group">
            <label for="fname">First name</label>
            <input required type="text" name="first_name" value="<?php echo $info[0]->f_name; ?>" class="form-control" id="fname">
          </div>
          
                    <div class="form-group">
            <label for="lname">Last name</label> <span> (only first letter shown to other users)</span>
            <input required type="text" name="last_name" value="<?php echo $info[0]->l_name; ?>" class="form-control" id="lname">
          </div>
          
             <div class="form-group">
            <label for="dname">Display name <i  data-toggle="tooltip" data-placement="right" title="If you represent an organisation, you can use its name as your display name. Display name is shown to other users instead of your first and last name." class="fa fa-exclamation"></i></label> 
            <input type="text" disabled value="<?php echo $info[0]->username; ?>" class="form-control" id="dname" required>
            
          </div>
          
           <div class="form-group">
            <label for="add">Address <i data-toggle="tooltip" data-placement="right" title="Address from where you are" class="fa fa-exclamation"></i></label> 

            <input type="text"  name="location"  value="<?php echo $info[0]->location; ?>" class="form-control" id="autocomplete" required>
              <input type="hidden" name="lat" id="latitude" value="<?php echo $info[0]->lat; ?>">
				<input type="hidden" name="lng" id="longitude" value="<?php echo $info[0]->log; ?>">
				<input type="hidden" name="locid" id="location_id" value="">
          </div>
          
          <div class="form-group">
            <label for="tele">Telephone</label> 

            <input required maxlength="14" type="text" reqiured  name="phone" value="<?php echo $info[0]->phone; ?>" class="form-control" id="tele">
            
          </div>
           <div class="form-group">
            <label for="pp">Profile Picture</label> 
			<div class="media">
			  <div class="media-left" style="padding-left: 0; ">
				<img src="<?php echo base_url(); ?>assets/img/<?php echo $info[0]->picture; ?>" class="media-object rounded" width="75px">
			  </div>
            <input type="file" name="picture"  title="Please Select a valid image format." class="form-control" id="pp">
            
          </div>
          
           <div class="form-group">
            <label for="au">Something About you</label> 

            <textarea type="text" name="description" class="form-control" id="au" rows="5" required><?php echo $info[0]->description; ?></textarea>
            
          </div>
           <button type="Submit" class="btn btn-dark themebutton btn-block mt-4">Save Information</button>
       
       
        </form>
      
      </div>
    </div>
  
  
  
  
  </div>
  
  
  <!--<div class="tab-pane fade" id="v-pills-work" role="tabpanel" aria-labelledby="v-pills-profile-tab">
  		 <div class="row justify-content-md-center">
      <div class="col col-md-10">
      	<div class="account-settings-text border-set">
        	<b>Username: </b> User name
        </div>
              	<div class="account-settings-address border-set">
        	<b>Email addresses</b> abc@gmail.com
        </div>
        
        	<div class="account-settings-address border-set">
        	<b>Password</b> *****
        </div>
        
        	<div class="account-settings-address border-set">
            
            <b> Delete Account </b> <br>
        	If you delete your account, your personal information (name, phone number, address, email, profile picture, etc.) will be deleted permanently and can't be recovered. All the listings you have created will be removed. You won't be able to reactivate your account.
<br>
Information where other members are involved (conversations with other people, transactions you've made, reviews you've given to others, etc) is not removed when you delete your account. However, your name will no longer be displayed next to this information.    

<br> 


      <button type="button" class="btn btn-dark themebutton mt-3 mb-3">Permanently delete my account</button>
        </div>
        
        
      </div>
      </div>
  
  
  </div>
  
  -->
</div>
</div>
</div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function () {

//	 $('[data-toggle="tooltip"]').tooltip(); 
	
	  $("#settingform").validate({
		  rules: {
		  
		 phone: {
			  required: true,
			  pattern: /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/
			  	},
          
		picture: {
			  required: false,
			  extension: "png|PNG|JPG|jpg|JPEG|jpeg|GIF|gif"
				}
		  }
	  
	  
	  
	  });
	  
});
</script>