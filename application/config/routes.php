<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'HomeController';
$route['login']='LoginController/login';
$route['users/verifylogin']='LoginController/verifyuserlogin';
$route['users/register']='RegisterController/userregister';
$route['users/registered']='RegisterController/userregistered';
$route['profile']='GardnerController/profile'; //gardner profile
$route['myprofile']='UserController/profile'; // user profile
$route['uindex']='UserController/index'; // user home
$route['gindex']='GardnerController/index'; //gardner home
$route['mysettings']='UserController/settings'; // user settings
$route['settings']='GardnerController/settings'; // gardners settings
$route['deliver_work/(:any)']='GardnerController/deliverwork/$1'; // deliver_work
$route['delivered_work/(:any)']='GardnerController/deliveredwork/$1'; // deliver_work
$route['accept_job']='UserController/acceptjob'; // deliver_work
$route['get_grd_job_history']='GardnerController/get_ajax_job_history'; // ajax call 
$route['get_grd_my_jobs']='GardnerController/get_ajax_my_jobs'; // ajax call 
$route['get_grd_new_jobs']='GardnerController/get_ajax_new_jobs'; // ajax call 
$route['get_grd_applied_jobs']='GardnerController/get_ajax_applied_jobs'; // ajax call 
$route['get_user_job_history']='UserController/get_ajax_job_history'; // ajax call 
$route['get_user_my_jobs']='UserController/get_ajax_my_jobs'; // ajax call 
$route['get_user_new_jobs']='UserController/get_ajax_new_jobs'; // ajax call 
$route['get_user_applied_jobs']='UserController/get_ajax_applied_jobs'; // ajax call 
$route['rejectjob/(:num)/(:num)']='UserController/do_ajax_reject_job/$1/$2'; // ajax call 
$route['ignorejob/(:num)']='GardnerController/do_ajax_ignore_job/$1'; // ajax call 
$route['ignorethisjob/(:num)']='GardnerController/ignore_job/$1'; // normalcall 
$route['search_gardeners_ajax?(:any)']='HomeController/search_gardeners_ajax'; // ajax call 
$route['search_gardeners']='HomeController/search_gardeners'; // ajax call 
$route['follow/(:num)']='SocialController/follow_up/$1'; // ajax call 
$route['add_likes']='UserController/add_likes'; // ajax call 
$route['get_user_notifications']='HomeController/get_user_notifications'; 
$route['get_user_total_notifications']='HomeController/get_user_total_notifications'; 
$route['get_grd_notifications']='HomeController/get_grd_notifications'; 
$route['get_grd_total_notifications']='HomeController/get_grd_total_notifications'; 
$route['get_posts']='HomeController/get_posts'; 
$route['hide_user_notifications']='HomeController/hide_user_notifications'; 
$route['hide_grd_notifications']='HomeController/hide_grd_notifications'; 
$route['check_user_exist']='HomeController/check_ajax_user_exist'; 
































































$route['create_social_feed']="SocialController/index";
$route['save_social_feed']="SocialController/savesocialfeed";
$route['listing']="HomeController/listing";
$route['gardner/(:any)']="HomeController/viewgardnerprofile/$1";
$route['user/(:any)']="HomeController/viewuserprofile/$1";
$route['blankerror']="HomeController/blankerror";


$route['apply_for_job/(:num)']="GardnerController/applyforjob/$1";
$route['postajob']="UserController/postajob";
$route['buying']="UserController/buying";
$route['social']="UserController/social";
$route['apply_job/(:num)']='GardnerController/applyjob/$1'; // user settings
$route['job_detail_page/(:any)']='GardnerController/jobdetailpage/$1'; // user settings
$route['ucurrent_d_page/(:any)']='UserController/ucurrentdetailpage/$1';
$route['uaplied_d_page/(:any)/(:any)']='UserController/uaplieddetailpage/$1/$1';
$route['user_job_detail_page/(:any)']='UserController/jobdetailpage/$1'; // user settings
$route['myhistory_detail_page/(:any)']='UserController/historydetailpage/$1'; // user detail history page
$route['history_detail_page/(:any)']='GardnerController/historydetailpage/$1'; // gardner detail history page

$route['review_page/(:any)']='UserController/jobreview/$1'; // user review page
$route['save_review']='UserController/savereview'; 
$route['gardner_review']='GardnerController/savereview';

$route['update_mysettings']='UserController/updatesettings'; // user settings
$route['update_settings']='GardnerController/updatesettings'; // gardners settings


$route['verify_email/(:any)']='RegisterController/verify_email/$1';
$route['verify_forget_password/(:any)']='RegisterController/verify_forget_password/$1';
$route['save_forget_password']='RegisterController/save_forget_password';
$route['forget_password']='RegisterController/forget_password';


$route['logout']='LoginController/logout';
$route['gardners/register']='RegisterController/gardnerregister';
$route['gardners/registered']='RegisterController/gardnerregistered';
$route['about']='HomeController/about';
$route['about/(:any)']='HomeController/about/$1';
$route['contactus']='HomeController/contactus';
$route['save_contactus']='HomeController/savecontactus';











$route['api/login']='ApiController/login';
$route['api/user_register']='ApiController/user_register';
$route['api/gardner_register']='ApiController/gardner_register';
$route['api/update_device_token']='ApiController/update_device_token';
$route['api/update_user_setting']='ApiController/update_user_setting';
$route['api/update_gardner_setting']='ApiController/update_gardner_setting';
$route['api/create_post']='ApiController/create_post';
$route['api/create_job']='ApiController/create_job';
$route['api/user_posts']='ApiController/user_posts';
$route['api/accept_job']='ApiController/accept_job';
$route['api/reject_job']='ApiController/reject_job';
$route['api/follow']='ApiController/follow';
$route['api/add_likes']='ApiController/add_likes';
$route['api/search']='ApiController/search';
$route['api/own_posts']='ApiController/own_posts';

$route['api/forget_password']='ApiController/forget_password';
$route['api/verify_email']='ApiController/verify_email';
$route['api/ignore_job']='ApiController/ignore_job';
$route['api/apply_job']='ApiController/apply_job';
$route['api/deliver_work']='ApiController/deliver_work';

$route['api/newjobs_sp']='ApiController/newjobs_sp';
$route['api/running_job_sp']='ApiController/running_job_sp';
$route['api/applied_job_sp']='ApiController/applied_job_sp';
$route['api/history_job_sp']='ApiController/history_job_sp';

$route['api/user_new_jobs']='ApiController/user_new_jobs';
$route['api/user_running_jobs']='ApiController/user_running_jobs';
$route['api/user_request_jobs']='ApiController/user_request_jobs';
$route['api/user_history_jobs']='ApiController/user_history_jobs';

$route['api/job_review_by_user']='ApiController/job_review_by_user';
$route['api/job_review_by_gardner']='ApiController/job_review_by_gardner';
$route['api/save_reveiw_by_gardner']='ApiController/save_reveiw_by_gardner';
$route['api/save_reveiw_by_user']='ApiController/save_reveiw_by_user';

$route['api/user_all_reviews']='ApiController/user_all_reviews';
$route['api/gardner_all_reviews']='ApiController/gardner_all_reviews';
$route['api/update_device_id']='ApiController/update_device_id';	
$route['api/gardner_public_profile']='ApiController/gardner_public_profile';	
$route['api/user_public_profile']='ApiController/user_public_profile';	






































$route['404_override'] = 'HomeController';

//$route['translate_uri_dashes'] = FALSE;

